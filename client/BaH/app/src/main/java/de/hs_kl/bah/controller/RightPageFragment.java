package de.hs_kl.bah.controller;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.model.IngameWhiteCard;
import de.hs_kl.bah.model.User;
import hs_kl.de.bah.R;

/**
 * The "submissions" part of the GameActivity.
 * Responsible for displaying players of the current gametable and their submissions.
 * Also used for judging the winnercards.
 */
public class RightPageFragment extends GamePageFragment{

    private User potentialWinner = null;
    private Button submitWinnerButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_game_right_page, container, false);

        submitWinnerButton = (Button) rootView.findViewById(R.id.BTN_SUBMIT_WINNER);
        ListView submissionListView = (ListView) rootView.findViewById(R.id.LV_GAME_RIGHTPAGE_SUBMISSIONS);

        SubmittedCardsListAdapter adapter = new SubmittedCardsListAdapter(getGameActivity(), this);
        submissionListView.setAdapter(adapter);

        //indicate current game state to the player
        //TODO: cache booleans and reuse in adapter to save performance
        if(getGameActivity().activeRoundIsPreviousRound()) {
            showNextButton();
        } else if(!getActiveRound().hasSufficientAmountOfPlayers()){
            indicateNotSufficientPlayers();
        } else if(getActiveRound().allPlayersExceptJudgeSubmittedCards()){
            if(!getGameActivity().mainUserIsJudge())
                indicateWaitingForJudge();
            else
                indicateChooseWinner();
        } else if(getGameActivity().mainUserAlreadySubmitted()) {
            indicateWaitingForOtherPlayers();
        } else {
            indicateSubmitYourCards();
        }

        return rootView;
    }

    public void markPotentialWinner(User user){
        potentialWinner = user;
        submitWinnerButton.setVisibility(View.VISIBLE);
        submitWinnerButton.setText(getResources().getString(R.string.game_submit_winner));
        submitWinnerButton.setEnabled(true);
    }

    public void unmarkPotentialWinner(){
        potentialWinner = null;
        indicateChooseWinner();
    }

    public User getPotentialWinner(){
        return potentialWinner;
    }

    private void showNextButton(){
        submitWinnerButton.setText(getResources().getString(R.string.game_next_round));
        submitWinnerButton.setVisibility(View.VISIBLE);
        submitWinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBahApp().removeIdFromAdvancedGameTableIds(getGameActivity().getGameTableId());
                getGameActivity().restartActivity();
            }
        });
    }

    public void indicateNotSufficientPlayers(){
        submitWinnerButton.setText(getResources().getString(R.string.game_not_sufficient_players));
        submitWinnerButton.setVisibility(View.VISIBLE);
        submitWinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show invite activity
                ActivityStarter.startInviteActivity(getGameActivity(), getGameActivity().getApplicationContext(), getGameActivity().getGameTableId());
            }
        });
    }

    public void indicateWaitingForJudge(){
        submitWinnerButton.setText(getResources().getString(R.string.game_waiting_for_judge));
        submitWinnerButton.setVisibility(View.VISIBLE);
        submitWinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing
            }
        });
    }

    public void indicateWaitingForOtherPlayers(){
        submitWinnerButton.setText(getResources().getString(R.string.game_waiting_for_others));
        submitWinnerButton.setVisibility(View.VISIBLE);
        submitWinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing
            }
        });
    }

    public void indicateSubmitYourCards(){
        submitWinnerButton.setText(getResources().getString(R.string.game_submit_your_cards));
        submitWinnerButton.setVisibility(View.VISIBLE);
        submitWinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing
            }
        });
    }

    public void indicateChooseWinner(){
        submitWinnerButton.setText(getResources().getString(R.string.game_choose_winner));
        submitWinnerButton.setVisibility(View.VISIBLE);
        submitWinnerButton.setEnabled(false);
    }


    @Override
    public String getTitle(){
        return getGameActivity().getResources().getString(R.string.game_right_page);
    }

    /**
     * Responsible for initing the respective player elements of the right page fragment listview.
     * Also contains a gridview for each element, which displays the submitted cards of a player.
     */
    private static class SubmittedCardsListAdapter extends ArrayAdapter<User> {
        private final BahActivity context;
        private final RightPageFragment fragment;
        private final Map<User, List<IngameWhiteCard>> playersWithSubmittedCards;
        private List<User> playerOrder;

        public SubmittedCardsListAdapter(BahActivity context, RightPageFragment fragment) {
            super(context, R.layout.listitem_submitted_cards, new ArrayList<>(fragment.getGameActivity().getPlayersWithSubmittedCards().keySet()));
            this.context = context;
            this.fragment = fragment;
            this.playersWithSubmittedCards = fragment.getGameActivity().getPlayersWithSubmittedCards();
            this.playerOrder = new ArrayList<>(playersWithSubmittedCards.keySet());
            shufflePlayers();
        }

        /**
         * shuffles the player order but puts judge at first position
         */
        private void shufflePlayers(){
            if(playerOrder.size()>1) {
                User judge = null;
                Collections.shuffle(playerOrder);
                for (int i = 0; i < playerOrder.size(); i++) {
                    if(fragment.getActiveRound().isJudge(playerOrder.get(i).getEmail())){
                        judge = playerOrder.get(i);
                    }
                }
                if(null!=judge){
                    playerOrder.remove(judge);
                    playerOrder.add(0, judge);
                }
            }
        }

        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            final View rowView = inflater.inflate(R.layout.listitem_submitted_cards, null, true);


            //display the respective player information
            final User player = playerOrder.get(position);
            TextView txtPlayerName = (TextView) rowView.findViewById(R.id.TXT_LISTITEM_SUBMITTED_PLAYERNAME);
            ImageView imgPlayerState = (ImageView) rowView.findViewById(R.id.IMG_LISTITEM_SUBMITTED_PLAYERSTATE);
            final boolean playerIsJudge = fragment.getActiveRound().isJudge(player.getEmail());
            final boolean allUsersHaveSubmitted = fragment.getActiveRound().allPlayersExceptJudgeSubmittedCards();
            final boolean tableHasSufficientPlayers = fragment.getActiveRound().hasSufficientAmountOfPlayers();
            final boolean showPlayerName = fragment.getGameActivity().activeRoundIsPreviousRound() || playerIsJudge;
            txtPlayerName.setText(showPlayerName ? player.getUsername() : fragment.getGameActivity().getResources().getString(R.string.game_anonymous));
            if(playerIsJudge){
                int judge_icon = allUsersHaveSubmitted && tableHasSufficientPlayers ? R.drawable.judge_action_icon : R.drawable.judge_icon;
                imgPlayerState.setImageDrawable(context.getResources().getDrawable(judge_icon));
            }else if(fragment.getGameActivity().getActiveRound().hasSubmittedCards(player)) {
                imgPlayerState.setImageDrawable(context.getResources().getDrawable(R.drawable.submitted));
            }else {
                imgPlayerState.setImageDrawable(context.getResources().getDrawable(R.drawable.not_submitted));
            }

            //dont display submitted card section if the respective player is the judge
            if(playerIsJudge)
                return rowView;

            //display submitted cards of the respective player only if the main user of this device already submitted his cards or if he is the judge
            List<IngameWhiteCard> submittedCards;
            final boolean mainUserIsJudge = fragment.getGameActivity().mainUserIsJudge();
            final boolean mainUserHasSubmitted = allUsersHaveSubmitted || fragment.getGameActivity().mainUserAlreadySubmitted();
            if (mainUserIsJudge || mainUserHasSubmitted) {
                submittedCards = playersWithSubmittedCards.get(player);

                if (submittedCards.isEmpty()) {
                    submittedCards = new ArrayList<>();
                    submittedCards.add(new IngameWhiteCard(-1, context.getResources().getString(R.string.game_not_submitted), IngameWhiteCard.NOT_SUBMITTED));
                }
                //submission order only matters when the user can actually see the submitted cards and if there are any
                else
                    submittedCards = fragment.getGameActivity().getSubmittedCardsSortedBySubmissionOrder(submittedCards);
            }
            //else indicate that not allowed to see submissions until he has submitted his cards
            else {
                submittedCards = new ArrayList<>();
                for (int i = 0; i < fragment.getActiveRound().getBlackCard().getNumOfBlanks(); i++)
                    submittedCards.add(new IngameWhiteCard(-1, context.getResources().getString(R.string.game_covert_card), IngameWhiteCard.NOT_SUBMITTED));
            }

            //show gridview containing "submitted" cards
            GridView horizontalGridView = (GridView) rowView.findViewById(R.id.HGV_LISTITEM_SUBMITTED_CARDS);
            configureSumittedCardsGridView(horizontalGridView, submittedCards.size());
            LeftPageFragment.UserHandListAdapter listAdapter = new LeftPageFragment.UserHandListAdapter(context, fragment, submittedCards);
            horizontalGridView.setAdapter(listAdapter);

            //prevent interaction if this is the gametables previous round (round finished)
            boolean isPreviousRound = fragment.getGameActivity().activeRoundIsPreviousRound();

            //show hint when clicking on a covert card
            if (!mainUserIsJudge && !mainUserHasSubmitted && !isPreviousRound) {
                horizontalGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Toast.makeText(context, context.getResources().getString(R.string.game_covert_card_hint), Toast.LENGTH_LONG).show();
                    }
                });
            }
            //enable selection / indication of the winner (judging)
            else if((mainUserIsJudge||isPreviousRound)&& allUsersHaveSubmitted && tableHasSufficientPlayers){
                //update rowView display (winner indication)
                final boolean currentPlayerIsPotentialWinner;
                if(isPreviousRound)
                    //in previous round, the winners were already chosen. check whether the respective user submitted the winning hand
                    currentPlayerIsPotentialWinner = fragment.getActiveRound().hasWinningHand(player);
                else {
                    //in current round indicate the user currently selected by the judge
                    User currenPotentialWinner = fragment.getPotentialWinner();
                    currentPlayerIsPotentialWinner = null != currenPotentialWinner && currenPotentialWinner.getEmail().equals(player.getEmail());
                }

                //indicate selected winner
                if(currentPlayerIsPotentialWinner){
                    rowView.setBackgroundResource(R.drawable.greencard);
                    if(isPreviousRound){
                        imgPlayerState.setImageDrawable(context.getResources().getDrawable(R.drawable.winner));
                    }
                    //normal player otherwise
                }else{
                    rowView.setBackgroundResource(R.drawable.blackcard);
                }

                //make the respective player selectable (not the judge and not if round already finished)
                if(!playerIsJudge && !isPreviousRound) {
                    rowView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectPotentialWinner(player, currentPlayerIsPotentialWinner, rowView, parent);
                        }
                    });
                    horizontalGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> par, View v, int pos, long id) {
                            //we are not interested in the clicked item, but in the player that this item belongs to
                            selectPotentialWinner(player, currentPlayerIsPotentialWinner, rowView, parent);
                        }
                    });
                }
            }

            return rowView;
        }

        private void selectPotentialWinner(User player, boolean playerIsPotentialWinner, View rowView, ViewGroup parent){

            if(playerIsPotentialWinner) {
                fragment.unmarkPotentialWinner();
                rowView.setBackgroundResource(R.drawable.blackcard);
            }
            else {
                //visually unmark previously selected winners
                for(int i=0; i<parent.getChildCount(); i++){
                    parent.getChildAt(i).setBackgroundResource(R.drawable.blackcard);
                }

                //mark potential winner
                rowView.setBackgroundResource(R.drawable.greencard);
                fragment.markPotentialWinner(player);
            }
        }

        private void configureSumittedCardsGridView(GridView gridview, int size) {
            if(size <= 0)
                size = 1;

            // get display metrics
            DisplayMetrics dm = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(dm);
            float density = dm.density;
            float dpScreenMargin = fragment.getGameActivity().getResources().getDimension(R.dimen.activity_horizontal_margin) / density;
            float dpWidth  = (dm.widthPixels / density) - 4.5f*dpScreenMargin;


            // Calculated width for the single elements of the grid view
            int width = size < 3 ? Math.round(dpWidth / size) : Math.round(dpWidth / 2.5f);

            // convert in respect to density
            int totalWidth = (int) (width * size * density);
            int singleItemWidth = (int) (width * density);

            // configure grid view
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(totalWidth, LinearLayout.LayoutParams.MATCH_PARENT);
            gridview.setLayoutParams(params);
            gridview.setColumnWidth(singleItemWidth);
            gridview.setStretchMode(GridView.STRETCH_SPACING);
            gridview.setNumColumns(size);
        }

    }
}

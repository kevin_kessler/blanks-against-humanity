package de.hs_kl.bah.async_tasks;

public interface BahRunner<T> {
    public void run(T t);
}

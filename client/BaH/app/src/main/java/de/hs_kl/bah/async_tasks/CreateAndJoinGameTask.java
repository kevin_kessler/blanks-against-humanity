package de.hs_kl.bah.async_tasks;

import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous "create game" task used to create a new game by the user and starting the game view afterwards
 */
public class CreateAndJoinGameTask extends BahAsyncTask<AsyncTaskResult<GameTable>> {

    private final String gameName;
    private final boolean isPrivate;
    private final NetworkServices networkServices;
    private final BahActivity parentActivity;
    BahRunner<AsyncTaskResult<GameTable>> onFinishRunner;

    public CreateAndJoinGameTask(BahActivity parentActivity, ProgressUiHelper progressUiHelper, String gameName, boolean isPrivate, BahRunner<AsyncTaskResult<GameTable>> onFinishRunner) {
        super(parentActivity, progressUiHelper);
        this.parentActivity = parentActivity;
        this.gameName = gameName;
        this.isPrivate = isPrivate;
        this.networkServices = new NetworkServicesImp(parentActivity.getBahApplication());
        this.onFinishRunner = onFinishRunner;
    }

    @Override
    protected AsyncTaskResult<GameTable> doInBackground(Void... params) {
        return networkServices.createGameTable(gameName, isPrivate);
    }

    @Override
    protected void onPostExecute(final AsyncTaskResult<GameTable> result) {
        super.onPostExecute(result);
        onFinishRunner.run(result);
    }

}

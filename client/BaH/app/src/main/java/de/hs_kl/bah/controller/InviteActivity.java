package de.hs_kl.bah.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import hs_kl.de.bah.R;

public class InviteActivity extends BahActivity {

    private static final String TAG = InviteActivity.class.getSimpleName();
    public static final String TABLE_ID = "table_id_flag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        if( ! getIntent().hasExtra(TABLE_ID)) {
            // If no table is given, finish this activity
            Log.e(TAG, "No TABLE_ID is in the intent which started InviteActivity. A valid tableid is necessary.");
            finish();
            return;
        }

        Intent intent = getIntent();
        String tableId = intent.getStringExtra(TABLE_ID);

        InviteActivityFragment fragment = (InviteActivityFragment) getFragmentManager().findFragmentById(R.id.INVITE_FRAGMENT);
        fragment.setTableId(tableId);
    }
}

package de.hs_kl.bah.android_services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = BootBroadcastReceiver.class.getSimpleName();
    static final String ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        // BOOT_COMPLETED” start Service if MainUserExists
        if (intent.getAction().equals(ACTION)) {
            Log.i(TAG, "Received BOOT_COMPLETED broadcast. Trying to start ServerPollService.");
            //Service
            ActivityStarter.startPollServiceIfMainUserExists(context);
        }
    }
}

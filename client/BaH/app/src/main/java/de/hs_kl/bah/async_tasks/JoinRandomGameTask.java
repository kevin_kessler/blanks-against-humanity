package de.hs_kl.bah.async_tasks;

import android.widget.Toast;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.DataStorageServicesImp;
import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous 'find game' task used to join a random game table
 */
public class JoinRandomGameTask extends BahAsyncTask<AsyncTaskResult<GameTable>> {

    private final BahActivity parentActivity;
    private final NetworkServices netWorkService;

    public JoinRandomGameTask(BahActivity parentActivity, ProgressUiHelper uiProgressUiHelper) {
        super(parentActivity, uiProgressUiHelper);
        this.parentActivity = parentActivity;
        this.netWorkService = new NetworkServicesImp(parentActivity.getBahApplication());
    }

    @Override
    protected AsyncTaskResult<GameTable> doInBackground(Void... params) {
        return netWorkService.joinRandomGameTable();
    }

    @Override
    protected void onPostExecute(final AsyncTaskResult<GameTable> result) {
        super.onPostExecute(result);

        GameTable gameTable = result.getResultParameter();
        if (null == gameTable || !gameTable.isValid()) {
            Toast.makeText(parentActivity, result.getErrMsg(), Toast.LENGTH_LONG).show();
        } else {
            (new DataStorageServicesImp(parentActivity)).save(gameTable);
            ActivityStarter.startGameActivity(parentActivity, parentActivity.getApplicationContext(), gameTable.getId(), false);
        }
    }

}

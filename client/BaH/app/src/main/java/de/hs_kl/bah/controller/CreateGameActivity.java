package de.hs_kl.bah.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.DataStorageServicesImp;
import de.hs_kl.bah.async_tasks.AsyncTaskResult;
import de.hs_kl.bah.async_tasks.BahRunner;
import de.hs_kl.bah.async_tasks.CreateAndJoinGameTask;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.GameTable;
import hs_kl.de.bah.R;

public class CreateGameActivity extends BahActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private CreateAndJoinGameTask createGameTask = null;

    private EditText txtGameName;
    private CheckBox cbPrivate;

    private ProgressUiHelper progressUiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);

        txtGameName = (EditText) findViewById(R.id.TXT_GAME_NAME);
        cbPrivate = (CheckBox) findViewById(R.id.CB_IS_PRIVATE);

        Button mBtnCreateGame = (Button) findViewById(R.id.BTN_CREATE_GAME);
        mBtnCreateGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptCreateGame();
            }
        });

        // Prepare progressUiHelper
        View createGameFormView = findViewById(R.id.CREATE_GAME_FORM);
        View progressView = findViewById(R.id.CREATE_GAME_PROGRESS);
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        progressUiHelper = new ProgressUiHelper(createGameFormView, progressView, shortAnimTime);
    }

    private void attemptCreateGame() {
        if (createGameTask != null && createGameTask.isRunning()) {
            return;
        }

        // Reset errors.
        txtGameName.setError(null);

        // Store values at the time of the creation attempt.
        String gameName = txtGameName.getText().toString();
        boolean isPrivate = cbPrivate.isChecked();

        boolean cancel = false;
        View focusView = null;

        // Check if user entered a name
        if (TextUtils.isEmpty(gameName)) {
            txtGameName.setError(getString(R.string.error_field_required));
            focusView = txtGameName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt create and focus form field with an error.
            focusView.requestFocus();
        } else {

            BahRunner<AsyncTaskResult<GameTable>> onFinishRunner = new BahRunner<AsyncTaskResult<GameTable>>() {
                @Override
                public void run(AsyncTaskResult<GameTable> gameTableAsyncTaskResult) {
                    GameTable returnedGameTable = gameTableAsyncTaskResult.getResultParameter();
                    boolean success = null != returnedGameTable;
                    if (!success) {
                        //show error message
                        Toast.makeText(CreateGameActivity.this, gameTableAsyncTaskResult.getErrMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        //save table to device
                        (new DataStorageServicesImp(CreateGameActivity.this)).save(returnedGameTable);

                        //set result (used in currentGamesActivity)
                        Intent resultIntent = new Intent();
                        setResult(Activity.RESULT_OK, resultIntent);

                        //start game activity
                        ActivityStarter.startGameActivity(CreateGameActivity.this, CreateGameActivity.this.getApplicationContext(), returnedGameTable.getId(), false);
                        CreateGameActivity.this.finish();
                    }
                }
            };
            createGameTask = new CreateAndJoinGameTask(this, progressUiHelper, gameName, isPrivate, onFinishRunner);
            createGameTask.execute((Void) null);
        }
    }




}

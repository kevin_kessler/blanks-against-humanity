package de.hs_kl.bah.model;

import com.google.gson.JsonObject;

import hs_kl.de.bah.BuildConfig;

/**
 * A MainUser is the user of the device.
 */
public class MainUser extends User {

    public static MainUser createFrom(JsonObject jsonMainUser) {
        String username = jsonMainUser.get("username").getAsString();
        String email = jsonMainUser.get("email").getAsString();
        String password = jsonMainUser.get("pwd").getAsString();
        int credit = jsonMainUser.get("credit").getAsInt();
        String sessionId = jsonMainUser.get("JSESSIONID").getAsString();
        return new MainUser(email, username, password, credit, sessionId);
    }

    /** We need to save the password to be able to re-authenticate the user in case the session invalidated. */
    private String password;
    private int credit;

    /** The currentSessionId is used to authenticate further calls (after login) to the server. */
    private String currentSessionId;

    public MainUser(String email, String username, String password, int credit, String session_id) {
        super(email, username);
        this.password = password;
        this.credit = credit;
        currentSessionId = session_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getCurrentSessionId() {
        return currentSessionId;
    }

    public void setCurrentSessionId(String currentSessionId) {
        this.currentSessionId = currentSessionId;
    }

    /**
     * Checks whether the user object has all essential parts.
     * Can be mainly used in assertions.
     */
    public boolean isValid() {
        return super.isValid() &&
                null != password && !password.isEmpty() &&
                0 <= credit;
    }

    /**
     * This method updates the calling instance with the data of the given user.
     * Helpful if a user reference has to be remained.
     * @param user
     */
    public void updateWith(MainUser user) {

        if (BuildConfig.DEBUG &&
                null == user || !user.isValid())
            throw new AssertionError("User is not valid.");

        super.updateWith(user);
        password = user.password;
        credit = user.credit;
        currentSessionId = user.currentSessionId;
    }
}

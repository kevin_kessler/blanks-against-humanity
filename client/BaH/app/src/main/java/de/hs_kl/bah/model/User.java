package de.hs_kl.bah.model;

import com.google.gson.JsonObject;

import hs_kl.de.bah.BuildConfig;

/**
 * Represents the data model of the user. This class contains everything user specific what is
 * necessary for the application.
 */
public class User {

    private String email;
    private String username;


    public User(String email, String username) {
        this.email = email;
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Checks whether the user object has all essential parts.
     * Can be mainly used in assertions.
     */
    public boolean isValid() {
        return null != email && !email.isEmpty() && null != username && !username.isEmpty();
    }

    /**
     * This method updates the calling instance with the data of the given user.
     * Helpful if a user reference has to be remained.
     */
    public void updateWith(User user) {

        if (BuildConfig.DEBUG &&
                null == user || !user.isValid())
            throw new AssertionError("User is not valid.");

        email = user.email;
        username = user.username;
    }

    @Override
    public String toString() {
        return String.format("%s;;;%s", username, email);
    }

    /**
     * This functions create a user object based on a string. The string has to have the syntax of
     * the string from user.toString().
     * So this is the opposite function of toString for User.
     *
     * This is necessary because we save the user as key in a map. If the map will be converted to
     * json the user has to be converted to a string representation. This will be done by the
     * toString() function. This function converts this representation back.
     */
    public static User createFrom(String userToString) {

        if (BuildConfig.DEBUG &&
                null == userToString || userToString.isEmpty() || !userToString.contains(";;;"))
            throw new AssertionError(String.format("User string is not valid: %s",userToString));

        String[] unameAndEmail = userToString.split(";;;");

        if (BuildConfig.DEBUG && 2 != unameAndEmail.length)
            throw new AssertionError(String.format("User string is not valid: %s",userToString));

        String username = unameAndEmail[0];
        String email = unameAndEmail[1].replace(";POINT;",".");

        return new User(email, username);
    }

    public static User createFrom(JsonObject jsonObject) {
        String _email = jsonObject.get("email").getAsString();
        String _username = jsonObject.get("username").getAsString();

        return new User(_email, _username);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ! (o instanceof User)) return false;

        User user = (User) o;

        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return !(username != null ? !username.equals(user.username) : user.username != null);

    }

    @Override
    public int hashCode() {
        int result = email != null ? email.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }
}

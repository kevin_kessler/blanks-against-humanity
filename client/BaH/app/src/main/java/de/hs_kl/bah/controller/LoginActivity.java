package de.hs_kl.bah.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.DataStorageServicesImp;
import de.hs_kl.bah.async_tasks.LoginTask;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.services.DataStorageServices;
import hs_kl.de.bah.R;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private LoginTask loginTask = null;

    // UI references.
    private EditText emailView;
    private EditText passwordView;

    private ProgressUiHelper progressUiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        emailView = (EditText) findViewById(R.id.ETXT_EMAIL_LOGIN);
        passwordView = (EditText) findViewById(R.id.ETXT_PASSWORD_LOGIN);
        // Load the last logged in email for convenience
        DataStorageServices dss = new DataStorageServicesImp(getApplicationContext());
        String lastLoggedInEmail = dss.loadLastLoggedInEmail();
        if (null != lastLoggedInEmail && ! lastLoggedInEmail.isEmpty()) {
            emailView.setText(lastLoggedInEmail);
            passwordView.requestFocus();
        }
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_SEND || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.BTN_SIGN_IN_LOGIN);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


        Button registerBtn = (Button) findViewById(R.id.BTN_REGISTER_LOGIN);
        registerBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityStarter.startRegisterActivity(LoginActivity.this, getApplicationContext());
                finish();
            }
        });

        // Prepare progressUiHelper
        View loginFormView = findViewById(R.id.LOGIN_FORM);
        View progressView = findViewById(R.id.LOGIN_PROGRESS);
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        progressUiHelper = new ProgressUiHelper(loginFormView, progressView, shortAnimTime);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (null!=loginTask && loginTask.isRunning()) {
            return;
        }

        // Reset errors.
        emailView.setError(null);
        passwordView.setError(null);

        // Store values at the time of the login attempt.
        String email = emailView.getText().toString().toLowerCase();
        String password = passwordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check if user entered password
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            focusView = passwordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
        } else if (!RegistrationActivity.isValidEmail(email)) {
            emailView.setError(getString(R.string.error_invalid_email));
            focusView = emailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            //kick off a background task to perform the user login attempt.
            loginTask = new LoginTask(this, progressUiHelper, email, password);
            loginTask.execute((Void) null);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        passwordView.setText("");
        super.onSaveInstanceState(outState);
    }
}


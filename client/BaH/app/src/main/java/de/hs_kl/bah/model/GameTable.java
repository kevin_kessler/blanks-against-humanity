package de.hs_kl.bah.model;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import hs_kl.de.bah.BuildConfig;

/**
 * A table represents a running game.
 */
public class GameTable {

    private static final String TAG = GameTable.class.getSimpleName();


    /**
     * Specifies the role of a user in the table
     */
    public enum TableRole {
        INVITED,
        PLAYING,
        PLAYING_ACTION_NECESSARY,
        PLAYING_NOT_ENOUGH_PLAYERS,
        JUDGING,
        JUDGING_ACTION_NECESSARY,
        JUDGING_NOT_ENOUGH_PLAYERS
    }

    /**
     * The minimum amount of players necessary to start a game.
     */
    public static final int MIN_PLAYERS_TO_START = 3;

    public boolean isValid() {
        // Checking that invites are not players at the same time
        for(User u : pendingUserInvites)
            if (curRound.getPlayers().contains(u)) {
                Log.d(TAG, String.format("GameTable is not valid. User %s is invited and at the same time a player in the current round", u));
                return false;
            }
        return name != null && !name.isEmpty() && curRound.isValid();
    }

    private String _id; //underscore because of mongo db
    private String name;
    private boolean isPrivate;
    private int roundCount;
    /**
     * This time helps to determine if a player is idle for a long time.
     * The timer will be set if a new round starts or if everybody submitted.
     * If a round starts all players except the judge have to submit white cards.
     * If all cards have been submitted the judge has to select the winner cards.
     * If a player does not react for a long time then he will be removed from the table.
     * The threshold will be determined by MAX_IDLE_DAYS_FOR_PLAYERS.
     *
     * See also getIdlePlayers()
     *
     * The time comes from java.util.Date.now()
     */
    private long timeToIndicateIdlePlayers;
    /**
     * The previous round is only used if we want to present a game result to the user.
     */
    private Round prevRound;

    private Round curRound;

    private List<User> pendingUserInvites;

    public GameTable(String _id, String name, boolean isPrivate, Round curRound) {
        this._id = _id;
        this.name = name;
        this.isPrivate = isPrivate;
        this.curRound = curRound;
        roundCount = 0;
        pendingUserInvites = new ArrayList<>();
    }

    public String getId() {
        return _id;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public Round getPreviousRound() {
        return prevRound;
    }

    public void setPreviousRound(Round prevRound) {
        this.prevRound = prevRound;
    }

    public Round getCurrentRound() {
        return curRound;
    }

    public void setCurrentRound(Round curRound) {
        this.curRound = curRound;
    }

    public List<User> getPendingUserInvites() {
        return pendingUserInvites;
    }

    public void setPendingUserInvites(List<User> pendingUserInvites) {
        this.pendingUserInvites = pendingUserInvites;
    }

    public void addPendingUserInvite(User pendingUserInvite) {
        pendingUserInvites.add(pendingUserInvite);
    }

    public void removePendingUserInvite(User pendingUserInvite) {
        pendingUserInvites.remove(pendingUserInvite);
    }

    public static GameTable createFrom(JsonObject jsonObject) {
        // Load all necessary data from json
        String _id = jsonObject.get("_id").getAsString();
        final String _name = jsonObject.get("name").getAsString();
        boolean _isPrivate = jsonObject.get("isPrivate").getAsBoolean();
        int _roundCount = jsonObject.get("roundCount").getAsInt();
        long _timeToIndicateIdlePlayers = getTimeToIndicateIdlePlayersFromJson(jsonObject.get("timeToIndicateIdlePlayers"));

        Round _curRound = Round.createFrom(jsonObject.get("curRound").getAsJsonObject());

        Round _prevRound = jsonObject.has("prevRound") ?
                Round.createFrom(jsonObject.get("prevRound").getAsJsonObject()) :
                null;

        List<User> _pendingUserInvites = new ArrayList<>();
        JsonArray jsonArray = jsonObject.getAsJsonArray("pendingUserInvites");
        for(int i=0; i < jsonArray.size() ;i++) {
            JsonElement pendingInviteElement = jsonArray.get(i);
            User pendingUser = User.createFrom(pendingInviteElement.getAsJsonObject());
            _pendingUserInvites.add(pendingUser);
        }

        // Construct result table
        GameTable resultGameTable = new GameTable(_id, _name, _isPrivate, _curRound);
        resultGameTable.timeToIndicateIdlePlayers = _timeToIndicateIdlePlayers;
        resultGameTable.setRoundCount(_roundCount);
        resultGameTable.setPreviousRound(_prevRound);
        resultGameTable.setPendingUserInvites(_pendingUserInvites);

        return resultGameTable;
    }

    private static long getTimeToIndicateIdlePlayersFromJson(JsonElement jsonElement) {
        if (jsonElement.isJsonObject()) {
            return jsonElement.getAsJsonObject().get("$numberLong").getAsLong();
        } else {
            return jsonElement.getAsLong();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameTable gameTable = (GameTable) o;

        if (_id != null ? !_id.equals(gameTable._id) : gameTable._id != null)
            return false;
        if (name != null ? !name.equals(gameTable.name) : gameTable.name != null)
            return false;
        if (isPrivate != gameTable.isPrivate) return false;
        if (roundCount != gameTable.roundCount) return false;
        if (prevRound != null ? !prevRound.equals(gameTable.prevRound) : gameTable.prevRound != null)
            return false;
        if (curRound != null ? !curRound.equals(gameTable.curRound) : gameTable.curRound != null)
            return false;
        return !(pendingUserInvites != null ? !pendingUserInvites.equals(gameTable.pendingUserInvites) : gameTable.pendingUserInvites != null);
    }

    @Override
    public int hashCode() {
        int result = 31 * (_id != null ? _id.hashCode() : 0);
        result = 31 * result + (isPrivate ? 1 : 0);
        result = 31 * result + roundCount;
        result = 31 * result + (prevRound != null ? prevRound.hashCode() : 0);
        result = 31 * result + (curRound != null ? curRound.hashCode() : 0);
        result = 31 * result + (pendingUserInvites != null ? pendingUserInvites.hashCode() : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public boolean isInvited(String email) {
        for(User u : pendingUserInvites) {
            if ( u.getEmail().equals(email))
                return true;
        }
        return false;
    }

    /**
     * Compares this table with given other table and returns the UpdateType.
     * It will be assumed that this table is the old table and the other table is the new one.
     * This means especially that this round counter has to be equals or smaller than the other.
     * @param other not null GameTable to compare with
     */
    public UpdateType compareAndGetUpdateType(GameTable other, String emailOfPlayer) {

        if (BuildConfig.DEBUG && null == other || ! other.isValid() )
            throw new AssertionError(String.format("No valid table given for comparison %s", other));

        if (BuildConfig.DEBUG && getRoundCount() > other.getRoundCount() )
            throw new AssertionError(String.format("Round counter of old table (%s) is higher than other (%s): %d > %d", this.getId(), other.getId(), this.getRoundCount(), other.getRoundCount() ));


        if (this.getRoundCount() < other.getRoundCount()) {
            if (other.isJudge(emailOfPlayer)) {
                return UpdateType.NEW_ROUND_AS_JUDGE;
            }
            else {
                return UpdateType.NEW_ROUND;
            }
        }

        // Did a user join or leave?
        int thisNumOfPlayers = this.curRound.getNumOfPlayers();
        int otherNumOfPlayers = other.curRound.getNumOfPlayers();
        if (thisNumOfPlayers != otherNumOfPlayers)
            if(thisNumOfPlayers > otherNumOfPlayers)
                return UpdateType.USER_LEFT;
            else
                return UpdateType.USER_JOINED;

        // New cards submitted?
        if (this.curRound.getNumOfSubmittedPlayers() < other.curRound.getNumOfSubmittedPlayers())
            return UpdateType.NEW_CARDS_SUBMITTED;

        return UpdateType.NO_UPDATE;

    }

    public boolean isJudge(String emailOfPlayer) {
        return getCurrentRound().isJudge(emailOfPlayer);
    }


    public List<IngameWhiteCard> getHandFor(User user) {
        return curRound.getHandFor(user);
    }

    public TableRole determineTableRoleOfUser(String emailOfPlayer) {
        if (isInvited(emailOfPlayer))
            return TableRole.INVITED;

        if ( ! getCurrentRound().hasSufficientAmountOfPlayers())
            if (isJudge(emailOfPlayer))
                return TableRole.JUDGING_NOT_ENOUGH_PLAYERS;
            else
                return TableRole.PLAYING_NOT_ENOUGH_PLAYERS;

        if(isJudge(emailOfPlayer)) {
            if (getCurrentRound().allPlayersExceptJudgeSubmittedCards())
                return TableRole.JUDGING_ACTION_NECESSARY;
            else
                return TableRole.JUDGING;
        }

        // User has to be a player
        if (BuildConfig.DEBUG && ! getCurrentRound().isPlayer(emailOfPlayer) )
            throw new AssertionError(String.format("User %s is not a player or an invited user on this table", emailOfPlayer));

        if (getCurrentRound().hasSubmittedCards(emailOfPlayer))
            return TableRole.PLAYING;
        else
            return TableRole.PLAYING_ACTION_NECESSARY;
    }


    public List<IngameWhiteCard> getSubmittedCardsByUser(User user) {
        return curRound.getSubmittedCardsByUser(user);
    }

    public boolean hasAlreadySubmitted(String email) {
        return curRound.hasSubmittedCards(email);
    }
    public boolean hasAlreadySubmitted(User user) {
        return curRound.hasSubmittedCards(user.getEmail());
    }

    public boolean allUsersSubmitted() {
        return curRound.allPlayersExceptJudgeSubmittedCards();
    }

    public boolean isJudge(User player) {
        return curRound.isJudge(player.getEmail());
    }

}

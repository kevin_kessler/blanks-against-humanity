package de.hs_kl.bah.controller;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.DataStorageServicesImp;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.services.DataStorageServices;
import hs_kl.de.bah.BuildConfig;

/**
 * Application context for sharing data between activities.
 * The data should not be edited. In case the data has to be edited, then it should be saved immediately.
 * This shall avoid inconsistency between Data in this class and stored data.
 */
public class BahApplication extends Application {

    private static final String TAG = BahApplication.class.getSimpleName();

    private MainUser mainUser;
    private List<GameTable> gameTables;
    private List<String> idsOfAdvancedGameTables;
    private DataStorageServices mDataStorageServices;

    public MainUser getMainUser() {
        if (null == mainUser)
            reloadMainUser();
        return mainUser;
    }

    public void logOutUser(){
        Log.i(TAG, String.format("Logging out user %s", mainUser));
        mainUser = null;
        gameTables = null;
        mDataStorageServices.deleteAllData();
        ActivityStarter.stopPollService(getApplicationContext());
    }

    public void reloadMainUser() {
        MainUser loadedUser = getDataStorageServices().loadMainUser();

        if (null == loadedUser) {
            mainUser = null;
            return;
        }

        // We don't want to remove an existing user instance
        if (null == mainUser)
            mainUser = loadedUser;
        else
            mainUser.updateWith(loadedUser);
    }

    public List<GameTable> getGameTables() {
        if (null == gameTables)
            reloadGameTables();
        return gameTables;
    }

    public GameTable getGameTable(String tableId) {
        if (null == gameTables)
            reloadGameTables();

        for(GameTable gt : gameTables)
            if(tableId.equals(gt.getId()))
                return gt;

        return reloadGameTable(tableId);
    }

    public void reloadGameTables() {
        final List<GameTable> allGameTables = getDataStorageServices().readAllGameTables();

        if (null == allGameTables) {
            if (null == gameTables)
                gameTables = new ArrayList<>();
            else
                gameTables.clear();
            return;
        }

        gameTables = allGameTables;
    }

    public GameTable reloadGameTable(String tableId) {

        GameTable readGameTable = getDataStorageServices().readGameTable(tableId);

        if (null == readGameTable) {
            leaveTable(tableId);
            return null;
        }
        if (BuildConfig.DEBUG && !readGameTable.isValid())
            throw new AssertionError(String.format("Tried to save not valid table: %s", readGameTable.toString()));

        GameTable previousGameTable = null;
        for(GameTable t : getGameTables()) {
            if (tableId.equals(t.getId())) {
                previousGameTable = t;
                break;
            }
        }
        if (null != previousGameTable)
            if (!getGameTables().remove(previousGameTable)) {
                Log.w(TAG, String.format("Could not delete table %s: %s", previousGameTable.getId(), previousGameTable.getName()) );
            }

        getGameTables().add(readGameTable);

        return readGameTable;
    }

    public boolean leaveTable(String tableId){
        if(null!=gameTables) {
            GameTable tableToRemove = null;
            for (GameTable table : gameTables) {
                if(table.getId().equals(tableId)){
                    tableToRemove = table;
                    break;
                }
            }
            if(null != tableToRemove)
                gameTables.remove(tableToRemove);

            removeIdFromAdvancedGameTableIds(tableId);
        }

       return getDataStorageServices().deleteGameTable(tableId);
    }

    public List<String> getIdsOfAdvancedGameTables(){
        if(null==idsOfAdvancedGameTables || idsOfAdvancedGameTables.isEmpty())
            reloadIdsOfAdvancedGameTables();
        return idsOfAdvancedGameTables;
    }

    public void reloadIdsOfAdvancedGameTables() {
        List<String> loadedIds = getDataStorageServices().loadIdsOfAdvancedGameTables();

        if (null == loadedIds) {
            idsOfAdvancedGameTables = new ArrayList<>();
            return;
        }

        idsOfAdvancedGameTables = loadedIds;
    }

    public void removeIdFromAdvancedGameTableIds(String advancedId){
        if(null!=idsOfAdvancedGameTables && idsOfAdvancedGameTables.contains(advancedId)) {
            idsOfAdvancedGameTables.remove(advancedId);
        }
        boolean removed = getDataStorageServices().removeIdFromAdvancedGameTableIds(advancedId);
        if(removed)
            reloadIdsOfAdvancedGameTables();
    }

    public DataStorageServices getDataStorageServices() {
        if( null == mDataStorageServices)
            // We don't care about parallel access
            mDataStorageServices = new DataStorageServicesImp(this);
        return mDataStorageServices;
    }
}

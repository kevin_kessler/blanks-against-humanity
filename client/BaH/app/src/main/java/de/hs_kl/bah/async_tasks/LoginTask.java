package de.hs_kl.bah.async_tasks;

import android.app.Activity;
import android.widget.Toast;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.DataStorageServicesImp;
import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.services.DataStorageServices;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous login/registration task used to authenticate
 * the user.
 */
public class LoginTask extends BahAsyncTask<MainUser> {

    private final String email;
    private final String password;
    private final NetworkServices netWorkService;

    public LoginTask(Activity parentActivity, ProgressUiHelper progressUiHelper, String email, String password) {
        super(parentActivity, progressUiHelper);
        this.email = email;
        this.password = password;
        this.netWorkService = new NetworkServicesImp(null);
    }

    @Override
    protected MainUser doInBackground(Void... params) {
        return netWorkService.loginUser(email, password);
    }

    @Override
    protected void onPostExecute(final MainUser mainUser) {
        super.onPostExecute(mainUser);

        if (null == mainUser || !mainUser.isValid()) {
            Toast.makeText(parentActivity, netWorkService.getLatestErrorMessage(), Toast.LENGTH_LONG).show();
        } else {
            DataStorageServices dss = new DataStorageServicesImp(parentActivity);
            dss.save(mainUser);
            dss.saveLastLoggedInEmail(mainUser.getEmail());
            ActivityStarter.startCurrentGamesActivity(parentActivity, parentActivity.getApplicationContext());
            parentActivity.finish();
        }
    }

}

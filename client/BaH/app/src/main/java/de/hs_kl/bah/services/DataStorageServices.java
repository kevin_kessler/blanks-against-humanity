package de.hs_kl.bah.services;

import java.util.List;

import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.MainUser;

/**
 * This interface is responsible to load and save user specific data.
 */
public interface DataStorageServices {

    /**
     * Loads the main user which is stored on the device.
     * @return the stored user or null if no user is stored on the device.
     */
    MainUser loadMainUser();

    /**
     * Saves given user to device storage.
     */
    void save(MainUser mainUser);

    /**
     * Is used to load email into login view in case the user logs out and want to log in again.
     */
    String loadLastLoggedInEmail();

    void saveLastLoggedInEmail(String email);

    /**
     * Loads the ids of the tables of which the user did not yet see
     * the result of the game round he submitted cards for.
     * In the meantime the tables advanced to the next round.
     * The round with the result of the previous round is
     * contained in "previousRound" attribute of the table.
     * The ids are read from the devices storage.
     */
    List<String> loadIdsOfAdvancedGameTables();

    /**
     * Saves the set of the given table ids to the devices storage.
     * Those ids belong to the tables of which the user did not yet see
     * the result of the game round he submitted cards for.
     * In the meantime the tables advanced to the next round.
     * The round with the result of the previous round is
     * contained in "previousRound" attribute of the table.
     * The ids are read from the devices storage.
     */
    void saveIdsOfAdvancedGameTables(List<String> idsOfAdvancedGameTables);

    boolean removeIdFromAdvancedGameTableIds(String id);

    void deleteIdsOfAdvancedGameTables();

    /**
     * Deletes all data. Used for logout.
     */
    void deleteAllData();

    void deleteAllGameTables();

    /*
     * Deletes the file of the game table with the given id.
     */
    boolean deleteGameTable(String tableId);

    /**
     * Saves given gametable.
     */
    void save(GameTable newGameTable);

    /**
     * Saves each of the given gametables.
     */
    void save(List<GameTable> newGameTables);

    /**
     * Tries to read a gametable with given tableId.
     * In case such a file does not exist null will be returned.
     */
    GameTable readGameTable(String tableId);

    /**
     * Reads all stored tables.
     */
    List<GameTable> readAllGameTables();
}

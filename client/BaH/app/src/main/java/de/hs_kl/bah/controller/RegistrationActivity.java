package de.hs_kl.bah.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.async_tasks.RegistrationTask;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import hs_kl.de.bah.R;

public class RegistrationActivity extends Activity {

    private static final String REGEX_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern PATTERN_EMAIL = Pattern.compile(REGEX_EMAIL);

    private static final String REGEX_USERNAME = "^[_A-Za-z0-9-]{3,20}$";
    private static final Pattern PATTERN_USERNAME = Pattern.compile(REGEX_USERNAME);

    //at least 6 characters
    private static final String REGEX_PW = "^.{6,}$";
    private static final Pattern PATTERN_PW = Pattern.compile(REGEX_PW);

    public static boolean isValidEmail(final String email)
    {
        if(null == email || email.isEmpty()) return false;
        Matcher matcher = PATTERN_EMAIL.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidUsername(final String username)
    {
        if(null == username || username.isEmpty()) return false;
        Matcher matcher = PATTERN_USERNAME.matcher(username);
        return matcher.matches();
    }

    public static boolean isValidPassword(final String pw)
    {
        if(null == pw || pw.isEmpty()) return false;
        Matcher matcher = PATTERN_PW.matcher(pw);
        return matcher.matches();
    }

    /**
     * Keep track of the registration task to ensure we can cancel it if requested.
     */
    private RegistrationTask registrationTask = null;

    // UI references.
    private EditText usernameView;
    private EditText emailView;
    private EditText passwordView;
    private EditText passwordRepeatView;

    private ProgressUiHelper progressUiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // Set up the login form.
        emailView = (EditText) findViewById(R.id.ETXT_EMAIL_REGISTRATION);
        usernameView = (EditText) findViewById(R.id.ETXT_USERNAME_REGISTRATION);
        passwordView = (EditText) findViewById(R.id.ETXT_PASSWORD_REGISTRATION);
        passwordRepeatView = (EditText) findViewById(R.id.ETXT_PASSWORD_REPEAT_REGISTRATION);
        passwordRepeatView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_SEND || id == EditorInfo.IME_NULL) {
                    register();
                    return true;
                }
                return false;
            }
        });

        Button registerBtn = (Button) findViewById(R.id.BTN_REGISTER_REGISTRATION);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        // Prepare progressUiHelper
        View registrationFormView = findViewById(R.id.REGISTRATION_FORM);
        View progressView = findViewById(R.id.REGISTRATION_PROGRESS);
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        progressUiHelper = new ProgressUiHelper(registrationFormView, progressView, shortAnimTime);
    }

    private void register() {
        if (registrationTask != null && registrationTask.isRunning()) {
            return;
        }

        // Reset errors.
        usernameView.setError(null);
        emailView.setError(null);
        passwordView.setError(null);
        passwordRepeatView.setError(null);

        // Store values at the time of the registration attempt.
        String username = usernameView.getText().toString();
        String email = emailView.getText().toString().toLowerCase();
        String password = passwordView.getText().toString();
        String password_repeat = passwordRepeatView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for passwords matching
        if (TextUtils.isEmpty(password_repeat)) {
            passwordRepeatView.setError(getString(R.string.error_field_required));
            focusView = passwordRepeatView;
            cancel = true;
        } else if(!password_repeat.equals(password)) {
            passwordRepeatView.setError(getString(R.string.error_passwords_dont_match));
            focusView = passwordRepeatView;
            cancel = true;
        }

        // Check for a valid password
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            focusView = passwordView;
            cancel = true;
        } else if(!isValidPassword(password)) {
            passwordView.setError(getString(R.string.error_invalid_password));
            focusView = passwordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
        } else if (!isValidEmail(email)) {
            emailView.setError(getString(R.string.error_invalid_email));
            focusView = emailView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            usernameView.setError(getString(R.string.error_field_required));
            focusView = usernameView;
            cancel = true;
        } else if (!isValidUsername(username)) {
            usernameView.setError(getString(R.string.error_invalid_username));
            focusView = usernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt registration and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            registrationTask = new RegistrationTask(this, progressUiHelper, username, email, password, password_repeat);
            registrationTask.execute((Void) null);
        }
    }

    @Override
    public void onBackPressed() {
        // Since we finished LoginActivity we need to start it again
        ActivityStarter.simpleStartLoginActivity(this, getApplicationContext());
        finish();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        passwordView.setText("");
        passwordRepeatView.setText("");
        super.onSaveInstanceState(outState);
    }

}

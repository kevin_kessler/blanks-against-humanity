/**
 * This package represents the model. The business logic and the data classes are stored here.
 * It should be free of android classed to easily port the application to other platforms.
 */
package de.hs_kl.bah.model;
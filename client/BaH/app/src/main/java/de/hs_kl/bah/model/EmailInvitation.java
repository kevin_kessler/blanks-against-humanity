package de.hs_kl.bah.model;

import android.util.Log;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple email invitation
 */
public class EmailInvitation extends Invitation {

    private static final String TAG = EmailInvitation.class.getSimpleName();

    public static Invitation createFrom(JsonObject jsonObject) {
        // Load all necessary data from json
        if (null == jsonObject || ! jsonObject.has("tableId")
                               || ! jsonObject.has("emailToInvite")) {
            Log.w(TAG, String.format("Invalid jsonObject: %s", jsonObject));
            return null;
        }

        String tableId = jsonObject.get("tableId").getAsString();
        String emailToInvite = jsonObject.get("emailToInvite").getAsString();

        return new EmailInvitation(emailToInvite, tableId);
    }

    private final String emailToInvite;

    public EmailInvitation(String emailToInvite, String tableIdToJoin) {
        super(Invitation.Type.EMAIL, tableIdToJoin);
        this.emailToInvite = emailToInvite;
    }

    public String getEmail() {
        return emailToInvite;
    }

    @Override
    public Map<String, String> getRequestParams() {
        Map<String, String> params = new HashMap<>();

        params.put("command", "table");
        params.put("action", "invite");
        params.put("email_to_invite", emailToInvite);
        params.put("tableId", tableId);

        return params;
    }
}

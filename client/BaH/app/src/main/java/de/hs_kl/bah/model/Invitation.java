package de.hs_kl.bah.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Map;

/**
 * Represents an invitation.
 *
 */
public abstract class Invitation {

    private static final String TAG = Invitation.class.getSimpleName();
    protected static final Gson gson = new Gson();

    /**
     * Returns the corresponding invitation or null if jsonObject is not valid.
     */
    public static Invitation createFrom(JsonObject jsonObject) {
        // Load all necessary data from json
        if (null == jsonObject || ! jsonObject.has("type")) {
            Log.w(TAG, String.format("Invalid jsonObject: %s",jsonObject));
            return null;
        }

        switch(jsonObject.get("type").getAsString()) {
            case "email":
                return EmailInvitation.createFrom(jsonObject);
            default:
                Log.w(TAG, String.format("Invalid jsonObject: %s",jsonObject));
                return null;
        }
    }

    public abstract Map<String,String> getRequestParams();

    public enum Type {
        EMAIL
    }

    private Type type;
    protected String tableId;

    protected Invitation(Type type, String tableId) {
        this.type = type;
        this.tableId = tableId;
    }

    public Type getType() {
        return type;
    }

    public String getAsJson() {
        return gson.toJson(this).toString();
    }

}

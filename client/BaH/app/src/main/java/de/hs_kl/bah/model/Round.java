package de.hs_kl.bah.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hs_kl.de.bah.BuildConfig;

/**
 * A round represents a game round on a table.
 * In a game round one user is the judge and all other users have to select cards.
 * A round ends as soon as all users submitted their cards and the judge chosen the winner card(s).
 */
public class Round {

    private User judge;

    private BlackCard blackCard;

    private Map<User, List<IngameWhiteCard>> playersWithHand;

    /**
     * The ids of the winning cards. Order in array represents the submit order.
     * Example: [2,6]
     * The first submitted card has the id 2. The second card has id 6.
     * The winning cards are (in this order) card 2 and card 6.
     */
    private int[] winnerCards;

    public Round(Map<User, List<IngameWhiteCard>> playersWithHand, User judge, BlackCard blackCard) {
        this.judge = judge;
        this.blackCard = blackCard;
        this.playersWithHand = playersWithHand;
        this.winnerCards = null;
    }

    public Round(Map<User, List<IngameWhiteCard>> playersWithHand, User judge, BlackCard blackCard, int[] winnerCards) {
        this(playersWithHand, judge, blackCard);
        this.winnerCards = winnerCards;
    }

    public List<User> getPlayers() {
        return new ArrayList<>(playersWithHand.keySet());
    }

    public void addPlayer(User player, List<IngameWhiteCard> hand){
        playersWithHand.put(player, hand);
    }

    public Map<User, List<IngameWhiteCard>> getPlayersWithHand(){
        return playersWithHand;
    }

    public User getJudge() {
        return judge;
    }

    public void setJudge(User judge) {
        this.judge = judge;
    }

    public BlackCard getBlackCard() {
        return blackCard;
    }

    public int[] getWinnerCards() {
        return winnerCards;
    }

    public void setWinnerCards(int[] winnerCards) {
        this.winnerCards = winnerCards;
    }

    public boolean hasSufficientAmountOfPlayers(){
        return getPlayersWithHand().keySet().size() >= GameTable.MIN_PLAYERS_TO_START;
    }

    public static Round createFrom(JsonObject jsonObject) {
        User _judge = User.createFrom(jsonObject.get("judge").getAsJsonObject());

        BlackCard _blackCard = BlackCard.createFrom(jsonObject.get("blackCard").getAsJsonObject());

        // Map<User, List<IngameWhiteCard>>
        Map<User, List<IngameWhiteCard>> _playerWithCards= new HashMap<>();
        JsonObject playerWithCardsAsJson = jsonObject.get("playersWithHand").getAsJsonObject();
        for(Map.Entry<String,JsonElement> playerWhiteCardsPair : playerWithCardsAsJson.entrySet()) {
            User user = User.createFrom(playerWhiteCardsPair.getKey());
            List<IngameWhiteCard> ingameCardsOfUser = new ArrayList<>();
            JsonArray cardsArray = playerWhiteCardsPair.getValue().getAsJsonArray();
            for(int i=0; i < cardsArray.size() ;i++) {
                JsonElement cardElement = cardsArray.get(i);
                IngameWhiteCard card = IngameWhiteCard.createFrom(cardElement.getAsJsonObject());
                ingameCardsOfUser.add(card);
            }
            _playerWithCards.put(user, ingameCardsOfUser);
        }

        int[] _winnerCards = null;
        if (jsonObject.has("winnerCards")) {
            JsonArray cardsArray = jsonObject.get("winnerCards").getAsJsonArray();
            _winnerCards = new int[cardsArray.size()];
            for(int i=0; i < cardsArray.size() ;i++) {
                _winnerCards[i] = cardsArray.get(i).getAsInt();
            }
        }

        return new Round(_playerWithCards, _judge, _blackCard, _winnerCards);
    }

    public List<String> getEmailsOfWinners() {
        List<String> winnerEmails = new ArrayList<>();

        // For every player ...
        for (User player : getPlayers()) {
            // ... except the judge ...
            if (judge.equals(player))
                continue;

            boolean hasWinningHand = hasWinningHand(player);

            // ... and if yes remember the winner.
            if (hasWinningHand) {
                winnerEmails.add(player.getEmail());
            }
        }

        return winnerEmails;
    }

    /**
     * Determines if the user has a winning hand. Therefore the user needs to have all winning cards
     * submitted in correct order.
     * @param player
     * @return true if the user has a winning hand
     */
    public boolean hasWinningHand(User player) {
        if(null == player || null == winnerCards || winnerCards.length < 1)
            return false;

        List<IngameWhiteCard> playersHand = getPlayersWithHand().get(player);
        for (int i=0; i < winnerCards.length ;++i) {
            boolean winCardIsWithCorrectOrderInPlayerssHand = false;
            for(IngameWhiteCard c : playersHand) {
                if (winnerCards[i] == c.getId()) {
                    // c is a winning card but has it also the correct submit order?
                    if (c.getSubmittedOrder() == i+1) {
                        winCardIsWithCorrectOrderInPlayerssHand = true;
                    }
                }
            }
            if ( ! winCardIsWithCorrectOrderInPlayerssHand)
                return false;
        }
        return true;
    }

    /**
     * Searches the winner cards as ingamewhitecard list by using the Ids of the winnercards[] int array
     * and iterating through each players hand.
     * @return The winner cards as List of IngameWhiteCards
     */
    public List<IngameWhiteCard> getWinnerCardsAsList(){
        if(BuildConfig.DEBUG && (winnerCards == null || winnerCards.length < 1))
            throw new AssertionError("Winnercards not set in this round");

        List<IngameWhiteCard> winnerCardList = new ArrayList<>();
        for(Map.Entry<User, List<IngameWhiteCard>> playerWithHand : playersWithHand.entrySet()){
            for(IngameWhiteCard handCard : playerWithHand.getValue()){
                for (int winnerCard : winnerCards) {
                    if (winnerCard == handCard.getId() && handCard.getSubmittedOrder()>IngameWhiteCard.NOT_SUBMITTED) {
                        winnerCardList.add(handCard);
                        break;
                    }
                }
                if(winnerCardList.size() == winnerCards.length)
                    return winnerCardList;
            }
        }

        throw new RuntimeException("Could not find winner cards among the players hands");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Round round = (Round) o;

        if (playersWithHand != null ? !playersWithHand.equals(round.playersWithHand) : round.playersWithHand != null)
            return false;
        if (judge != null ? !judge.equals(round.judge) : round.judge != null) return false;
        if (blackCard != null ? !blackCard.equals(round.blackCard) : round.blackCard != null)
            return false;
        return !(winnerCards != null ? Arrays.equals(winnerCards, round.winnerCards) : round.winnerCards != null);

    }

    @Override
    public int hashCode() {
        int result = playersWithHand != null ? playersWithHand.hashCode() : 0;
        result = 31 * result + (judge != null ? judge.hashCode() : 0);
        result = 31 * result + (blackCard != null ? blackCard.hashCode() : 0);
        result = 31 * result + (winnerCards != null ? winnerCards.hashCode() : 0);
        return result;
    }

    public boolean isJudge(String emailOfPlayer) {
        return judge.getEmail().equals(emailOfPlayer);
    }

    public boolean allPlayersExceptJudgeSubmittedCards() {
        for(User player : getPlayers()) {
            if (!hasSubmittedCards(player))
                return false;
        }
        return true;
    }
    public boolean hasSubmittedCards(User player) {
        return judge.equals(player) || blackCard.getNumOfBlanks() == getNumOfSubmittedCards(player);
    }

    private int getNumOfSubmittedCards(User user) {
        int counter = 0;
        for(IngameWhiteCard card : playersWithHand.get(user))
            if (card.isSubmitted())
                counter++;
        return counter;
    }

    public boolean isPlayer(String emailOfPlayer) {
        boolean isInPlayerList = false;

        for (User u : getPlayers()) {
            if (u.getEmail().equals(emailOfPlayer)) {
                isInPlayerList = true;
                break;
            }
        }
        return ! isJudge(emailOfPlayer) && isInPlayerList;
    }

    public boolean hasSubmittedCards(String emailOfPlayer) {
        return hasSubmittedCards(getPlayerByEmail(emailOfPlayer));
    }

    private User getPlayerByEmail(String emailOfPlayer) {
        for(User u : getPlayers()) {
            if (u.getEmail().equals(emailOfPlayer))
                return u;
        }
        return null;
    }

    public boolean isValid() {
        return judge != null && judge.isValid() && blackCard.isValid() && 0 < playersWithHand.size();
    }

    public List<IngameWhiteCard> getHandFor(User user) {
        return playersWithHand.get(user);
    }
    public List<IngameWhiteCard> getSubmittedCardsByUser(User user){
        if(isJudge(user.getEmail()))
            return null;

        List<IngameWhiteCard> userHand = null;
        for(Map.Entry<User, List<IngameWhiteCard>> playerWithHand : playersWithHand.entrySet())
            if (playerWithHand.getKey().getEmail().equals(user.getEmail())) {
                userHand = playerWithHand.getValue();
            }

        List<IngameWhiteCard> submittedCards = new ArrayList<>();

        for(IngameWhiteCard card : userHand)
            if(card.isSubmitted())
                submittedCards.add(card);

        return submittedCards;
    }


    public int getNumOfSubmittedPlayers() {
        int counter = 0;
        for(Map.Entry<User, List<IngameWhiteCard>> playerWithHand : playersWithHand.entrySet()) {
            if ( ! isJudge(playerWithHand.getKey().getEmail()))
                if (hasAtLeastOneSubmittedCard(playerWithHand.getValue()))
                    counter++;
        }
        return counter;
    }

    private boolean hasAtLeastOneSubmittedCard(List<IngameWhiteCard> usersCards) {
        for(IngameWhiteCard card : usersCards)
            if (card.isSubmitted())
                return true;
        return false;
    }

    public int getNumOfPlayers() {
        return playersWithHand.size();
    }
}

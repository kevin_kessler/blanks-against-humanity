package de.hs_kl.bah.controller.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.hs_kl.bah.android_services.ServerPollService;
import de.hs_kl.bah.async_tasks.BahRunner;
import hs_kl.de.bah.BuildConfig;

/**
 * This receiver gets a list of updated game table ids from ServerPollService.
 */
public class GameTableUpdateReceiver extends BroadcastReceiver
{
    BahRunner<String[]> bahRunner;
    private boolean aNewRoundStarted;

    public GameTableUpdateReceiver(BahRunner<String[]> bahRunner) {

        if (BuildConfig.DEBUG && null == bahRunner)
            throw new AssertionError("BahRunner may not be null");

        this.bahRunner = bahRunner;
    }

    public boolean aNewRoundHasStarted() {
        return aNewRoundStarted;
    }

    public void onReceive(Context context, Intent intent)
    {
        String[] tablIds = intent.getStringArrayExtra(ServerPollService.GAME_TABLE_IDS);

        if (intent.hasExtra(ServerPollService.A_NEW_ROUND_STARTED))
            aNewRoundStarted = true;
        else
            aNewRoundStarted = false;

        bahRunner.run(tablIds);
    }


}

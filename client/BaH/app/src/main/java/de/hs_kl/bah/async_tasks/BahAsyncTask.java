package de.hs_kl.bah.async_tasks;

import android.app.Activity;
import android.os.AsyncTask;

import de.hs_kl.bah.controller.util.ProgressUiHelper;

public abstract class BahAsyncTask<T> extends AsyncTask<Void, Void, T> {

    protected Activity parentActivity;
    protected ProgressUiHelper progressUiHelper;
    protected boolean isRunning;

    public BahAsyncTask(Activity parentActivity, ProgressUiHelper progressUiHelper){
        this.parentActivity = parentActivity;
        this.progressUiHelper = progressUiHelper;
        isRunning = false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isRunning = true;
        if (progressUiHelper != null)
            progressUiHelper.showProgress(true);
    }

    @Override
    protected void onPostExecute(final T result) {
        isRunning = false;
        if (progressUiHelper != null)
            progressUiHelper.showProgress(false);
    }

    @Override
    protected void onCancelled() {
        isRunning = false;
        progressUiHelper.showProgress(false);
    }

    public boolean isRunning(){
        return isRunning;
    }

}

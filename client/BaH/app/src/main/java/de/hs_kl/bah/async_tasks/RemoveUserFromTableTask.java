package de.hs_kl.bah.async_tasks;

import android.widget.Toast;

import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous "remove a user from a table" task used to remove a user from a game table
 */
public class RemoveUserFromTableTask extends BahAsyncTask<Boolean>  {

    private final BahActivity parentActivity;
    private final NetworkServices netWorkService;
    private final String tableId;
    private final String email;

    public RemoveUserFromTableTask(BahActivity parentActivity, ProgressUiHelper progressUiHelper,  String email, String tableId) {
        super(parentActivity, progressUiHelper);
        this.parentActivity = parentActivity;
        this.netWorkService = new NetworkServicesImp(parentActivity.getBahApplication());
        this.tableId = tableId;
        this.email = email;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return netWorkService.removeUserFromGameTable(email, tableId);
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        super.onPostExecute(success);

        if (!success) {
            Toast.makeText(parentActivity, netWorkService.getLatestErrorMessage(), Toast.LENGTH_LONG).show();
        }
    }
}

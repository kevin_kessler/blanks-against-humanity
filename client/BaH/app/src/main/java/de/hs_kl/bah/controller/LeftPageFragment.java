package de.hs_kl.bah.controller;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hs_kl.bah.model.IngameWhiteCard;
import hs_kl.de.bah.R;

/**
 * The "hand" part of the GameActivity.
 * Responsible for displaying the white cards of the mainusers hand.
 * Also used to select / unselect white cards and then submitting them to the server.
 */
public class LeftPageFragment extends GamePageFragment{
    private ListView leftListView;
    private ListView rightListView;
    private Button submitButton;
    private List<IngameWhiteCard> selectedCards;

    public List<IngameWhiteCard> getSelectedCards(){
        return selectedCards;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_game_left_page, container, false);
        leftListView = (ListView) rootView.findViewById(R.id.LV_GAME_LEFTPAGE_WHITECARDS_LEFT);
        rightListView = (ListView) rootView.findViewById(R.id.LV_GAME_LEFTPAGE_WHITECARDS_RIGHT);
        submitButton = (Button) rootView.findViewById(R.id.BTN_SUBMIT_WHITE_CARDS);
        selectedCards = new ArrayList<>();

        //split users cards into two lists (left and right)
        List<IngameWhiteCard> userHand = getGameActivity().getHandOfMainUser();
        List<IngameWhiteCard> leftCards = new ArrayList<>();
        List<IngameWhiteCard> rightCards = new ArrayList<>();
        for (int i = 0; i < userHand.size(); i++) {
            IngameWhiteCard cardToAdd = userHand.get(i);
            if(i%2 ==0)
                leftCards.add(cardToAdd);
            else
                rightCards.add(cardToAdd);
        }

        // init adapters for both lists and set them to the listviews that are responsible for displaying them
        UserHandListAdapter leftAdapter = new UserHandListAdapter(getGameActivity(), this, leftCards);
        UserHandListAdapter rightAdapter = new UserHandListAdapter(getGameActivity(), this, rightCards);
        leftListView.setAdapter(leftAdapter);
        rightListView.setAdapter(rightAdapter);

        // if the user already submitted cards in this round, or if he is the judge, he is not able to select / deselect cards and not able to submit anymore
        boolean alreadySubmitted = getActiveRound().hasSubmittedCards(getMainUser().getEmail());
        boolean isJudge = getActiveRound().isJudge(getMainUser().getEmail());
        if(alreadySubmitted || isJudge){
            if(alreadySubmitted) {
                //set selected cards to the cards that where already submitted (in submission order)
                selectedCards = getGameActivity().getSubmittedCardsSortedBySubmissionOrder(userHand);
            }

            //'disable' submit button -> instead of submitting, only point to submission page
            submitButton.setVisibility(View.VISIBLE);
            submitButton.setText(isJudge ? getResources().getString(R.string.game_you_are_judge) : getResources().getString(R.string.game_you_already_submitted));
            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getGameActivity().swipeToRight(null);
                }
            });
        }

        return rootView;
    }

    public int getNumOfSelectedCards(){
        return selectedCards.size();
    }

    public void addCardToSelection(IngameWhiteCard card){
        int numOfBlanks = getActiveRound().getBlackCard().getNumOfBlanks();
        if(getNumOfSelectedCards() < numOfBlanks)
            selectedCards.add(card);

        //show submit button if enough cards are selected
        if(getNumOfSelectedCards() == numOfBlanks)
            submitButton.setVisibility(View.VISIBLE);
    }

    public void removeCardFromSelection(IngameWhiteCard card){
        if(cardIsSelected(card)) {
            selectedCards.remove(card);
            submitButton.setVisibility(View.GONE);
        }
    }

    public boolean cardIsSelected(IngameWhiteCard card){
        return selectedCards.contains(card);
    }

    public int getIndexOfSelectedCard(IngameWhiteCard card){
        return selectedCards.indexOf(card);
    }

    @Override
    public String getTitle(){
        return getGameActivity().getResources().getString(R.string.game_left_page);
    }

    /**
     * Responsible for initing whitecard items of a certain listview.
     * This is used for the mainusers hand of cards as well as for the white cards displayed in the right page fragment (submissions).
     */
    public static class UserHandListAdapter extends ArrayAdapter<IngameWhiteCard> {
        private final BahActivity context;
        private final GamePageFragment fragment;
        private final List<IngameWhiteCard> cardsToShow;

        public UserHandListAdapter(BahActivity context, GamePageFragment fragment, List<IngameWhiteCard> cardsToShow) {
            super(context, R.layout.listitem_ingame_whitecard, cardsToShow);
            this.context = context;
            this.cardsToShow = cardsToShow;
            this.fragment = fragment;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {

            //set text of white card
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.listitem_ingame_whitecard, null, true);
            TextView txtTitle = (TextView) rowView.findViewById(R.id.TXT_LISTITEM_HANDCARD_CONTENT);
            txtTitle.setText(cardsToShow.get(position).getText());

            //show selection number if in the selection fragment (left page) and init onclick listener
            if(fragment instanceof LeftPageFragment) {
                final LeftPageFragment leftFragment = (LeftPageFragment)fragment;
                final RelativeLayout circleHolder = (RelativeLayout) rowView.findViewById(R.id.RL_LISTITEM_HANDCARD_CIRCLE_FORM);
                final TextView circleText = (TextView) rowView.findViewById(R.id.TXT_LISTITEM_HANDCARD_CIRCLE_NUMBER);
                final IngameWhiteCard currentCard = cardsToShow.get(position);
                boolean currentCardIsSelected =  leftFragment.cardIsSelected(currentCard);
                if(currentCardIsSelected) {
                    circleHolder.setVisibility(View.VISIBLE);
                    circleText.setText(String.valueOf(leftFragment.getIndexOfSelectedCard(currentCard)+1));
                }else{
                    circleHolder.setVisibility(View.INVISIBLE);
                    circleText.setText(String.valueOf(0));
                }

                // select / deselect white card on click (if user is not judge)
                RelativeLayout cardContainer = (RelativeLayout)rowView.findViewById(R.id.RL_LISTITEM_HANDCARD);
                if(!leftFragment.getGameActivity().mainUserIsJudge()) {
                    cardContainer.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            boolean currentCardIsSelected = leftFragment.cardIsSelected(currentCard);
                            //deselect
                            if (currentCardIsSelected && !leftFragment.getActiveRound().hasSubmittedCards(leftFragment.getMainUser().getEmail())) {
                                leftFragment.removeCardFromSelection(currentCard);
                                circleHolder.setVisibility(View.INVISIBLE);

                                //update other circletexts, including the ones of the other listview
                                int circleNumOfDeselected = Integer.parseInt(circleText.getText().toString());
                                ListView leftListView = (ListView) parent.getRootView().findViewById(R.id.LV_GAME_LEFTPAGE_WHITECARDS_LEFT);
                                ListView rightListView = (ListView) parent.getRootView().findViewById(R.id.LV_GAME_LEFTPAGE_WHITECARDS_RIGHT);
                                List<View> cardViews = getAllViewsOf(leftListView);
                                cardViews.addAll(getAllViewsOf(rightListView));
                                for (View cardView : cardViews) {
                                    TextView childCircleText = (TextView) cardView.findViewById(R.id.TXT_LISTITEM_HANDCARD_CIRCLE_NUMBER);
                                    int currNum = Integer.parseInt(childCircleText.getText().toString());
                                    if (currNum > circleNumOfDeselected) {
                                        childCircleText.setText(String.valueOf(currNum - 1));
                                    }
                                }
                            }

                            //select only if number of selected cards doesnt exceed number of available blanks
                            else if (leftFragment.getNumOfSelectedCards() < fragment.getGameActivity().getActiveRound().getBlackCard().getNumOfBlanks()) {
                                leftFragment.addCardToSelection(currentCard);
                                circleText.setText(String.valueOf(leftFragment.getNumOfSelectedCards()));
                                circleHolder.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }//end if mainuser != judge
            }//end if fragment instanceof LeftPageFragment

            //indicate submission order in case we are showing white cards in the submission view (right page fragment)
            else if(fragment instanceof  RightPageFragment){
                IngameWhiteCard currentCard = cardsToShow.get(position);
                if(currentCard.isSubmitted()){
                    RelativeLayout circleHolder = (RelativeLayout) rowView.findViewById(R.id.RL_LISTITEM_HANDCARD_LITTLE_CIRCLE_FORM);
                    circleHolder.setVisibility(View.VISIBLE);
                    TextView circleText = (TextView)rowView.findViewById(R.id.TXT_LISTITEM_HANDCARD_LITTLE_CIRCLE_NUMBER);
                    circleText.setText(String.valueOf(currentCard.getSubmittedOrder()));
                }
            }


            return rowView;
        }

        public List<View> getAllViewsOf(ListView lv){
            List<View> views = new ArrayList<>();
            for(int i=0; i<lv.getChildCount(); i++)
                views.add(lv.getChildAt(i));
            return views;
        }
    }
}

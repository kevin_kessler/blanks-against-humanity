package de.hs_kl.bah.android_services;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hs_kl.bah.async_tasks.AsyncTaskResult;
import de.hs_kl.bah.controller.BahApplication;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.Invitation;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.services.DataStorageServices;
import de.hs_kl.bah.services.NetworkServices;
import hs_kl.de.bah.BuildConfig;

/**
 * Implements NetworkServices with android specific packages.
 */
public class NetworkServicesImp implements NetworkServices {

    private static final String TAG = NetworkServicesImp.class.getSimpleName();
    public static int RESPONSE_CODE_OK = 200; //TODO: create lib shared with the errorcodes from server
    /** In case if the communication to the server was not successful. */
    public static int ERROR_CODE_BAD_REQUEST = -1;
    public static String ERROR_MSG_BAD_REQUEST = "Bad request, check internet connection.";

    //local server
//    private final String HOST = "192.168.178.31";
//    private final String PORT = "8080";

    //server escher
    //private final String HOST = "escher.informatik.fh-kl.de";
    //private final String PORT = "80";

    //server bah
    private final String HOST = "143.93.17.81";
    private final String PORT = "8080";

    private final String BASE_URL = String.format("http://%s:%s/bah_server/index", HOST, PORT);
    private String latestErrorMessage;
    private BahApplication bahApp;
    private DataStorageServices dataSServices;

    public NetworkServicesImp(BahApplication bahApp) {
        this.bahApp = bahApp;
        dataSServices = new DataStorageServicesImp(getAppContext());
    }

    public BahApplication getAppContext() {
        return bahApp;
    }

    public void setAppContext(BahApplication appContext) {
        this.bahApp = appContext;
    }

    @Override
    public String getLatestErrorMessage()
    {
        return latestErrorMessage;
    }

    private void setLatestErrorMessage(String errorMessage)
    {
        latestErrorMessage = errorMessage;
    }

    @Override
    public MainUser registerUser(String username, String email, String password, String password_repeat) {

        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "register");
        parameters.put("username", username);
        parameters.put("email", email.toLowerCase());
        parameters.put("pwd", password);
        parameters.put("pwdrepeat", password_repeat);
        AbstractMap.SimpleEntry<Integer, String> registerResponse = doPostRequest(BASE_URL, parameters);

        //handle response
        int responseCode = registerResponse.getKey();
        String responseMessage = registerResponse.getValue();

        if(responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);
            return null;
        }

        //create mainuser object from response
        JsonObject jsonUser = jsonStringToObject(responseMessage);
        jsonUser.addProperty("pwd", password);
        return MainUser.createFrom(jsonUser);
    }

    @Override
    public MainUser loginUser(String email, String password) {

        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "login");
        parameters.put("email", email.toLowerCase());
        parameters.put("pwd", password);
        AbstractMap.SimpleEntry<Integer, String> loginResponse = doPostRequest(BASE_URL, parameters);

        //handle response
        int responseCode = loginResponse.getKey();
        String responseMessage = loginResponse.getValue();

        if(responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);
            return null;
        }

        //create mainuser object from response
        JsonObject jsonUser = jsonStringToObject(responseMessage);
        jsonUser.addProperty("pwd", password);
        return MainUser.createFrom(jsonUser);
    }

    @Override
    public AsyncTaskResult<GameTable> createGameTable(String tableName, boolean isPrivate){
        if(null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");


        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "create");
        parameters.put("email", bahApp.getMainUser().getEmail());
        parameters.put("tablename", tableName);
        parameters.put("private", isPrivate ? "true" : "false");
        AbstractMap.SimpleEntry<Integer, String> response = doPostRequest(BASE_URL, parameters);

        //handle response
        int responseCode = response.getKey();
        String responseMessage = response.getValue();

        if(responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);
            return new AsyncTaskResult<>(responseCode, responseMessage, null);
        }

        //create gametable object from response
        JsonObject jsonTable = jsonStringToObject(responseMessage);
        GameTable tableToJoin = GameTable.createFrom(jsonTable);
        return new AsyncTaskResult<>(RESPONSE_CODE_OK, "", tableToJoin);
    }

    @Override
    public GameTable getAndSaveGameTable(String tableId) {

        if (null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");


        // Send request
        Map<String, String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "gettable");
        parameters.put("email", bahApp.getMainUser().getEmail());
        parameters.put("tableId", tableId);
        AbstractMap.SimpleEntry<Integer, String> response = doPostRequest(BASE_URL, parameters);

        // Handle response
        int responseCode = response.getKey();
        String responseMessage = response.getValue();

        if (responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);

            if (responseCode != ERROR_CODE_BAD_REQUEST) {
                // Delete also table from device since it seems the table das not exists (at least for current user)
                dataSServices.deleteGameTable(tableId);
                bahApp.reloadGameTable(tableId);
            }


            return null;
        }

        // Create GameTable object from response
        JsonObject jsonTable = jsonStringToObject(responseMessage);
        final GameTable gameTable = GameTable.createFrom(jsonTable);

        if (BuildConfig.DEBUG && null == gameTable)
            throw new AssertionError("GameTable is null but should not be: tableId="+tableId);

        dataSServices.save(gameTable);
        bahApp.reloadGameTable(gameTable.getId());

        return gameTable;
    }


    @Override
    public boolean getAndSaveAllTables() {
        if (null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");


        // Send request
        Map<String, String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "getall");
        parameters.put("email", bahApp.getMainUser().getEmail());
        AbstractMap.SimpleEntry<Integer, String> response = doPostRequest(BASE_URL, parameters);

        // Handle response
        int responseCode = response.getKey();
        String responseMessage = response.getValue();

        if (responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);
            return false;
        }

        // Create GameTable object from response
        JsonObject jsonObject = jsonStringToObject(responseMessage);

        if ( ! jsonObject.has("tables") ||  ! jsonObject.get("tables").isJsonArray()) {
            String errMSg = String.format("Returned object is not a JsonArray: %s", jsonObject.toString());
            Log.e(TAG, errMSg);
            setLatestErrorMessage(errMSg);
            return false;
        }

        List<GameTable> newGameTables = new ArrayList<>();
        final JsonArray jsonArray = jsonObject.get("tables").getAsJsonArray();
        for(int i=0; i < jsonArray.size() ;i++) {
            JsonElement tableElement = jsonArray.get(i);
            GameTable gameTable = GameTable.createFrom(tableElement.getAsJsonObject());
            if (null == gameTable || ! gameTable.isValid()) {
                String errMSg = String.format("Could not GameTable from JsonElement %s. \n Skipping this table. ", tableElement.toString());
                Log.w(TAG, errMSg);
                setLatestErrorMessage(errMSg);
                continue;
            }
            newGameTables.add(gameTable);
        }

        //delete old game tables and save the new ones
        dataSServices.deleteAllGameTables();
        dataSServices.save(newGameTables);

        return true;
    }

    public AsyncTaskResult<GameTable> joinRandomGameTable(){
        if(null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");

        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "joinrandomly");
        parameters.put("email", bahApp.getMainUser().getEmail());
        AbstractMap.SimpleEntry<Integer, String> response = doPostRequest(BASE_URL, parameters);

        //handle response
        int responseCode = response.getKey();
        String responseMessage = response.getValue();

        if(responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);
            return new AsyncTaskResult<>(responseCode, responseMessage, null);
        }

        //create gametable object from response
        JsonObject jsonTable = jsonStringToObject(responseMessage);
        GameTable tableToJoin = GameTable.createFrom(jsonTable);
        return new AsyncTaskResult<>(RESPONSE_CODE_OK, "", tableToJoin);
    }

    public boolean removeUserFromGameTable(String email, String gameTableId){
        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "removeuser");
        parameters.put("email", email);
        parameters.put("tableId", gameTableId);
        AbstractMap.SimpleEntry<Integer, String> response = doPostRequest(BASE_URL, parameters);

        //handle response
        int responseCode = response.getKey();
        String responseMessage = response.getValue();

        if(responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);
            return false;
        }

        return true;
    }

    @Override
    public boolean inviteUser(Invitation invitation) {

        //send request
        Map<String,String> parameters = invitation.getRequestParams();

        AbstractMap.SimpleEntry<Integer, String> loginResponse = doPostRequest(BASE_URL, parameters);

        //handle response
        int responseCode = loginResponse.getKey();
        String responseMessage = loginResponse.getValue();

        if(responseCode != RESPONSE_CODE_OK) {
            setLatestErrorMessage(responseMessage);
            return false;
        }

        return true;
    }

    @Override
    public AsyncTaskResult<GameTable> acceptInvitation(String tableId) {
        if(null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");

        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "accept_invitation");
        parameters.put("email_of_invitation", bahApp.getMainUser().getEmail());
        parameters.put("tableId", tableId);

        return executeUpdateTableOperationAndSaveTable(BASE_URL, parameters);
    }

    @Override
    public AsyncTaskResult<Boolean> declineInvitation(String tableId) {
        Log.i(TAG, String.format("Successfully declined invitation in table %s", tableId));
        if(null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");

        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "decline_invitation");
        parameters.put("email_of_invitation", bahApp.getMainUser().getEmail());
        parameters.put("tableId", tableId);

        // General request
        AbstractMap.SimpleEntry<Integer, String> response = doPostRequest(BASE_URL, parameters);

        // Handle response
        int responseCode = response.getKey();
        String responseMessage = response.getValue();

        if(responseCode == RESPONSE_CODE_OK) {
            Log.i(TAG, String.format("Successfully declined invitation in table %s", tableId));
            dataSServices.deleteGameTable(tableId);
            bahApp.reloadGameTable(tableId);
            return new AsyncTaskResult<>(true);
        }

        Log.i(TAG, String.format("Declining invitation for table %s failed. Error message: %s", tableId, responseMessage));

        setLatestErrorMessage(responseMessage);

        if (responseCode != ERROR_CODE_BAD_REQUEST) {
            // An error occurred, try to fetch up-to-date GameTable
            getAndSaveGameTable(tableId);
        }

        return new AsyncTaskResult<>(responseCode, responseMessage, false);
    }

    public GameTable submitSelectedCardsToTable(int[] cardIdsToSubmit, String tableId){
        if(null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");

        //build card ids string
        String cardIdString = buildCardSubmissionString(cardIdsToSubmit);

        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "submit");
        parameters.put("tableId", tableId);
        parameters.put("email", bahApp.getMainUser().getEmail());
        parameters.put("whitecards", cardIdString);

        return executeUpdateTableOperationAndSaveTable(BASE_URL, parameters).getResultParameter();
    }

    public GameTable submitWinnerCardsToTable(int[] cardIdsToSubmit, String tableId){
        if(null == bahApp)
            throw new AssertionError("BahApplication is required for this action!");

        //build card ids string
        String cardIdString = buildCardSubmissionString(cardIdsToSubmit);

        //send request
        Map<String,String> parameters = new HashMap<>();
        parameters.put("command", "table");
        parameters.put("action", "judge");
        parameters.put("tableId", tableId);
        parameters.put("email", bahApp.getMainUser().getEmail());
        parameters.put("whitecards", cardIdString);

        return executeUpdateTableOperationAndSaveTable(BASE_URL, parameters).getResultParameter();
    }

    /************************************HELPER FUNCTIONS*********************************************/

    private JsonObject jsonStringToObject(String jsonString)
    {
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        return element.getAsJsonObject();
    }

    private String buildCardSubmissionString(int[] cardIdsToSubmit){
        StringBuilder cardIdBuilder = new StringBuilder();
        for(int i = 0; i<cardIdsToSubmit.length; i++){
            cardIdBuilder.append(String.valueOf(cardIdsToSubmit[i]));
            if(i+1 < cardIdsToSubmit.length)
                cardIdBuilder.append(";");
        }
        return cardIdBuilder.toString();
    }

    private AbstractMap.SimpleEntry<Integer, String> doPostRequest(final String urlstring, final Map<String,String> parameters)
    {
        return doPostRequest(urlstring, parameters, null);
    }

    private AbstractMap.SimpleEntry<Integer, String> doPostRequest(final String urlstring, final Map<String,String> parameters, final Map<String,String> cookies)
    {
        //Build parameter query
        final String paramChain = mapToChainedString(parameters, "=", "&");

        Log.d(TAG, String.format("Sending Http request to %s with parameters %s", urlstring, paramChain));

        //Build cookie string
        String cookieChain = null;
        if(null != cookies) {
            cookieChain = mapToChainedString(cookies, "=", ";");
        }

        //send post request
        HttpURLConnection connection = null;
        BufferedInputStream responseStream = null;
        String responseMessage = null;
        int responseCode = 0;
        try {

            //open connection
            URL url = new URL(urlstring);
            connection = (HttpURLConnection)url.openConnection();

            //insert cookies
            if(null != cookieChain)
                connection.setRequestProperty("Cookie", cookieChain);

            //insert post parameters and send request
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setReadTimeout(10000);
            Writer writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(paramChain);
            writer.flush();
            writer.close();

            //retrieve response. inputStream throws io exception when server responds with an errorcode
            responseCode = connection.getResponseCode();
            responseStream = new BufferedInputStream(connection.getInputStream());

        } catch (MalformedURLException e) {
            Log.e(TAG, e.toString());
        } catch (IOException e) {
            if(null!=connection) {
                //server returned error code
                responseStream = new BufferedInputStream(connection.getErrorStream());

            }else{
                Log.e(TAG, e.toString());
            }
        } finally {
            if(null != responseStream){
                try {
                    responseMessage = IOUtils.toString(responseStream);
                    responseStream.close();
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                }
            }

            if(null != connection)
                connection.disconnect();
            ;
        }

        if (responseCode == RESPONSE_CODE_OK) {
            // Extract response message
            final JsonObject jsonObject = jsonStringToObject(responseMessage);

            if (BuildConfig.DEBUG && (jsonObject == null || !jsonObject.has("state") ) )
                throw new AssertionError(String.format("Wrong response. Is not a successful response: %s", responseMessage));

            String state = jsonObject.get("state").getAsString();

            if (BuildConfig.DEBUG && (state == null || (!"OK".equals(state) && !"FAILED".equals(state)) ))
                throw new AssertionError(String.format("Wrong state. Is not a successful response: %s", responseMessage));

            if ("OK".equals(state)) {
                if (BuildConfig.DEBUG && (!jsonObject.has("content") ) )
                    throw new AssertionError(String.format("Wrong response. Is not a successful response: %s", responseMessage));

                responseMessage = jsonObject.get("content").toString();
            } else {  // "FAILED".equals(state) == true
                if (BuildConfig.DEBUG && (!jsonObject.has("errorcode") || !jsonObject.has("message") ) )
                    throw new AssertionError(String.format("Wrong response. Is not a successful response: %s", responseMessage));

                responseCode = jsonObject.get("errorcode").getAsInt();
                responseMessage = jsonObject.get("message").getAsString();
            }
        } else {
            Log.d(TAG, String.format("Bad response: %d %s", responseCode, responseMessage));
            responseCode = ERROR_CODE_BAD_REQUEST;
            responseMessage = ERROR_MSG_BAD_REQUEST;
        }

        return new AbstractMap.SimpleEntry<>(responseCode, responseMessage);
    }

    private String mapToChainedString(final Map<String,String> map, final String keyValueDelimiter, final String entryDelimiter)
    {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for(String parameterKey : map.keySet())
        {
            //append parameter key=value
            sb.append(parameterKey).append(keyValueDelimiter).append(map.get(parameterKey));

            //another parameter following? key1=value1&key2=value2&...
            if(i < map.keySet().size()-1)
                sb.append(entryDelimiter);

            i++;
        }
        return sb.toString();
    }


    private static final List<String> UPDATE_OPERATIONS = Arrays.asList("accept_invitation", "submit", "judge");
    /**
     * A helper function to request an update operation.
     *
     * All update functions return the new GameTable. This table needs to be saved.
     * In case a failure happened, the GameTable will be reloaded from the server.
     * If a reload failed the table has to be deleted from users device.
     */
    private AsyncTaskResult<GameTable> executeUpdateTableOperationAndSaveTable(final String urlstring, final Map<String,String> parameters) {

        if (BuildConfig.DEBUG && !parameters.containsKey("action")
                              && null != parameters.get("action")
                              && ! UPDATE_OPERATIONS.contains(parameters.get("action"))
                              && !parameters.containsKey("tableId")) {
            throw new AssertionError("Wrong parameters given: "+parameters.toString());
        }

        // General request
        AbstractMap.SimpleEntry<Integer, String> response = doPostRequest(urlstring, parameters);

        // Handle response
        int responseCode = response.getKey();
        String responseMessage = response.getValue();

        if(responseCode == RESPONSE_CODE_OK) {
            JsonObject jsonTable = jsonStringToObject(responseMessage);
            GameTable gameTable = GameTable.createFrom(jsonTable);
            dataSServices.save(gameTable);
            bahApp.reloadGameTable(gameTable.getId());
            return new AsyncTaskResult<>(RESPONSE_CODE_OK, "", gameTable);

        }
        
        setLatestErrorMessage(responseMessage);

        if (responseCode != ERROR_CODE_BAD_REQUEST) {
            // An error occurred, try to fetch up-to-date GameTable
            getAndSaveGameTable(parameters.get("tableId"));
        }

        return new AsyncTaskResult<>(responseCode, responseMessage, null);
    }

    /**
     * Identifies tables that advanced to the next round without showing the result to the user yet.
     * Returns the ids of those tables.
     */
    private List<String> determineIdsOfAdvancedGameTables(List<GameTable> _old, List<GameTable> _new){

        List<String> tableIdsWithRoundChanges = new ArrayList<>();
        for(GameTable o : _old){
            for(GameTable n : _new){
                if(o.getId().equals(n.getId()) && o.getRoundCount() != n.getRoundCount() ){
                    tableIdsWithRoundChanges.add(o.getId());
                }
            }
        }

        return tableIdsWithRoundChanges;
    }
}

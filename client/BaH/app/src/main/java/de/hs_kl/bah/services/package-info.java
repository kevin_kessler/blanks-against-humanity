/**
 * This package contains platform independent interfaces used by the application.
 * Android implementations of these interfaces should be implemented in hs_kl.de.bah.android_services.
 *
 * The interfaces are used by the model to access platform dependent services.
 */
package de.hs_kl.bah.services;

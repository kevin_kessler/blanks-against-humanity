package de.hs_kl.bah.async_tasks;

import de.hs_kl.bah.android_services.NetworkServicesImp;

/**
 * Contains the return parameter and in failed processes the error data.
 */
public class AsyncTaskResult<T> {

    /** Return code of the http request */
    private final int returnCode;

    /** Error message. Null if no error occurred */
    private final String errMsg;

    private final T resultParameter;

    public AsyncTaskResult(T resultParameter) {
        this(NetworkServicesImp.RESPONSE_CODE_OK, "", resultParameter);
    }


    public AsyncTaskResult(int returnCode, String errMsg, T resultParameter) {
        this.returnCode = returnCode;
        this.errMsg = errMsg;
        this.resultParameter = resultParameter;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public T getResultParameter() {
        return resultParameter;
    }

    public boolean wasSuccessful() {
        return returnCode == NetworkServicesImp.RESPONSE_CODE_OK;
    }
}

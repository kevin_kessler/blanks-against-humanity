package de.hs_kl.bah.android_services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import de.hs_kl.bah.controller.CreateGameActivity;
import de.hs_kl.bah.controller.CurrentGamesActivity;
import de.hs_kl.bah.controller.GameActivity;
import de.hs_kl.bah.controller.HelpActivity;
import de.hs_kl.bah.controller.InviteActivity;
import de.hs_kl.bah.controller.LoginActivity;
import de.hs_kl.bah.controller.RegistrationActivity;
import de.hs_kl.bah.controller.SettingsActivity;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.services.DataStorageServices;

/**
 * This class helps to start/stop activities.
 */
public class ActivityStarter {

    private static final String TAG = ActivityStarter.class.getSimpleName();

    public static void startRegisterActivity(Activity activity, Context context) {
        startActivity(activity, context, RegistrationActivity.class);
    }
    public static void startHelpActivity(Activity activity, Context context) {
        startActivity(activity, context, HelpActivity.class);
    }
    public static void startCurrentGamesActivity(Activity activity, Context context) {
        startActivity(activity, context, CurrentGamesActivity.class);
    }
    public static void startCreateGameActivity(Activity activity, Context context) {
        startActivity(activity, context, CreateGameActivity.class);
    }
    public static void startGameActivity(Activity activity, Context context, String gameTableId, boolean toPreviousRound) {
        Log.i(TAG, String.format("Sending intent to start GameActivity: gameTableId: %s toPreviousRound: %s.", gameTableId, Boolean.toString(toPreviousRound)));
        //put currentGamesActivity on top of stack to always redirect there when pressing back in the game activity.
        //the passed intent extra ensures to automatically redirect to game activity after currentGamesActivity has been executed
        if(!(activity instanceof CurrentGamesActivity) && !(activity instanceof GameActivity)){
            Intent intent = new Intent(context, CurrentGamesActivity.class);
            intent.putExtra(GameActivity.GAMETABLE_EXTRA, gameTableId);
            activity.startActivity(intent);
        }

        //if the request already comes from the currentGamesActivity or gameActivity we can simply start the game activity
        else{
            Intent intent = new Intent(context, GameActivity.class);
            intent.putExtra(GameActivity.GAMETABLE_EXTRA, gameTableId);
            intent.putExtra(GameActivity.PREVIOUS_ROUND_EXTRA, toPreviousRound);
            activity.startActivity(intent);
        }
    }
    public static void startSettingsActivity(Activity activity, Context context) {
        startActivity(activity, context, SettingsActivity.class);
    }
    public static void simpleStartLoginActivity(Activity activity, Context context) {
        startActivity(activity, context, LoginActivity.class);
    }
    public static void startInviteActivity(Activity activity, Context context, String tableId) {
        Log.i(TAG, String.format("Sending intent to start Class InviteActivity with tableId %s.", tableId));
        Intent intent = new Intent(context, InviteActivity.class);
        intent.putExtra(InviteActivity.TABLE_ID, tableId);
        activity.startActivity(intent);
    }

    /**
     * If we want to logout at any activity in the app we have to destroy the remaining activities
     * in the stack. To do this need to go to the CurrentGamesActivity with the flag FLAG_ACTIVITY_CLEAR_TOP
     * and there finish the CurrentGamesActivity and start the LoginActivity
     */
    public static void clearActivityStackAndStartLoginActivity(Activity activity, Context context) {
        Log.i(TAG, "Clearing activity stack and start login.");
        Intent intent = new Intent(context, CurrentGamesActivity.class);
        // This flag also removes all remaining activities.
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // This will cause the CurrentGamesActivity to finish and start the loginActivity
        intent.putExtra(CurrentGamesActivity.LOGOUT_EXTRA, true);
        activity.startActivity(intent);
    }

    private static void startActivity(Activity activity, Context context, Class<?> clazz) {
        Log.i(TAG, String.format("Sending intent to start Class %s.", clazz.getSimpleName()));
        Intent intent = new Intent(context, clazz);
        activity.startActivity(intent);
    }

    public static void startPollServiceIfMainUserExists(Context context) {
        DataStorageServices dataSServices = new DataStorageServicesImp(context);
        MainUser mu = dataSServices.loadMainUser();
        if(null != mu) {
            Log.i(TAG, String.format("Starting ServerPollService, MainUser: %s", mu.getEmail()));
            Intent serviceIntent = new Intent(context, ServerPollService.class);
            context.startService(serviceIntent);
        } else {
            Log.i(TAG, "Cannot start ServerPollService, MainUser does not exist.");
        }
    }

    public static void stopPollService(Context context) {
        Log.i(TAG, "Sending intent to stop ServerPollService.");
        context.stopService(new Intent(context, ServerPollService.class));
    }
}

package de.hs_kl.bah.model;

/**
 * Indicates the type of an update.
 */
public enum UpdateType {

    /** Nothing changed. */
    NO_UPDATE,

    /** Discovered new table. */
    NEW,

    /** Discovered new invitation. */
    NEW_INVITED,

    /** User is not at the table anymore. */
    DELETED,

    /** Table started new round. */
    NEW_ROUND,

    /** Table started new round and user is judge. */
    NEW_ROUND_AS_JUDGE,

    /** New cards have been submitted. */
    NEW_CARDS_SUBMITTED,

    /** New user joined table. */
    USER_JOINED,

    /** User left table. */
    USER_LEFT,

    OTHER /** Unspecified update. */

}

/**
 * This package contains common used functions in the controller package.
 */
package de.hs_kl.bah.controller.util;
package de.hs_kl.bah.model;

import android.text.Html;
import android.text.Spanned;

import com.google.gson.JsonObject;

import java.util.List;

import hs_kl.de.bah.BuildConfig;

/**
 * Data holder representing a black card.
 */
public class BlackCard {

    public static final String BLANK_INDICATOR = "{BLANK}";
    public static final String BLANK_REPRESENTATION = "_______";

    private int id;
    private int numOfBlanks;
    private String text;

    public BlackCard(int id, int numOfBlanks, String text){
        this.id = id;
        this.numOfBlanks = numOfBlanks;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public int getNumOfBlanks() {
        return numOfBlanks;
    }

    public String getText() {
        return text;
    }

    public static BlackCard createFrom(JsonObject jsonObject) {
        int _id = jsonObject.get("id").getAsInt();
        int _numOfBlanks = jsonObject.get("numOfBlanks").getAsInt();
        String _text = jsonObject.get("text").getAsString();
        return new BlackCard(_id, _numOfBlanks, _text);
    }

    public Spanned fillBlanks(List<IngameWhiteCard> whiteCards){
        if(BuildConfig.DEBUG && whiteCards.size() < numOfBlanks)
            throw new AssertionError("Can not fill blanks. Not enough white cards to fill.");

        for(int i=0; i<numOfBlanks; i++){
            for(IngameWhiteCard c : whiteCards){
                if(c.getSubmittedOrder() == i+1){
                    String highlightedCardText = "<font color='#00ff00'>["+c.getText()+"]</font>";
                    text = text.replaceFirst(BLANK_REPRESENTATION, highlightedCardText);
                    break;
                }
            }
        }
        return Html.fromHtml(text);
    }

    public String representBlanks(){
        text = text.replace(BlackCard.BLANK_INDICATOR, BLANK_REPRESENTATION);
        return text;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BlackCard other = (BlackCard) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "BlackCard [id=" + id + ", numOfBlanks=" + numOfBlanks
                + ", text=" + text + "]";
    }

    public boolean isValid() {
        return 0 <= id && 0 < numOfBlanks && text != null && text.contains(BLANK_INDICATOR);
    }
}

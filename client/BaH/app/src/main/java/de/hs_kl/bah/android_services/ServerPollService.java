package de.hs_kl.bah.android_services;

import android.app.Application;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hs_kl.bah.async_tasks.BahRunner;
import de.hs_kl.bah.controller.BahApplication;
import de.hs_kl.bah.controller.CurrentGamesActivity;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.model.UpdateType;
import de.hs_kl.bah.services.DataStorageServices;
import de.hs_kl.bah.services.NetworkServices;
import hs_kl.de.bah.BuildConfig;
import hs_kl.de.bah.R;

/**
 * Remote service to fetch periodically for tables.
 * In case of
 */
public class ServerPollService extends Service {

    /**
     * Necessary to determine if the user is currently at a screen and the polling has to be increased.
     */
    enum UserState { IN_MAIN_SCREEN, IN_GAME_SCREEN, ELSEWHERE};

    private static final String TAG = ServerPollService.class.getSimpleName();
    private static final long LONG_UPDATE_INTERVAL_IN_MILLIS = 120*60*1000;  // Poll every 2 hours
    private static final long SHORT_UPDATE_INTERVAL_IN_MILLIS = 4000;
    private static final long SHORT_UPDATE_INTERVAL_IN_MILLIS_FOR_POLLING_SINGLE_TABLE = 2000;
    public static final String ACTION_UPDATE_GAMETABLES_NOTIFICATION = "de.hs_kl.bah.intent.action.ACTION_UPDATE_GAMETABLES_NOTIFICATION";
    //TODO: logout if user cannot be loaded from storage or loggs in at another device
    public static final String ACTION_LOGOUT = "de.hs_kl.bah.intent.action.ACTION_LOGOUT";
    public static final String GAME_TABLE_IDS = "game_table_ids_extra";
    public static final String A_NEW_ROUND_STARTED = "a_new_round_started_extra";

    private BahApplication bahApp;
    private String emailOfMainUser;
    private DataStorageServices dataSServices;
    private NetworkServices networkServices;
    private Timer timerForAllTables;
    private Timer timerForSingleTable;
    private String tableIdToPoll;
    private List<GameTable> availableTables;

    private UserUsesAppNotificationReceiver userUsesAppNotificationReceiver;
    private UserState userState;

    public ServerPollService() {
        availableTables = new ArrayList<>();
        userState = UserState.ELSEWHERE;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        dataSServices = new DataStorageServicesImp(getApplicationContext());

        Application app = getApplication();

        if (BuildConfig.DEBUG && ! (app instanceof BahApplication) )
            throw new AssertionError(String.format("app %s is not an instance of BahApplication", app));

        bahApp = (BahApplication) app;
        MainUser mainUser = bahApp.getMainUser();

        if (null == mainUser) {
            Log.i(TAG, "ServerPollService will not be started, MainUser is null.");
            stopSelf();
            return;
        }

        reloadUserCredentials();

        networkServices = new NetworkServicesImp( (BahApplication) app );

        // Register broadcast
        userUsesAppNotificationReceiver = new UserUsesAppNotificationReceiver(new BahRunner<Intent>() {
            @Override
            public void run(Intent intent) {
                setUserState(intent);
            }
        });
        IntentFilter filter = new IntentFilter(UserUsesAppNotificationReceiver.USES_NOTIFICATION_ACTION);
        this.registerReceiver(userUsesAppNotificationReceiver, filter);
        restartTimerForAllTables();
    }

    private void setUserState(Intent intent) {

        int usesAppNotification = intent.getIntExtra(UserUsesAppNotificationReceiver.EXTRA_USES_NOTIFICATION, -1);
        Log.d(TAG, String.format("ServerPollService received UserUsesAppNotification: %d",usesAppNotification));

        switch (usesAppNotification) {
            case UserUsesAppNotificationReceiver.ENTERS_MAIN_SCREEN:
                userState =UserState.IN_MAIN_SCREEN;
                restartTimerForAllTables();
                break;
            case UserUsesAppNotificationReceiver.ENTERS_GAME_SCREEN:
                userState =UserState.IN_GAME_SCREEN;
                tableIdToPoll = intent.getStringExtra(UserUsesAppNotificationReceiver.EXTRA_TABLE_ID);
                adaptPollingForSingleTable();
                break;
            case UserUsesAppNotificationReceiver.LEAVES_MAIN_SCREEN:
            case UserUsesAppNotificationReceiver.LEAVES_GAME_SCREEN:
                userState =UserState.ELSEWHERE;
                restartTimerForAllTables();
                adaptPollingForSingleTable();
                break;
            default:
                Log.w(TAG,String.format("Invalid UserUsesAppNotification number: %d", usesAppNotification));
        }
    }

    private void adaptPollingForSingleTable() {
        if (UserState.ELSEWHERE == userState) {
            if (null != timerForSingleTable)
                timerForSingleTable.cancel();
            Log.d(TAG, String.format("Stopped polling for single table %s", tableIdToPoll));
        } else if (UserState.IN_GAME_SCREEN == userState) {
            timerForSingleTable = new Timer();
            TimerTask timerTaskForSingleTable = new TimerTask() {
                @Override
                public void run() {
                    Log.i(TAG, String.format("Polling Server for single table: %s", tableIdToPoll));
                    try{
                        pollServerForASingleTable(tableIdToPoll, userState);
                    } catch (Exception e) {
                        Log.e(TAG, "Received exception during pollServerForASingleTable()", e);
                    }
                }
            };
            timerForSingleTable.scheduleAtFixedRate(timerTaskForSingleTable, 0, SHORT_UPDATE_INTERVAL_IN_MILLIS_FOR_POLLING_SINGLE_TABLE);
        }
    }

    private void restartTimerForAllTables() {
        if (null != timerForAllTables)
            timerForAllTables.cancel();
        timerForAllTables = new Timer();
        long updateInterval = UserState.IN_MAIN_SCREEN == userState ? SHORT_UPDATE_INTERVAL_IN_MILLIS : LONG_UPDATE_INTERVAL_IN_MILLIS;
        TimerTask timerTaskForAllTables = new TimerTask() {
            @Override
            public void run() {
                Log.i(TAG, "Polling Server.");
                try{
                    pollServerForAllTables(userState);
                } catch (Exception e) {
                    Log.e(TAG, "Received exception during pollServerForAllTables()", e);
                }
            }
        };
        timerForAllTables.scheduleAtFixedRate(timerTaskForAllTables, 0, updateInterval);
        Log.i(TAG, String.format("Polling (re)started with interval %d.", updateInterval));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        reloadUserCredentials();

        return super.onStartCommand(intent, flags, startId);
    }

    private void reloadUserCredentials() {
        bahApp.reloadMainUser();
        MainUser mainUser = bahApp.getMainUser();

        if (null == mainUser) {
            Log.i(TAG, "ServerPollService will not be started, MainUser is null.");
            stopSelf();
            return;
        }

        emailOfMainUser = mainUser.getEmail();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timerForAllTables != null) {
            timerForAllTables.cancel();
        }
        if (timerForSingleTable != null) {
            timerForSingleTable.cancel();
        }
        this.unregisterReceiver(userUsesAppNotificationReceiver);
        Log.i(TAG, "Polling stopped.");
    }


    /** localUserState in case the global status will be changed during the process. */
    private void pollServerForASingleTable(String tableId, UserState localUserState) {
        // Read table in front of possible updates
        GameTable gameTable = dataSServices.readGameTable(tableId);

        GameTable newGameTable = networkServices.getAndSaveGameTable(tableId);

        if (null != newGameTable && null!=gameTable) {
            Log.i(TAG, "Successful Fetching of GameTable.");

            Map<GameTable,UpdateType> updatedTables = new HashMap<>();

            UpdateType updateType = gameTable.compareAndGetUpdateType(newGameTable, emailOfMainUser);
            if (UpdateType.NO_UPDATE != updateType)
                updatedTables.put(newGameTable, updateType);

            if (0 != updatedTables.size()) {

                fetchForMainUser();

                // Notify user and application
                notifyAboutUpdatedTables(updatedTables, localUserState);
            } else {
                Log.i(TAG, "GameTable was not updated.");
            }
        } else {
            Log.i(TAG, String.format("Fetching for GameTable %s was not successful.", tableId));
        }
    }
    /** localUserState in case the status will be changed during the process. */
    private void pollServerForAllTables(UserState localUserState) {
        // Read tables in front of possible updates
        availableTables = dataSServices.readAllGameTables();

        if (fetchGameTablesFromServer()) {
            Log.i(TAG, "Successful Fetching of GameTables.");
            List<GameTable> newGameTables = dataSServices.readAllGameTables();

            Map<GameTable,UpdateType> updatedTables = checkAndGetUpdatedTables(availableTables, newGameTables);

            if (0 != updatedTables.size()) {
                availableTables = newGameTables;

                fetchForMainUser();

                // Notify user and application
                notifyAboutUpdatedTables(updatedTables, localUserState);
            } else {
                Log.i(TAG, "No GameTable updates found.");
            }
        } else {
            Log.i(TAG, "Fetching of GameTables was not successful.");
        }
    }



    private void fetchForMainUser() {
        // Reload also user data. Credits could be changed
        MainUser mainUser = bahApp.getMainUser();
        // TODO: a function like fetchUserData would be better. This is actually a "hack"
        mainUser = networkServices.loginUser(mainUser.getEmail(), mainUser.getPassword());
        dataSServices.save(mainUser);
    }

    private boolean fetchGameTablesFromServer() {
        if (networkServices.getAndSaveAllTables()) {
            return true;
        } else {
            Log.w(TAG, String.format("Failed to getAndSaveAllTables(), errMsg: %s", networkServices.getLatestErrorMessage()));
            return false;
        }
    }

    private Map<GameTable,UpdateType> checkAndGetUpdatedTables(List<GameTable> oldGameTables, List<GameTable> newGameTables) {

        if (BuildConfig.DEBUG && (null == oldGameTables || null == newGameTables) )
            throw new AssertionError(String.format("oldGameTables or newGameTables are null.\nold: %s\nnew: %s", oldGameTables, newGameTables));

        Map<GameTable,UpdateType> updatedGames = new HashMap<>();

        for(GameTable oldTable : oldGameTables) {

            GameTable newTable = getByTableId(newGameTables, oldTable.getId());

            if (null == newTable) {
                updatedGames.put(oldTable, UpdateType.DELETED);
                continue;
            }

            UpdateType updateType = oldTable.compareAndGetUpdateType(newTable, emailOfMainUser);

            if (UpdateType.NO_UPDATE != updateType) {
                updatedGames.put(newTable, updateType);
                continue;
            }

        }

        if (oldGameTables.size() < newGameTables.size())
            // New GameTables do also exist
            for (GameTable newTable : newGameTables) {
                if (null == getByTableId(oldGameTables, newTable.getId())) {
                    if (newTable.isInvited(emailOfMainUser))
                        updatedGames.put(newTable, UpdateType.NEW_INVITED);
                    else
                        updatedGames.put(newTable, UpdateType.NEW);
                    continue;
                }
            }

        return updatedGames;
    }
    private GameTable getByTableId(List<GameTable> gameTables, String tableId) {
        for(GameTable t : gameTables)
            if (t.getId().equals(tableId))
                return t;
        return null;
    }

    public void notifyAboutUpdatedTables(Map<GameTable,UpdateType> updatedTables, UserState localUserState) {

        //user to track tables with new rounds to be able to show result of previous round to user before entering new round
        List<String> newRoundTableIds = new ArrayList<>();

        boolean atLeastOneNewRoundStarted = false;

        //prepare notification summary which will be sent to device as notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        List<String> notificationTexts = new ArrayList<>();
        for(Map.Entry<GameTable, UpdateType> updatedTable : updatedTables.entrySet()){
            UpdateType updateType = updatedTable.getValue();
            GameTable gameTable = updatedTable.getKey();

            //track tables with new round to show result of previous round to user before entering new round
            if(updateType == UpdateType.NEW_ROUND || updateType == UpdateType.NEW_ROUND_AS_JUDGE) {
                newRoundTableIds.add(gameTable.getId());
                atLeastOneNewRoundStarted = true;
            }

            //if a table has been removed on the server, ensure that we dont have the id in the list of advanced game table ids anymore
            if(updateType == UpdateType.DELETED)
                dataSServices.removeIdFromAdvancedGameTableIds(gameTable.getId());

            //only send a notification to the user if there was an invite or if a round finished
            /** This avoids sending new round finished notifications if user is currently in GaeActivity with currentGame. */
            boolean isNewRound = updateType == UpdateType.NEW_ROUND  || updateType == UpdateType.NEW_ROUND_AS_JUDGE;
            boolean isNotInGameViewWithCurrentTable = !( UserState.IN_GAME_SCREEN == localUserState && tableIdToPoll.equals(gameTable.getId()));

            if(updateType == UpdateType.NEW_INVITED || (isNewRound && isNotInGameViewWithCurrentTable))
                notificationTexts.add(getNotificationContentByUpdateType(gameTable, updateType).getValue());

            boolean isInGameViewWithCurrentTable = ! isNotInGameViewWithCurrentTable;
            if ( (updateType == UpdateType.USER_JOINED ||  updateType == UpdateType.USER_LEFT) && isInGameViewWithCurrentTable)
                notificationTexts.add(getNotificationContentByUpdateType(gameTable, updateType).getValue());
        }

        //save the list of new round table ids
        if(!newRoundTableIds.isEmpty()) {
            dataSServices.saveIdsOfAdvancedGameTables(newRoundTableIds);
        }

        // Send broadcast after newRoundTableIds has been saved, since the receivers rely on that information
        String[] tableIds = new String[updatedTables.size()];
        int i = 0;
        for (GameTable e : updatedTables.keySet()) {
            tableIds[i] = e.getId();
            ++i;
        }
        Intent intent = new Intent(ACTION_UPDATE_GAMETABLES_NOTIFICATION);
        intent.putExtra(GAME_TABLE_IDS, tableIds);
        if (atLeastOneNewRoundStarted)
            intent.putExtra(A_NEW_ROUND_STARTED, true);
        sendBroadcast(intent);

        if(notificationTexts.isEmpty())
            return; //nothing to notify the user about

        //TODO: clear summary when users took actions that make the notification obsolete. e.g. user saw the result of a round -> remove notification "round finished"
        //create a notification summary and always send with the same id, because we want to update the notification summary instead of creating multiple ones
        Notification summary = createNotificationSummary(notificationTexts);
        notificationManager.notify(0, summary);
        Log.d(TAG, "successfully sent notification summary");
    }

    private Notification createNotificationSummary(List<String> notificationTexts){
        Log.d(TAG, String.format("preparing notification summary: numOfMsgs=%d", notificationTexts.size()));
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification)
                .setLargeIcon(icon)
                .setColor(Color.WHITE)
                .setContentTitle(notificationTexts.size() + " events")
                .setContentText("Blanks Against Humanity Events")
                .setNumber(notificationTexts.size())
                .setContentIntent(prepareNotificationIntent())
                .setAutoCancel(true)
                .setSound(soundUri);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setSummaryText("Blanks Against Humanity");
        inboxStyle.setBigContentTitle("Event details");
        for (int i=0; i < notificationTexts.size(); i++) {
            inboxStyle.addLine(notificationTexts.get(i));
        }
        builder.setStyle(inboxStyle);
        return builder.build();
    }

    private AbstractMap.SimpleEntry<String,String> getNotificationContentByUpdateType(GameTable gameTable, UpdateType updateType){
        String title;
        String text;

        //TODO: replace "somebody" by username
        switch (updateType){
            case NEW_INVITED:
                title = String.format(getResources().getString(R.string.notification_new_invitation_title), gameTable.getName());
                text = String.format(getResources().getString(R.string.notification_new_invitation_text), gameTable.getName(), "Somebody");
                break;
            case NEW_ROUND:
            case NEW_ROUND_AS_JUDGE:
                title = String.format(getResources().getString(R.string.notification_new_round_title), gameTable.getName());
                text = String.format(getResources().getString(R.string.notification_new_round_text), gameTable.getName());
                break;
            case USER_JOINED:
                title = String.format(getResources().getString(R.string.notification_user_joined_title), gameTable.getName());
                text = String.format(getResources().getString(R.string.notification_user_joined_text), gameTable.getName(), "Somebody");
                break;
            case USER_LEFT:
                title = String.format(getResources().getString(R.string.notification_user_left_title), gameTable.getName());
                text = String.format(getResources().getString(R.string.notification_user_left_text), gameTable.getName(), "Somebody");
                break;
            //TODO: keep updated during development
            default:
                title = getResources().getString(R.string.notification_default_title);
                text = getResources().getString(R.string.notification_default_text);
                break;
        }

        return new AbstractMap.SimpleEntry<>(title, text);
    }

    private PendingIntent prepareNotificationIntent(){
        // setup stackbuilder to define to go to parent activity when pressing back after tapping notification
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        Intent resultIntent = new Intent(getApplicationContext(), CurrentGamesActivity.class);
        stackBuilder.addParentStack(CurrentGamesActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        // retrieve pending intent from prepared stack
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

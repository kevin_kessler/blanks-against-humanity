package de.hs_kl.bah.async_tasks;

import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;

/**
 * Represents an asynchronous loading task used to read
 * the game tables.
 */
public class LoadCurrentGamesTask extends BahAsyncTask<Void>{

    private BahActivity parentActivity;
    private BahRunner<Void> onPostRunner;

    public LoadCurrentGamesTask(BahActivity currentGamesActivity, ProgressUiHelper progressUiHelper, BahRunner<Void> onPostRunner){
        super(currentGamesActivity, progressUiHelper);
        parentActivity = currentGamesActivity;
        this.onPostRunner = onPostRunner;
    }

    @Override
    protected Void doInBackground(Void... params) {
        parentActivity.getBahApplication().reloadGameTables();
        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        super.onPostExecute(v);
        onPostRunner.run(v);
    }

}

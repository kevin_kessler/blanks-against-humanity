package de.hs_kl.bah.async_tasks;

import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous loading task used to accept an invitation.
 */
public class AcceptInvitationTask extends BahAsyncTask<AsyncTaskResult<GameTable>>{

    private BahRunner<AsyncTaskResult<GameTable>> onPostRunner;
    private final NetworkServices networkServices;
    private final String tableId;

    public AcceptInvitationTask(BahActivity activity, ProgressUiHelper progressUiHelper, String tableId, BahRunner<AsyncTaskResult<GameTable>> onPostRunner){
        super(activity, progressUiHelper);
        this.onPostRunner = onPostRunner;
        this.networkServices = new NetworkServicesImp(activity.getBahApplication());
        this.tableId = tableId;
    }

    @Override
    protected AsyncTaskResult<GameTable> doInBackground(Void... params) {
        return networkServices.acceptInvitation(tableId);
    }

    @Override
    protected void onPostExecute(AsyncTaskResult<GameTable> taskResult) {
        super.onPostExecute(taskResult);
        onPostRunner.run(taskResult);
    }

}

package de.hs_kl.bah.controller;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.model.Round;

/**
 * Superclass for the two page fragments used within GameActivity
 * LeftPageFragment and RightPageFragment
 */
public abstract class GamePageFragment extends Fragment {

    private GameActivity activity;

    public abstract String getTitle();

    public GameActivity getGameActivity(){
        return activity;
    }
    public BahApplication getBahApp(){ return activity.getBahApplication(); }
    public MainUser getMainUser(){ return getBahApp().getMainUser(); }
    public Round getActiveRound() { return activity.getActiveRound(); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (GameActivity)getActivity();
        return null;
    }
}

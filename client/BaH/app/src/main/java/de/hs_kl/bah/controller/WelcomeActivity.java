package de.hs_kl.bah.controller;

import android.app.Activity;
import android.os.Bundle;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.model.MainUser;
import hs_kl.de.bah.R;

/**
 * This is the launcher activity.
 * It checks whether a logged in user exists.
 */
public class WelcomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if (loggedInUserExists()) {
            ActivityStarter.startCurrentGamesActivity(this, getApplicationContext());
        } else {
            ActivityStarter.simpleStartLoginActivity(this, getApplicationContext());
        }
        finish();
    }

    private boolean loggedInUserExists() {
        BahApplication bahApp = (BahApplication) getApplication();

        MainUser mainUser = bahApp.getMainUser();
        return null != mainUser && mainUser.isValid();
    }

}

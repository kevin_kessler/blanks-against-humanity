package de.hs_kl.bah.controller.util;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Helper class for reading and writing files.
 */
public class FileIO {

    private final static String TAG = FileIO.class.getSimpleName();

    public static void writeFile(File directory, String filename, String content) {
        File fileWithinDir = new File(directory, filename); //Getting a file within the dir.
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(fileWithinDir);
            out.write(content.getBytes());
        } catch (IOException e) {
            Log.d(TAG, String.format("Could not write file %s", filename), e);
        } finally {
            try {
                if (null != out)
                    out.close();
            } catch (IOException e) {
                // Nothing to do
            }
        }
    }

    public static String readFile(File directory, String filename) {
        File fileWithinDir = new File(directory, filename);
        return readFile(fileWithinDir);
    }
    public static String readFile(File file) {
        Scanner in = null;
        StringBuilder sb = new StringBuilder();
        try {
            FileInputStream fin = new FileInputStream(file);
            in = new Scanner(fin);
            while (in.hasNext()) {
                sb.append(in.nextLine()).append("\n");
            }
        } catch (IOException e) {
            Log.d(TAG, String.format("Could not read file %s", file.getName()));
        } finally {
            if (null != in)
                in.close();
        }

        return sb.toString();
    }
}

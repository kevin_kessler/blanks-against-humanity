package de.hs_kl.bah.controller.util;

/**
 * Base class to handle AsyncTaskResults
 */
public abstract class AsyncTaskResultHandler {

    public void handleResult() {
        if (wasSuccessful()) {
            handleSuccess();
        } else {
            handleFailure();
        }
    }

    public abstract boolean wasSuccessful();
    public abstract void handleSuccess();
    public abstract void handleFailure();
}

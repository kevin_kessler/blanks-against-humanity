package de.hs_kl.bah.android_services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hs_kl.bah.controller.util.FileIO;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.services.DataStorageServices;
import hs_kl.de.bah.BuildConfig;

/**
 * Implements the android data storage. Responsible for loading and saving files from and to the
 * storage of the android device.
 */
public class DataStorageServicesImp implements DataStorageServices {

    private static final String TAG = DataStorageServicesImp.class.getSimpleName();
    private static final String GAMETABLE_DIR_PATH = "gametables";
    private static final String GAMETABLE_NAME_TEMPLATE = "gametable_%s.json";
    private static final String USER_FILE_KEY = "de.hs_kl.bah.USER_FILE_KEY";
    private static final String ADVANCED_GAMETABLE_IDS_DIR_PATH = "advanced_gametables";
    private static final String ADVANCED_GAMETABLE_IDS_FILENAME = "advanced_gametables.json";

    private Context context;

    public DataStorageServicesImp(Context context) {
        this.context = context;
    }

    private final String LAST_LOGGED_IN_EMAIL_KEY = "last_user_email";
    private final String USER_EMAIL_KEY = "user_email";
    private final String USER_NAME_KEY = "user_name";
    private final String USER_PASSWORD_KEY = "user_password";
    private final String USER_CREDIT_KEY  = "user_credit";
    private final String USER_SESSION_ID_KEY  = "user_session_id";

    @Override
    public MainUser loadMainUser() {
        SharedPreferences sharedPref = context.getSharedPreferences(
                USER_FILE_KEY, Context.MODE_MULTI_PROCESS);
        String email = sharedPref.getString(USER_EMAIL_KEY, null);
        String username = sharedPref.getString(USER_NAME_KEY, null);
        String password = sharedPref.getString(USER_PASSWORD_KEY, null);
        int credit = sharedPref.getInt(USER_CREDIT_KEY, 0);
        String sesion_id = sharedPref.getString(USER_SESSION_ID_KEY, null);

        if (null == email || null == username || null == password)
            return null;
        else
            return new MainUser(email, username, password, credit, sesion_id);

    }

    @Override
    public void save(MainUser mainUser) {

        if (BuildConfig.DEBUG &&
                null == mainUser || !mainUser.isValid())
            throw new AssertionError("User is not valid.");

        SharedPreferences sharedPref = context.getSharedPreferences(
                USER_FILE_KEY, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(USER_EMAIL_KEY, mainUser.getEmail());
        editor.putString(USER_NAME_KEY, mainUser.getUsername());
        editor.putString(USER_PASSWORD_KEY, mainUser.getPassword());
        editor.putInt(USER_CREDIT_KEY, mainUser.getCredit());
        editor.putString(USER_SESSION_ID_KEY, mainUser.getCurrentSessionId());

        editor.commit();
    }

    @Override
    public String loadLastLoggedInEmail() {
        SharedPreferences sharedPref = context.getSharedPreferences(
                USER_FILE_KEY, Context.MODE_MULTI_PROCESS);
        String lastLoggedInEmail = sharedPref.getString(LAST_LOGGED_IN_EMAIL_KEY, null);

        return lastLoggedInEmail;
    }

    @Override
    public void saveLastLoggedInEmail(String email) {

        if (BuildConfig.DEBUG &&
                (null == email || email.isEmpty()))
            throw new AssertionError("Email is not valid.");

        SharedPreferences sharedPref = context.getSharedPreferences(
                USER_FILE_KEY, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(LAST_LOGGED_IN_EMAIL_KEY, email.toLowerCase());

        editor.commit();
    }

    @Override
    public List<String> loadIdsOfAdvancedGameTables() {
        //load ids as json from file
        File directory = context.getDir(ADVANCED_GAMETABLE_IDS_DIR_PATH, Context.MODE_MULTI_PROCESS);
        String jsonString = FileIO.readFile(directory, ADVANCED_GAMETABLE_IDS_FILENAME);
        if (null == jsonString || jsonString.isEmpty()) {
            Log.w(TAG, "Could not read the list of advanced game table ids from file");
            return null;
        }

        //restore list objects by gson
        Gson gson = new Gson();
        ArrayList<String> idsOfAdvancedGameTables = gson.fromJson(jsonString, new TypeToken<ArrayList<String>>() {
        }.getType());
        if(null!=idsOfAdvancedGameTables)
            Log.d(TAG, String.format("Successfully read the list of advanced game table ids from file. Containing %d ids ", idsOfAdvancedGameTables.size()));
        else
            Log.e(TAG, String.format("Could not restore the list of advanced game table. json corrup? : %s", jsonString));
        return idsOfAdvancedGameTables;
    }

    @Override
    public void saveIdsOfAdvancedGameTables(List<String> idsOfAdvancedGameTables){
        if (BuildConfig.DEBUG && null == idsOfAdvancedGameTables)
            throw new AssertionError("could not save list of advanced gametable ids: idsOfAdvancedTables is null.");

        //we do not want to lose id's of tables we didnt check the result of yet
        //merge existing list with retrieved one (without duplicates)
        List<String> oldAdvancedIds = loadIdsOfAdvancedGameTables();
        if(null!=idsOfAdvancedGameTables && null!=oldAdvancedIds)
            for(String oldId : oldAdvancedIds)
                if(!idsOfAdvancedGameTables.contains(oldId))
                    idsOfAdvancedGameTables.add(oldId);

        //convert to json and write to file (shared prefs did not work because of multi process access)
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(idsOfAdvancedGameTables);
        File directory = context.getDir(ADVANCED_GAMETABLE_IDS_DIR_PATH, Context.MODE_MULTI_PROCESS);
        FileIO.writeFile(directory, ADVANCED_GAMETABLE_IDS_FILENAME, jsonOutput);
        Log.d(TAG, "Successfully wrote the list of advanced game table ids to file: "+jsonOutput);
    }

    @Override
    public boolean removeIdFromAdvancedGameTableIds(String id){
        List<String> advancedIds = loadIdsOfAdvancedGameTables();
        if(advancedIds==null || !advancedIds.contains(id)) {
            Log.w(TAG, String.format("Could not remove id=%s from the list of advanced game table ids because the list does not contain it", id));
            return false;
        }

        advancedIds.remove(id);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(advancedIds);
        File directory = context.getDir(ADVANCED_GAMETABLE_IDS_DIR_PATH, Context.MODE_MULTI_PROCESS);
        FileIO.writeFile(directory, ADVANCED_GAMETABLE_IDS_FILENAME, jsonOutput);
        Log.d(TAG, String.format("Successfully removed id=%s from the list of advanced game table ids", id));
        return true;
    }

    @Override
    public void deleteAllData() {
        // User data
        SharedPreferences sharedPref = context.getSharedPreferences(
                USER_FILE_KEY, Context.MODE_MULTI_PROCESS);
        String tmpLastEmail = sharedPref.getString(LAST_LOGGED_IN_EMAIL_KEY, null);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.putString(LAST_LOGGED_IN_EMAIL_KEY, tmpLastEmail);
        editor.commit();

        deleteAllGameTables();
        deleteIdsOfAdvancedGameTables();

        //TODO: update in process of development
    }

    @Override
    public void save(List<GameTable> newGameTables){
        for(GameTable gameTable : newGameTables)
            save(gameTable);
    }

    @Override
    public void save(GameTable newGameTable) {

        if (BuildConfig.DEBUG &&
                null == newGameTable || !newGameTable.isValid())
            throw new AssertionError(String.format("Tried to save not valid table: %s", null == newGameTable ? null : newGameTable.toString()));

        Gson gson = new Gson();
        String jsonOutput = gson.toJson(newGameTable);
        writeTableFile(newGameTable.getId(), jsonOutput);
    }
    private void writeTableFile(String tableId, String jsonOutput) {
        File directory = context.getDir(GAMETABLE_DIR_PATH, Context.MODE_MULTI_PROCESS);
        String fileName = String.format(GAMETABLE_NAME_TEMPLATE, tableId);
        FileIO.writeFile(directory, fileName, jsonOutput);
    }

    @Override
    public GameTable readGameTable(String tableId) {
        String jsonString = readGameTableFile(tableId);
        if (null == jsonString || jsonString.isEmpty())
            return null;
        JsonObject jsonObject = jsonStringToObject(jsonString);
        return GameTable.createFrom(jsonObject);
    }

    private String readGameTableFile(String tableId) {
        String fileName = String.format(GAMETABLE_NAME_TEMPLATE, tableId);
        File directory = context.getDir(GAMETABLE_DIR_PATH, Context.MODE_MULTI_PROCESS);
        return FileIO.readFile(directory, fileName);
    }

    @Override
    public void deleteAllGameTables() {
        File tablesDirectory = context.getDir(GAMETABLE_DIR_PATH, Context.MODE_MULTI_PROCESS);
        for (File f : tablesDirectory.listFiles()) {
            if (f.isFile()) {
                boolean deleted = f.delete();
                if(!deleted)
                    Log.e(TAG, String.format("Could not delete file: %s", f.getAbsolutePath()));
            }
        }
    }

    @Override
    public void deleteIdsOfAdvancedGameTables() {
        File dir = context.getDir(ADVANCED_GAMETABLE_IDS_DIR_PATH, Context.MODE_MULTI_PROCESS);
        for (File f : dir.listFiles()) {
            if (f.isFile()) {
                boolean deleted = f.delete();
                if(!deleted)
                    Log.e(TAG, String.format("Could not delete file: %s", f.getAbsolutePath()));
            }
        }
    }

     public boolean deleteGameTable(String tableId) {
        String tableFileName = String.format(GAMETABLE_NAME_TEMPLATE, tableId);

        File tablesDirectory = context.getDir(GAMETABLE_DIR_PATH, Context.MODE_MULTI_PROCESS);
        for (File f : tablesDirectory.listFiles()) {
            if (f.isFile() && f.getName().equals(tableFileName)) {
                boolean deleted = f.delete();
                if(!deleted)
                    Log.e(TAG, String.format("Could not delete file: %s", f.getAbsolutePath()));
                return deleted;
            }
        }
        return false;
    }

    @Override
    public List<GameTable> readAllGameTables() {
        List<GameTable> allGameTables = new ArrayList<>();
        File tablesDirectory = context.getDir(GAMETABLE_DIR_PATH, Context.MODE_MULTI_PROCESS);
        for (File f : tablesDirectory.listFiles()) {
            if (f.isFile()) {
                String jsonTable = FileIO.readFile(f);
                JsonObject jsonObject = jsonStringToObject(jsonTable);
                GameTable gameTable = GameTable.createFrom(jsonObject);
                allGameTables.add(gameTable);
            }
        }
        return allGameTables;
    }

    private JsonObject jsonStringToObject(String jsonString)
    {
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(jsonString, JsonElement.class);
        return element.getAsJsonObject();
    }
}

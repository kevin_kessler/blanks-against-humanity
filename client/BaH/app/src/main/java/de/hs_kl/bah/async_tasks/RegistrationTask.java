package de.hs_kl.bah.async_tasks;

import android.app.Activity;
import android.widget.Toast;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.DataStorageServicesImp;
import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.services.DataStorageServices;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous registration task used to register and authenticate
 * the user.
 */
public class RegistrationTask extends BahAsyncTask<MainUser> {

    private final String username;
    private final String email;
    private final String password;
    private final String passwordRepeat;
    private final NetworkServices netWorkService;

    public RegistrationTask(Activity parentActivity, ProgressUiHelper progressUiHelper, String username, String email, String password, String passwordRepeat) {
        super(parentActivity, progressUiHelper);
        this.parentActivity = parentActivity;
        this.username = username;
        this.email = email;
        this.password = password;
        this.passwordRepeat = passwordRepeat;
        this.netWorkService = new NetworkServicesImp(null);
    }

    @Override
    protected MainUser doInBackground(Void... params) {
        return netWorkService.registerUser(username, email, password, passwordRepeat);
    }

    @Override
    protected void onPostExecute(final MainUser mainUser) {
        super.onPostExecute(mainUser);

        if (null == mainUser || !mainUser.isValid()) {
            Toast.makeText(parentActivity, netWorkService.getLatestErrorMessage(), Toast.LENGTH_SHORT).show();
        } else {
            DataStorageServices dss = new DataStorageServicesImp(parentActivity);
            dss.save(mainUser);
            dss.saveLastLoggedInEmail(mainUser.getEmail());
            ActivityStarter.startCurrentGamesActivity(parentActivity, parentActivity.getApplicationContext());
            parentActivity.finish();
        }
    }

}

package de.hs_kl.bah.controller;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import de.hs_kl.bah.async_tasks.BahRunner;
import de.hs_kl.bah.async_tasks.InviteTask;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.EmailInvitation;
import de.hs_kl.bah.model.Invitation;
import hs_kl.de.bah.BuildConfig;
import hs_kl.de.bah.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class InviteActivityFragment extends Fragment {

    private static final String TAG = InviteActivity.class.getSimpleName();

    private ProgressUiHelper progressUiHelper;
    private EditText txtEmailOrUsername;

    private InviteTask inviteTask;

    private String tableId;

    public InviteActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_invite, container, false);

        View progressView = fragmentView.findViewById(R.id.INVITATION_PROGRESS);
        View invitationFormView = fragmentView.findViewById(R.id.INVITATION_FORM);
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        progressUiHelper = new ProgressUiHelper(invitationFormView, progressView, shortAnimTime);

        txtEmailOrUsername = (EditText) fragmentView.findViewById(R.id.TXT_EMAIL_TO_INVITE);

        Button btnInvite = (Button) fragmentView.findViewById(R.id.BTN_INVITE);
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptInvitation();
            }
        });

        inviteTask = null;
        tableId = null;

        return fragmentView;
    }

    public void setTableId(String tableId) {

        if (BuildConfig.DEBUG && null == tableId || tableId.isEmpty())
            throw new AssertionError(String.format("tableId  %s is not valid.", tableId ));

        this.tableId = tableId;
    }

    private void attemptInvitation() {
        if (null == inviteTask) {

            if (TextUtils.isEmpty(tableId)) {
                Log.e(TAG, "Invalid tableId given. Finishing activity.");
                getActivity().finish();
            }

            txtEmailOrUsername.setError(null);

            // Check for a valid email address.
            String emailOrUsername = txtEmailOrUsername.getText().toString().toLowerCase();
            boolean isEmail = emailOrUsername.contains("@");
            if (TextUtils.isEmpty(emailOrUsername)) {
                txtEmailOrUsername.setError(getString(R.string.error_field_required));
                txtEmailOrUsername.requestFocus();
            } else if (isEmail && !RegistrationActivity.isValidEmail(emailOrUsername)) {
                txtEmailOrUsername.setError(getString(R.string.error_invalid_email));
                txtEmailOrUsername.requestFocus();
            } else if (isEmail && !RegistrationActivity.isValidEmail(emailOrUsername)) {
                txtEmailOrUsername.setError(getString(R.string.error_invalid_username));
                txtEmailOrUsername.requestFocus();
            } else {
                // Valid email or username and tableId. Send invitation
                Invitation invitation = new EmailInvitation(emailOrUsername, tableId);

                inviteTask = new InviteTask((BahActivity)getActivity(), progressUiHelper, invitation, new BahRunner<Boolean>() {
                    @Override
                    public void run(Boolean successful) {
                        if (successful) {
                            Toast.makeText(getActivity(), R.string.successful_invite, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {
                            Toast.makeText(getActivity(), R.string.no_successful_invite, Toast.LENGTH_SHORT).show();
                        }
                        inviteTask = null;
                    }
                });
                inviteTask.execute();
            }
        }
    }
}

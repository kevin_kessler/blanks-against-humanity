package de.hs_kl.bah.controller;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.viewpagerindicator.UnderlinePageIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.ServerPollService;
import de.hs_kl.bah.android_services.UserUsesAppNotificationReceiver;
import de.hs_kl.bah.async_tasks.BahRunner;
import de.hs_kl.bah.async_tasks.LeaveTableTask;
import de.hs_kl.bah.async_tasks.SubmitSelectedCardsToTableTask;
import de.hs_kl.bah.async_tasks.SubmitWinnerCardsToTableTask;
import de.hs_kl.bah.controller.util.GameTableUpdateReceiver;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.IngameWhiteCard;
import de.hs_kl.bah.model.Round;
import de.hs_kl.bah.model.User;
import hs_kl.de.bah.BuildConfig;
import hs_kl.de.bah.R;

/**
 * This is where the actual gaming part takes place.
 * Here the current or previous round of a certain gametable is displayed.
 * Players can interact with this activity to read the gamerounds black card,
 * to view their hand of cards, to submit white cards,
 * to view submissions by other players and to judge the winning cards.
 */
public class GameActivity extends BahActivity {

    private final String TAG = GameActivity.class.getSimpleName();

    public static final String GAMETABLE_EXTRA = "gametableid";
    public static final String PREVIOUS_ROUND_EXTRA = "loadpreviousround";

    private BroadcastReceiver updateReceiver;

    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    LeftPageFragment leftPage;
    RightPageFragment rightPage;

    /**The gametable currently displayed in this activity*/
    private GameTable gameTable;
    /**Can either be currentRound or previousRound of the table. Is decided on entering the activity.*/
    private Round activeRound;
    private boolean activeRoundIsPreviousRound;
    private LeaveTableTask leaveTableTask = null;
    private SubmitSelectedCardsToTableTask submitPlayerCardsTask;
    private SubmitWinnerCardsToTableTask submitWinnerCardsTask;
    private ProgressUiHelper progressUiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //retrieve gameTable from intent and determine wheter to show previous or current round
        String tableId = this.getIntent().getStringExtra(GAMETABLE_EXTRA);
        gameTable = bahApp.getGameTable(tableId);
        //if the gametable was not found, return to previous activity
        if(null==gameTable)
            finish();

        // Prepare progressUiHelper
        View currentGamesFormView = findViewById(R.id.GAME_ACTIVITY_FORM);
        View progressView = findViewById(R.id.GAME_ACTIVITY_PROGRESS);
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        progressUiHelper = new ProgressUiHelper(currentGamesFormView, progressView, shortAnimTime);

        // init fragments
        leftPage = new LeftPageFragment();
        rightPage = new RightPageFragment();

        // set active round depending on whether to show previous or current round
        activeRoundIsPreviousRound = this.getIntent().getExtras().getBoolean(PREVIOUS_ROUND_EXTRA);
        activeRound = activeRoundIsPreviousRound ? gameTable.getPreviousRound() : gameTable.getCurrentRound();

        // set black card text of active round
        TextView blackCardView = (TextView)findViewById(R.id.TXT_GAME_BLACKCARD);
        activeRound.getBlackCard().representBlanks();
        blackCardView.setText(activeRoundIsPreviousRound ? activeRound.getBlackCard().fillBlanks(activeRound.getWinnerCardsAsList()) : activeRound.getBlackCard().getText());

        // Instantiate a ViewPager and a PagerAdapter for the swiping area of the game activity
        viewPager = (ViewPager) findViewById(R.id.VP_GAME);
        pagerAdapter = new GamePagerAdapter(getFragmentManager(), leftPage, rightPage);
        viewPager.setAdapter(pagerAdapter);

        // indicate which page of the viewpager is currently shown
        UnderlinePageIndicator pageIndicator = (UnderlinePageIndicator)findViewById(R.id.VPI_GAME_PAGE_INDICATOR);
        pageIndicator.setViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register broadcast
        if (null == updateReceiver)
            updateReceiver = new GameTableUpdateReceiver(new BahRunner<String[]>() {
                @Override
                public void run(String[] tableIds) {
                Log.d(TAG, String.format("Broadcast received with tableIds %s", Arrays.toString(tableIds)));
                for(String id : tableIds) {
                    if (id.equals(getGameTableId())) {

                        // we have to update the game activity because the table changed. check if a new round has started.
                        // if so, we must put an extra to ensure user will see the result before entering the new round
                        GameTable gt = bahApp.reloadGameTable(id);
                        bahApp.reloadIdsOfAdvancedGameTables();

                        //table removed?
                        if(null==gt)
                            finish();

                        List<String> advancedTableIds = bahApp.getIdsOfAdvancedGameTables();
                        boolean roundFinished = advancedTableIds.contains(id);

                        //restart activity. TODO: better would be to just reload the GameTable. For now it's OK.
                        ActivityStarter.startGameActivity(GameActivity.this, GameActivity.this.getApplicationContext(), id, roundFinished);
                        finish();
                    }
                }
                }
            });
        IntentFilter filter = new IntentFilter(ServerPollService.ACTION_UPDATE_GAMETABLES_NOTIFICATION);
        this.registerReceiver(updateReceiver, filter);

        UserUsesAppNotificationReceiver.sendUserUsesAppBroadcast(this, UserUsesAppNotificationReceiver.ENTERS_GAME_SCREEN, getGameTableId());

        // swipe to submission page if user already submitted cards during this round
        if(getActiveRound().hasSubmittedCards(bahApp.getMainUser().getEmail()))
            swipeToRight(null);
    }

    @Override
    protected void onPause() {
        this.unregisterReceiver(updateReceiver);
        UserUsesAppNotificationReceiver.sendUserUsesAppBroadcast(this, UserUsesAppNotificationReceiver.LEAVES_GAME_SCREEN);
        super.onPause();
    }

    private static final int BACK_TO_MENU_ACTION_ID = -1;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //no super call. we don't want the default options here
        menu.add(0,BACK_TO_MENU_ACTION_ID,0, R.string.action_back_to_menu);
        getMenuInflater().inflate(R.menu.menu_gametable, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_leave_table:
                if(null != leaveTableTask && leaveTableTask.isRunning())
                    break;

                BahRunner<Void> onLeaveSuccessRunner = new BahRunner<Void>() {
                    @Override
                    public void run(Void v) {
                        //leave activity
                        onBackPressed();
                    }
                };
                leaveTableTask = new LeaveTableTask(this, progressUiHelper, gameTable.getId(), onLeaveSuccessRunner);
                leaveTableTask.execute((Void) null);
                break;

            case R.id.action_invite_player:
                ActivityStarter.startInviteActivity(this, getApplicationContext(), gameTable.getId());
                break;

            case BACK_TO_MENU_ACTION_ID:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() <= 0 || mainUserAlreadySubmitted()) {
            super.onBackPressed();
        } else {
            swipeToLeft(null);
        }
    }
    public void swipeToLeft(View view){
        if(viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    public void swipeToRight(View view){
        if(viewPager.getCurrentItem() < pagerAdapter.getCount()-1) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        }
    }

    public Round getActiveRound(){
        return activeRound;
    }

    public boolean activeRoundIsPreviousRound(){
        return activeRoundIsPreviousRound;
    }

    public boolean mainUserIsJudge(){
        return getActiveRound().isJudge(bahApp.getMainUser().getEmail());
    }

    public List<IngameWhiteCard> getHandOfMainUser(){
        List<IngameWhiteCard> cards = getActiveRound().getHandFor(bahApp.getMainUser());

        if (BuildConfig.DEBUG && null == cards || 0 == cards.size())
            throw new AssertionError(String.format("Hand of main user is invalid: %s ", cards));

        return cards;
    }

    public List<IngameWhiteCard>  getSubmittedCardsSortedBySubmissionOrder(List<IngameWhiteCard> cardsToSort){

        //determine amount of submitted cards
        int numOfSubmittedCards = 0;
        for(IngameWhiteCard card : cardsToSort)
            if(card.isSubmitted())
                numOfSubmittedCards++;

        //sort cards by submission order
        IngameWhiteCard[] submittedCards = new IngameWhiteCard[numOfSubmittedCards];
        for(IngameWhiteCard card : cardsToSort) {
            if (card.isSubmitted()) {
                submittedCards[card.getSubmittedOrder()-1] = card;
            }
        }

        //convert sorted array to list
        List<IngameWhiteCard> submittedCardList = new ArrayList<>();
        Collections.addAll(submittedCardList, submittedCards);

        return submittedCardList;
    }

    public boolean mainUserAlreadySubmitted(){
        return getActiveRound().hasSubmittedCards(bahApp.getMainUser().getEmail());
    }

    public String getGameTableId(){
        return gameTable.getId();
    }

    Map<User, List<IngameWhiteCard>> getPlayersWithSubmittedCards(){
        Map<User, List<IngameWhiteCard>> playersWithSubmittedCards = new HashMap<>();
        for(Map.Entry<User, List<IngameWhiteCard>> playerWithHand : activeRound.getPlayersWithHand().entrySet()){
            List<IngameWhiteCard> submittedCards = new ArrayList<>();
            for(IngameWhiteCard card : playerWithHand.getValue()){
                if(card.isSubmitted())
                    submittedCards.add(card);
            }
            playersWithSubmittedCards.put(playerWithHand.getKey(), submittedCards);
        }

        return playersWithSubmittedCards;
    }

    public void submitSelectedCards(View v){
        if(submitPlayerCardsTask != null && submitPlayerCardsTask.isRunning())
            return;

        // reload activity. this will update the gameTable and refresh the view
        BahRunner<Void> onSubmitSuccessRunner = new BahRunner<Void>() {
            @Override
            public void run(Void v) {
                restartActivity();
            }
        };

        // execute submission
        submitPlayerCardsTask = new SubmitSelectedCardsToTableTask(this, progressUiHelper, onSubmitSuccessRunner, leftPage.getSelectedCards(), gameTable.getId());
        submitPlayerCardsTask.execute((Void) null);
    }

    /** Restarts the GameActivity to load the currentRound of the current gametable */
    public void restartActivity(){
        restartActivity(false);
    }

    public void restartActivity(boolean inPreviousRound){
        ActivityStarter.startGameActivity(GameActivity.this, GameActivity.this.getApplicationContext(), gameTable.getId(), inPreviousRound);
        finish();
    }

    /**
     * Used by the judge to submit selected winner cards
     */
    public void submitSelectedWinner(View v){
        if(submitWinnerCardsTask != null && submitWinnerCardsTask.isRunning())
            return;

        //determine winnercards
        User userWithWinnerCards = rightPage.getPotentialWinner();
        List<IngameWhiteCard> winnerCards = getActiveRound().getSubmittedCardsByUser(userWithWinnerCards);
        winnerCards = getSubmittedCardsSortedBySubmissionOrder(winnerCards);

        // reload activity. this will update the gameTable and refresh the view. a new game round starts
        BahRunner<Void> onSubmitSuccessRunner = new BahRunner<Void>() {
            @Override
            public void run(Void v) {
                //show result to the judge
                restartActivity(true);
            }
        };

        // execute submission
        submitWinnerCardsTask = new SubmitWinnerCardsToTableTask(this, progressUiHelper, onSubmitSuccessRunner, winnerCards, gameTable.getId());
        submitWinnerCardsTask.execute((Void) null);
    }

    /**
     * This Adapter is used for the ViewPager to enable swiping between the LeftPageFragment
     * and the RightPageFragment of the GameActivity.
     */
    private static class GamePagerAdapter extends FragmentPagerAdapter {
        private static final int NUM_OF_PAGES = 2;

        private LeftPageFragment leftPage;
        private RightPageFragment rightPage;

        public GamePagerAdapter(FragmentManager manager, LeftPageFragment leftPage, RightPageFragment rightPage) {
            super(manager);
            this.leftPage = leftPage;
            this.rightPage = rightPage;
        }

        @Override
        public int getCount() {
            return NUM_OF_PAGES;
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0)
                return leftPage;
            else
                return rightPage;
        }

        @Override
        public String getPageTitle(int position) {
            if(position == 0)
                return leftPage.getTitle();
            else
                return rightPage.getTitle();
        }
    }

}

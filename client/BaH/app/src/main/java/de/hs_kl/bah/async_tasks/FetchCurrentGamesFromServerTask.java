package de.hs_kl.bah.async_tasks;

import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous loading task used to fetch and save
 * the game tables.
 */
public class FetchCurrentGamesFromServerTask extends BahAsyncTask<Void>{

    private BahActivity parentActivity;
    private BahRunner<Void> onPostRunner;
    private final NetworkServices networkServices;

    public FetchCurrentGamesFromServerTask(BahActivity currentGamesActivity, ProgressUiHelper progressUiHelper, BahRunner<Void> onPostRunner){
        super(currentGamesActivity, progressUiHelper);
        parentActivity = currentGamesActivity;
        this.onPostRunner = onPostRunner;
        this.networkServices = new NetworkServicesImp(currentGamesActivity.getBahApplication());
    }

    @Override
    protected Void doInBackground(Void... params) {
        // All tables
        networkServices.getAndSaveAllTables();
        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        super.onPostExecute(v);
        onPostRunner.run(v);
    }

}

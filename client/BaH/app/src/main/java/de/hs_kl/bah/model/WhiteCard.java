package de.hs_kl.bah.model;

import com.google.gson.JsonObject;

/**
 * Data holder representing a white card.
 */
public class WhiteCard {

    protected int id;
    protected String text;

    public WhiteCard(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public static WhiteCard createFrom(JsonObject jsonObject) {
        int _id = jsonObject.get("id").getAsInt();
        String _text = jsonObject.get("text").getAsString();
        return new WhiteCard(_id, _text);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WhiteCard other = (WhiteCard) obj;
        return id==other.id;
    }
    @Override
    public String toString() {
        return "WhiteCard [id=" + id + ", text=" + text + "]";
    }
}

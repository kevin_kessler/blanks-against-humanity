package de.hs_kl.bah.services;

import de.hs_kl.bah.async_tasks.AsyncTaskResult;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.Invitation;
import de.hs_kl.bah.model.MainUser;

/**
 * This interface contains all necessary network communications. This is the only spot in which
 * the applicatin communicates with the server
 */
public interface NetworkServices {

    MainUser registerUser(String username, String email, String password, String password_repeat);
    MainUser loginUser(String email, String password);
    String getLatestErrorMessage();
    AsyncTaskResult<GameTable> createGameTable(String gameName, boolean isPrivate);

    /**
     * Retrieves the GameTable with given tableId and saves it on the device with DataStorageService.
     * In case the server returned an error, the table will also be deleted on the device.
     */
    GameTable getAndSaveGameTable(String tableId);

    /**
     * Saves also the table if a successful request arrives.
     */
    GameTable submitSelectedCardsToTable(int[] cardIdsToSubmit, String tableId);
    /**
     * Saves also the table if a successful request arrives.
     */
    GameTable submitWinnerCardsToTable(int[] cardIdsToSubmit, String tableId);
    /**
     * Saves also the table if a successful request arrives.
     */
    AsyncTaskResult<GameTable> joinRandomGameTable();

    /**
     * Sends a request to remove user with email.
     * Saves also the table if a successful request arrives.
     */
    boolean removeUserFromGameTable(String email, String gameTableId);

    /**
     * Retrieves all GameTables for current user and returns if it succeeded.
     */
    boolean getAndSaveAllTables();

    /**
     * Invites a user with email to GameTable with id tableId
     */
    boolean inviteUser(Invitation invitation);

    AsyncTaskResult<GameTable> acceptInvitation(String tableId);

    AsyncTaskResult<Boolean> declineInvitation(String tableId);
}

package de.hs_kl.bah.android_services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.hs_kl.bah.async_tasks.BahRunner;
import hs_kl.de.bah.BuildConfig;

/**
 * This receiver gets notifications when the user enters/exits CurrentGameActivity and
 * GameActivity.
 * Used to send information from the activities to ServerPollService.
 */
public class UserUsesAppNotificationReceiver extends BroadcastReceiver {

    public static final String USES_NOTIFICATION_ACTION = "de.hs_kl.bah.intent.action.ACTION_USES_APP_NOTIFICATION";

    public static final String EXTRA_USES_NOTIFICATION = "extra_uses_notification_key";
    public static final String EXTRA_TABLE_ID = "extra_table_id_key";

    public static final int ENTERS_MAIN_SCREEN = 0;
    public static final int LEAVES_MAIN_SCREEN = 1;
    public static final int ENTERS_GAME_SCREEN = 2;
    public static final int LEAVES_GAME_SCREEN = 3;

    public static void sendUserUsesAppBroadcast(Context context, int notificationType) {
        sendUserUsesAppBroadcast(context, notificationType, null);
    }
    public static void sendUserUsesAppBroadcast(Context context, int notificationType, String tableId){

        if(BuildConfig.DEBUG && ! (notificationType == ENTERS_MAIN_SCREEN ||
                                   notificationType == LEAVES_MAIN_SCREEN ||
                                   (notificationType == ENTERS_GAME_SCREEN && null != tableId && ! tableId.isEmpty()) ||
                                   notificationType == LEAVES_GAME_SCREEN))
            throw new AssertionError(String.format("Wrong notificationType %d", notificationType));
        Intent intent = new Intent(USES_NOTIFICATION_ACTION);
        intent.putExtra(EXTRA_USES_NOTIFICATION, notificationType);
        if (null != tableId)
            intent.putExtra(EXTRA_TABLE_ID,  tableId);
        context.sendBroadcast(intent);
    }


    BahRunner<Intent> bahRunner;

    public UserUsesAppNotificationReceiver(BahRunner<Intent> bahRunner) {

        this.bahRunner = bahRunner;

    }

    public void onReceive(Context context, Intent intent) {

        if (null != bahRunner)
            bahRunner.run(intent);

    }
}

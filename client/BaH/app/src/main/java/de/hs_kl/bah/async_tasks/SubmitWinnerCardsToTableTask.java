package de.hs_kl.bah.async_tasks;

import android.widget.Toast;

import java.util.List;

import de.hs_kl.bah.android_services.DataStorageServicesImp;
import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.GameTable;
import de.hs_kl.bah.model.IngameWhiteCard;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous 'submit cards' task used to submit the winner cards of a round
 */
public class SubmitWinnerCardsToTableTask extends BahAsyncTask<GameTable> {

    private final BahActivity parentActivity;
    private final BahRunner onSuccessRunner;
    private final NetworkServices netWorkService;
    private final List<IngameWhiteCard> cardsToSubmit;
    private final String tableId;

    public SubmitWinnerCardsToTableTask(BahActivity parentActivity, ProgressUiHelper uiProgressUiHelper, BahRunner onSucessRunner, List<IngameWhiteCard> cardsToSubmit, String tableId) {
        super(parentActivity, uiProgressUiHelper);
        this.parentActivity = parentActivity;
        this.netWorkService = new NetworkServicesImp(parentActivity.getBahApplication());
        this.cardsToSubmit = cardsToSubmit;
        this.tableId = tableId;
        this.onSuccessRunner = onSucessRunner;
    }

    @Override
    protected GameTable doInBackground(Void... params) {
        int[] cardIds = new int[cardsToSubmit.size()];
        for (int i = 0; i < cardIds.length; i++) {
            cardIds[i] = cardsToSubmit.get(i).getId();
        }
        return netWorkService.submitWinnerCardsToTable(cardIds, tableId);
    }

    @Override
    protected void onPostExecute(final GameTable gameTable) {
        super.onPostExecute(gameTable);

        if (null == gameTable || !gameTable.isValid()) {
            Toast.makeText(parentActivity, netWorkService.getLatestErrorMessage(), Toast.LENGTH_LONG).show();
        } else {
            (new DataStorageServicesImp(parentActivity)).save(gameTable);
            onSuccessRunner.run((Void) null);
        }
    }

}

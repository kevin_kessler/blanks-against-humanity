package de.hs_kl.bah.controller;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.model.MainUser;
import de.hs_kl.bah.services.DataStorageServices;
import hs_kl.de.bah.R;

/**
 * The base class for all activities in which the MainUser data need to exist.
 * For all except Login- and RegistrationActivity.
 * It holds common used data and the options menu.
 */
public abstract class BahActivity extends FragmentActivity {

    protected BahApplication bahApp;
    protected String TAG = BahActivity.class.getSimpleName();

    protected DataStorageServices mDataStorageServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bahApp = (BahApplication) getApplication();
        MainUser mainUser = bahApp.getMainUser();
        if (null == mainUser || !mainUser.isValid()) {
            logoutAndRedirectToLoginActivity();
            finish();
        }

        mDataStorageServices = bahApp.getDataStorageServices();
    }

    private void logoutAndRedirectToLoginActivity() {
        ActivityStarter.clearActivityStackAndStartLoginActivity(this, getApplicationContext());
    }

       @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_suggest_card:
                //TODO: start activity
                Toast.makeText(this, "Not implemented yet", Toast.LENGTH_LONG).show();
                break;
            case R.id.action_settings:
                ActivityStarter.startSettingsActivity(this, getApplicationContext());
                break;
            case R.id.action_help:
                ActivityStarter.startHelpActivity(this, getApplicationContext());
                break;
            case R.id.action_logout:
                logoutAndRedirectToLoginActivity();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    public BahApplication getBahApplication(){
        return bahApp;
    }

}

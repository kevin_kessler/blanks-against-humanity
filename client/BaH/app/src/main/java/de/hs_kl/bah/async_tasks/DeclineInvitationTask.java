package de.hs_kl.bah.async_tasks;

import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous loading task used to decline an invitation.
 */
public class DeclineInvitationTask extends BahAsyncTask<AsyncTaskResult<Boolean>>{

    private BahRunner<AsyncTaskResult<Boolean>> onPostRunner;
    private final NetworkServices networkServices;
    private final String tableId;

    public DeclineInvitationTask(BahActivity currentGamesActivity, ProgressUiHelper progressUiHelper, String tableId, BahRunner<AsyncTaskResult<Boolean>> onPostRunner){
        super(currentGamesActivity, progressUiHelper);
        this.onPostRunner = onPostRunner;
        this.networkServices = new NetworkServicesImp(currentGamesActivity.getBahApplication());
        this.tableId = tableId;
    }

    @Override
    protected AsyncTaskResult<Boolean> doInBackground(Void... params) {
        return networkServices.declineInvitation(tableId);
    }

    @Override
    protected void onPostExecute(AsyncTaskResult<Boolean> result) {
        super.onPostExecute(result);
        onPostRunner.run(result);
    }

}

package de.hs_kl.bah.async_tasks;

import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.model.Invitation;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Represents an asynchronous loading task used to invite a
 * user.
 */
public class InviteTask extends BahAsyncTask<Boolean>{

    private BahRunner<Boolean> onPostRunner;
    private final NetworkServices networkServices;
    private final Invitation invitation;

    public InviteTask(BahActivity currentGamesActivity, ProgressUiHelper progressUiHelper, Invitation invitation, BahRunner<Boolean> onPostRunner){
        super(currentGamesActivity, progressUiHelper);
        this.onPostRunner = onPostRunner;
        this.networkServices = new NetworkServicesImp(currentGamesActivity.getBahApplication());
        this.invitation = invitation;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return networkServices.inviteUser(invitation);
    }

    @Override
    protected void onPostExecute(Boolean successfulInvitation) {
        super.onPostExecute(successfulInvitation);
        onPostRunner.run(successfulInvitation);
    }

}

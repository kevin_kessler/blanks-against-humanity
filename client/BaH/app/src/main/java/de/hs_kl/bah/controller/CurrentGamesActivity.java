package de.hs_kl.bah.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import de.hs_kl.bah.android_services.ActivityStarter;
import de.hs_kl.bah.android_services.ServerPollService;
import de.hs_kl.bah.android_services.UserUsesAppNotificationReceiver;
import de.hs_kl.bah.async_tasks.AcceptInvitationTask;
import de.hs_kl.bah.async_tasks.AsyncTaskResult;
import de.hs_kl.bah.async_tasks.BahRunner;
import de.hs_kl.bah.async_tasks.DeclineInvitationTask;
import de.hs_kl.bah.async_tasks.FetchCurrentGamesFromServerTask;
import de.hs_kl.bah.async_tasks.JoinRandomGameTask;
import de.hs_kl.bah.async_tasks.LeaveTableTask;
import de.hs_kl.bah.async_tasks.LoadCurrentGamesTask;
import de.hs_kl.bah.controller.util.AsyncTaskResultHandler;
import de.hs_kl.bah.controller.util.GameTableUpdateReceiver;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.controller.util.ToastResultHandler;
import de.hs_kl.bah.model.GameTable;
import hs_kl.de.bah.BuildConfig;
import hs_kl.de.bah.R;

/**
 * This is the main activity of the application.
 * The user automatically enters this activity, if he is logged in.
 * Here a lot of things happen.
 * - The mainuser information is displayed and updated
 * - All of the active gametables that the mainuser is taking part at, are displayed here
 * - Starts the background ServerPollService, which is responsible for updating mainuser information and gametables
 * - Offers different options, e.g. Create New Game, Join Random Game (Quick Play), Settings, etc.
 * - The listitems of the gametables list provide context menus to leave the table, invite players, accept or decline invitation
 * - ...
 */
public class CurrentGamesActivity extends BahActivity implements AdapterView.OnItemClickListener {

    public static final String LOGOUT_EXTRA = "logout_flag";
    final int ACITIVITY_FOR_RESULT_CREATE_GAME_REQUEST = 1;

    private LoadCurrentGamesTask loadingTask = null;
    private LeaveTableTask leaveTableTask = null;
    private AcceptInvitationTask acceptInvitationTask = null;
    private DeclineInvitationTask declineInvitationTask = null;
    private JoinRandomGameTask joinRandomGameTask = null;
    private FetchCurrentGamesFromServerTask fetchCurrentGamesFromServerTask = null;
    private ProgressUiHelper progressUiHelper;

    private TextView tvCredit;
    private TextView tvUsername;

    private GameTableUpdateReceiver updateReceiver;
    private TextView textView;
    private ListView gamesListView;
    private List<GameTable> gameTables;
    private List<String> idsOfAdvancedGameTables;

    // Boolean to skip loadTables() in onResume on first invocation
    boolean invokingFirstTimeOnResume = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_games);

        if(getIntent().hasExtra(LOGOUT_EXTRA)) {
            // Then some activity send a 'logout command'
            bahApp.logOutUser();
            ActivityStarter.simpleStartLoginActivity(this, getApplicationContext());
            finish();
            return;
        }

        ActivityStarter.startPollServiceIfMainUserExists(this);

        textView = (TextView) findViewById(R.id.EMPTY_NOTIFICATION_VIEW);
        gamesListView = (ListView) findViewById(R.id.CURRENT_GAMES_LISTVIEW);

        // Prepare progressUiHelper
        View currentGamesFormView = findViewById(R.id.CURRENT_GAMES_FORM);
        View progressView = findViewById(R.id.CURRENT_GAMES_PROGRESS);
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        progressUiHelper = new ProgressUiHelper(currentGamesFormView, progressView, shortAnimTime);


        tvUsername = (TextView) findViewById(R.id.TXT_USERNAME);
        tvCredit = (TextView) findViewById(R.id.TXT_CREDIT);

        loadTables(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //display user information
        bahApp.reloadMainUser();  // in case the credit account has changed
        tvUsername.setText(bahApp.getMainUser().getUsername());
        tvCredit.setText(Integer.toString(bahApp.getMainUser().getCredit()));

        // sync game tables
        fetchCurrentGamesFromServer();

        gameTables = bahApp.getGameTables();

        // Get table ids of tables that have a new round and the previous result was not shown to the user yet
        idsOfAdvancedGameTables = bahApp.getIdsOfAdvancedGameTables();

        // Register broadcast
        if (null == updateReceiver)
            updateReceiver = new GameTableUpdateReceiver(new BahRunner<String[]>() {
                @Override
                public void run(String[] tableIds) {
                    Log.d(TAG, String.format("Broadcast received with tableIds %s", Arrays.toString(tableIds)));

                    //reload list of advanced game tables if necessary (e.g. if table has been removed on the server)
                    bahApp.reloadIdsOfAdvancedGameTables();
                    idsOfAdvancedGameTables = bahApp.getIdsOfAdvancedGameTables();

                    loadTables(false);
                    updateCreditTextView();
                }
            });

        IntentFilter filter = new IntentFilter(ServerPollService.ACTION_UPDATE_GAMETABLES_NOTIFICATION);
        this.registerReceiver(updateReceiver, filter);

        // Check if a gameTableId was passed to which the user should be redirected
        String tableId = this.getIntent().getStringExtra(GameActivity.GAMETABLE_EXTRA);
        if(null != tableId) {
            getIntent().removeExtra(GameActivity.GAMETABLE_EXTRA);
            startGameActivityWith(tableId);
        }

        UserUsesAppNotificationReceiver.sendUserUsesAppBroadcast(this, UserUsesAppNotificationReceiver.ENTERS_MAIN_SCREEN);

        // Skip first invocation since we load also in onCreate (but with 'true' as parameter)
        if (invokingFirstTimeOnResume) {
            invokingFirstTimeOnResume = false;
        } else {
            loadTables(false);
        }
    }

    @Override
    protected void onPause() {
        gameTables = null;
        this.unregisterReceiver(updateReceiver);
        UserUsesAppNotificationReceiver.sendUserUsesAppBroadcast(this, UserUsesAppNotificationReceiver.LEAVES_MAIN_SCREEN);
        super.onPause();
    }

    private void loadTables(boolean showProgressCircle) {
        if (loadingTask != null && loadingTask.isRunning()) {
            return;
        }

        BahRunner<Void> onPostLoadRunner = new BahRunner<Void>() {
            @Override
            public void run(Void v) {
                displayTables();
            }
        };
        ProgressUiHelper uiHelper = showProgressCircle ? progressUiHelper : null;
        loadingTask = new LoadCurrentGamesTask(this, uiHelper, onPostLoadRunner);
        loadingTask.execute((Void) null);
    }

    private void updateCreditTextView() {
        if (updateReceiver.aNewRoundHasStarted()) {
            bahApp.reloadMainUser();
            tvCredit.setText(Integer.toString(bahApp.getMainUser().getCredit()));
        }
    }


    public void displayTables() {
        gameTables = bahApp.getGameTables();

        if (BuildConfig.DEBUG && null == gameTables)
            throw new AssertionError("GameTables is null");

        boolean listIsEmpty = gameTables.isEmpty();

        if (!listIsEmpty) {
            String[] gameTableNames = new String[gameTables.size()];
            GameTable.TableRole[] tableRoles = new GameTable.TableRole[gameTables.size()];
            for(int i=0; i< gameTables.size(); i++){
                gameTableNames[i] = gameTables.get(i).getName();
                tableRoles[i] = gameTables.get(i).determineTableRoleOfUser(bahApp.getMainUser().getEmail());
            }
            CurrentGamesListAdapter adapter = new CurrentGamesListAdapter(this, gameTableNames, tableRoles);
            gamesListView.setAdapter(adapter);
            gamesListView.setOnItemClickListener(this);
        }

        textView.setVisibility(listIsEmpty ? View.VISIBLE : View.GONE);
        gamesListView.setVisibility(listIsEmpty ? View.GONE : View.VISIBLE);

        registerForContextMenu(gamesListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_current_games, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_create_game:
                Intent intent = new Intent(this, CreateGameActivity.class);
                startActivityForResult(intent, ACITIVITY_FOR_RESULT_CREATE_GAME_REQUEST);
                break;
            case R.id.action_join_random:
                attemptToJoinRandomGame();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACITIVITY_FOR_RESULT_CREATE_GAME_REQUEST) {
            //if user created a game via the options menu of this activity, finish the activity.
            //otherwise we would have the current games activity twice on stack, since start game always goes over current games activity.
            if (resultCode == RESULT_OK) {
                Log.e("HALLO", "FINISHING");
                finish();
            }
            //else nothing to do. the user decided to not create a game
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==gamesListView.getId()) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            final GameTable gameTable = gameTables.get(info.position);
            menu.setHeaderTitle(gameTable.getName());

            //invitation context menu
            if(gameTable.isInvited(bahApp.getMainUser().getEmail())){
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu_handle_invitation, menu);
            }
            //default context menu
            else {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu_gametable, menu);
            }

        }
    }



    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        GameTable gameTableToOperateOn = gameTables.get(info.position);


        int menuItemId = item.getItemId();
        switch(menuItemId) {
            case R.id.action_leave_table:
                attemptToLeaveTable(gameTableToOperateOn.getId());
                break;
            case R.id.action_invite_player:
                ActivityStarter.startInviteActivity(this, getApplicationContext(), gameTableToOperateOn.getId());
                break;

            case R.id.action_accept_invitation:
                attemptToAcceptInvitation(gameTableToOperateOn.getId());
                break;

            case R.id.action_decline_invitation:
                attemptToDeclineInvitation(gameTableToOperateOn.getId());
                break;

            case R.id.action_cancel_invitation:
                //nothing to do here
                break;

            default:
                return super.onContextItemSelected(item);
        }

        return true;
    }

    public void startGameActivityWith(GameTable gameTable) {
        startGameActivityWith(gameTable.getId());
    }

    public void startGameActivityWith(String gameTableId){
        boolean redirectToPreviousRound = idsOfAdvancedGameTables.contains(gameTableId);
        ActivityStarter.startGameActivity(this, getApplicationContext(), gameTableId, redirectToPreviousRound);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final GameTable gameTable = gameTables.get(position);
        if(gameTable.isInvited(bahApp.getMainUser().getEmail()))
        {
            popupInvitationDecisionFor(gameTable);
        } else {
            startGameActivityWith(gameTable);
        }
    }

    private void popupInvitationDecisionFor(final GameTable gameTable) {

        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_invitation_msg_)
                .setPositiveButton(R.string.dialog_pos_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        attemptToAcceptInvitation(gameTable.getId());
                    }
                })
                .setNegativeButton(R.string.dialog_neg_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        attemptToDeclineInvitation(gameTable.getId());
                    }
                })
                .setNeutralButton(R.string.dialog_neutral_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Nothing to do
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void attemptToAcceptInvitation(String tableId) {
        if (anAsynTaskIsRunning())
            return;

        acceptInvitationTask = new AcceptInvitationTask(this, progressUiHelper, tableId, new BahRunner<AsyncTaskResult<GameTable>>() {
            @Override
            public void run(AsyncTaskResult<GameTable> result) {
                String successMsg = CurrentGamesActivity.this.getResources().getString(R.string.successfully_accepted);
                AsyncTaskResultHandler handler = new ToastResultHandler(CurrentGamesActivity.this, successMsg, result);
                handler.handleResult();

                loadTables(true);
                acceptInvitationTask = null;
            }
        });
        acceptInvitationTask.execute((Void) null);
    }


    private void attemptToDeclineInvitation(String tableId) {
        if (anAsynTaskIsRunning())
            return;

        // asyncTask = new DeclineInvitationTask(this, progressUiHelper, tableId, new BahRunner<Boolean>() {
        declineInvitationTask = new DeclineInvitationTask(this, progressUiHelper, tableId, new BahRunner<AsyncTaskResult<Boolean>>() {
            @Override
            public void run(AsyncTaskResult<Boolean> result) {
                String successMsg = CurrentGamesActivity.this.getResources().getString(R.string.successfully_declined);
                AsyncTaskResultHandler handler = new ToastResultHandler(CurrentGamesActivity.this, successMsg, result);
                handler.handleResult();


                loadTables(true);
                declineInvitationTask = null;
            }
        });
        declineInvitationTask.execute((Void) null);
    }

    private void attemptToLeaveTable(String tableId){
        if(anAsynTaskIsRunning())
            return;

        BahRunner<Void> onLeaveSuccessRunner = new BahRunner<Void>() {
            @Override
            public void run(Void v) {
                loadTables(false);
                leaveTableTask = null;
            }
        };
        leaveTableTask = new LeaveTableTask(this, progressUiHelper, tableId, onLeaveSuccessRunner);
        leaveTableTask.execute((Void) null);
    }

    private void attemptToJoinRandomGame(){
        if(anAsynTaskIsRunning())
            return;

        joinRandomGameTask = new JoinRandomGameTask(this, progressUiHelper);
        joinRandomGameTask.execute((Void) null);
    }

    private void fetchCurrentGamesFromServer(){
        if (null == fetchCurrentGamesFromServerTask) {
            fetchCurrentGamesFromServerTask = new FetchCurrentGamesFromServerTask(this, progressUiHelper, new BahRunner<Void>() {
                @Override
                public void run(Void v) {
                    bahApp.reloadGameTables();
                    fetchCurrentGamesFromServerTask = null;
                }
            });

            fetchCurrentGamesFromServerTask.execute((Void)null);
        }
    }

    private boolean noAsynTaskIsRunning() {
        return null == leaveTableTask && null == acceptInvitationTask && null == declineInvitationTask && (null == joinRandomGameTask || !joinRandomGameTask.isRunning());
    }
    private boolean anAsynTaskIsRunning() {
        return ! noAsynTaskIsRunning();
    }

    /**
     * Responsible for initing the different items of the gametables listview
     */
    private static class CurrentGamesListAdapter extends ArrayAdapter<String> {
        private final Activity context;
        private final GameTable.TableRole[] tableRoles;
        String[] tableNames;

        public CurrentGamesListAdapter(Activity context, String[] tableNames, GameTable.TableRole[] tableRoles) {

            super(context, R.layout.listitem_current_games, tableNames);

            this.context = context;
            this.tableNames = tableNames;
            this.tableRoles = tableRoles;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = context.getLayoutInflater();

            View rowView = inflater.inflate(R.layout.listitem_current_games, null, true);
            TextView gameTitle = (TextView) rowView.findViewById(R.id.TXT_LISTITEM_CURRENT_GAMES_TITLE);
            TextView gameText = (TextView) rowView.findViewById(R.id.TXT_LISTITEM_CURRENT_GAMES_TEXT);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.IMG_LISTITEM_CURRENT_GAMES_ICON);
            gameTitle.setText(tableNames[position]);
            gameText.setText(getTableTextByRole(tableRoles[position]));
            imageView.setImageResource(getTableImageIdByRole(tableRoles[position]));

            return rowView;
        }

        private String getTableTextByRole(GameTable.TableRole role) {

            switch (role) {

                case PLAYING_ACTION_NECESSARY:
                    return context.getResources().getString(R.string.gamestate_player_action);

                case PLAYING:
                    return context.getResources().getString(R.string.gamestate_player_wait);

                case PLAYING_NOT_ENOUGH_PLAYERS:
                    return context.getResources().getString(R.string.gamestate_player_no_players);

                case JUDGING:
                    return context.getResources().getString(R.string.gamestate_judge_wait);

                case JUDGING_ACTION_NECESSARY:
                    return context.getResources().getString(R.string.gamestate_judge_action);

                case JUDGING_NOT_ENOUGH_PLAYERS:
                    return context.getResources().getString(R.string.gamestate_judge_no_players);

                case INVITED:
                    return context.getResources().getString(R.string.gamestate_invited);

                default:
                    throw new AssertionError(String.format("New TableType? %s", role));

            }
        }

        private int getTableImageIdByRole(GameTable.TableRole role) {

            switch (role) {

                case PLAYING_ACTION_NECESSARY:
                    return R.drawable.not_submitted;

                case PLAYING:
                    return R.drawable.submitted;

                case PLAYING_NOT_ENOUGH_PLAYERS:
                    return R.drawable.not_enough_players;

                case JUDGING:
                    return R.drawable.judge_icon;

                case JUDGING_ACTION_NECESSARY:
                    return R.drawable.judge_action_icon;

                case JUDGING_NOT_ENOUGH_PLAYERS:
                    return R.drawable.not_enough_players;

                case INVITED:
                    return R.drawable.invitation;

                default:
                    throw new AssertionError(String.format("New TableType? %s", role));

            }
        }
    }

}

package de.hs_kl.bah.controller.util;

import android.app.Activity;
import android.widget.Toast;

import de.hs_kl.bah.async_tasks.AsyncTaskResult;
import hs_kl.de.bah.BuildConfig;

/**
 * Handles AsyncTaskResults with Toasts.
 * Either toasts an success message or the error message of  the AsyncTaskResult
 */
public class ToastResultHandler extends AsyncTaskResultHandler {

    private final Activity activity;

    private final String successMsg;

    private final AsyncTaskResult<?> result;

    public ToastResultHandler(Activity activity, String successMsg, AsyncTaskResult<?> result) {

        if (BuildConfig.DEBUG &&
                null == activity || null == successMsg || null == result)
            throw new AssertionError(String.format("No valid parameters: \nactivity %s\nsuccessMsg %s\nresult %s", activity, successMsg, result));

        this.activity = activity;
        this.successMsg = successMsg;
        this.result = result;
    }

    @Override
    public boolean wasSuccessful() {
        return result.wasSuccessful();
    }

    @Override
    public void handleSuccess() {
        toast(successMsg);
    }

    @Override
    public void handleFailure() {
        toast(result.getErrMsg());
    }

    private void toast(String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }
}

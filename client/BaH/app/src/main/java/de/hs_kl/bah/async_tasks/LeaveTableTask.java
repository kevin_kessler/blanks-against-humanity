package de.hs_kl.bah.async_tasks;

import android.widget.Toast;

import de.hs_kl.bah.android_services.NetworkServicesImp;
import de.hs_kl.bah.controller.BahActivity;
import de.hs_kl.bah.controller.util.ProgressUiHelper;
import de.hs_kl.bah.services.NetworkServices;

/**
 * Used to remove the MainUser from give GameTable with tableId
 */
public class LeaveTableTask extends BahAsyncTask<Boolean>  {

    private final BahActivity parentActivity;
    private final NetworkServices netWorkService;
    private final String tableId;
    private final BahRunner<Void> onSuccessRunner;

    public LeaveTableTask(BahActivity parentActivity, ProgressUiHelper progressUiHelper, String tableId, BahRunner<Void> onSuccessRunner) {
        super(parentActivity, progressUiHelper);
        this.parentActivity = parentActivity;
        this.netWorkService = new NetworkServicesImp(parentActivity.getBahApplication());
        this.tableId = tableId;
        this.onSuccessRunner = onSuccessRunner;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return netWorkService.removeUserFromGameTable(parentActivity.getBahApplication().getMainUser().getEmail(), tableId);
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        super.onPostExecute(success);

        if (!success) {
            Toast.makeText(parentActivity, netWorkService.getLatestErrorMessage(), Toast.LENGTH_LONG).show();
        }else{
            parentActivity.getBahApplication().leaveTable(tableId);
            onSuccessRunner.run((Void) null);
        }
    }
}

<!--***************************************************
 * The skeleton of the page. it serves as layout for every other page. 
 */-->


 <!-- set charset and import JSTL Core -->
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><c:out value='${requestScope["title"]}' escapeXml="false"/></title>
		
		<!-- load the css sheets, that are used by every page -->
		<link rel="stylesheet" type="text/css" href="">
	</head>
<body>
	<c:choose>
		<c:when test='${not empty content}'>
			<jsp:include page='${requestScope["content"]}'/>
		</c:when>
		<c:otherwise>
			BAH Server
		</c:otherwise>
	</c:choose>			
</body>
</html>
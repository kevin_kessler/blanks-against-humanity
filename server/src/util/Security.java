package util;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Security {

	/**
	 * Generates a secure password by encrypting the given password string and applying
	 * the given salt
	 * 
	 * @param password
	 * @param salt
	 * @return the generated password hash
	 */
	public static byte[] generatePassword(String password, byte[] salt) {
		try {
			SecretKeyFactory f = SecretKeyFactory
					.getInstance("PBKDF2WithHmacSHA1");
			KeySpec ks = new PBEKeySpec(password.toCharArray(), salt, 10000,
					160);
			SecretKey s = f.generateSecret(ks);
			Key k = new SecretKeySpec(s.getEncoded(), "AES");
			return k.getEncoded();
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			return new byte[0];
		}
	}

	/**
	 * Generates a random salt used to generate secure passwords
	 * 
	 * @return
	 */
	public static byte[] generateSalt() {
		try {
			SecureRandom random;
			random = SecureRandom.getInstance("SHA1PRNG");
			byte[] salt = new byte[8];
			random.nextBytes(salt);
			return salt;
		} catch (NoSuchAlgorithmException e) {
			return new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		}
	}

}

package util;

public class ErrorCode 
{
		/**
		 * No Error occured. Everything fine.
		 * HTTP Response Code OK: 200
		 */
		public static final int NO_ERROR = 200; 	
		
		/** The given email does not match the regex pattern.*/
		public static final int INVALID_EMAIL = 418; 				
		
		/** The given username does not match the regex pattern.*/
		public static final int INVALID_USERNAME = 419; 	
		
		/** The given password does not match the regex pattern.*/
		public static final int INVALID_PASSWORD = 420; 			
		
		/** The repeated password does not match the other one.*/
		public static final int PASSWORD_NOT_EQUAL = 421;
		
		/** The given email for registration is already contained in the database */
		public static final int EMAIL_TAKEN = 422;
		
		/** The given user email does not match the given password for login */
		public static final int INVALID_USER_CREDENTIALS = 423;
		
		/** No user of the database matches the given input*/
		public static final int USER_NOT_FOUND = 424;
		
		/** A required parameter is missing or not correct */
		public static final int WRONG_REQUEST_PARAMETER = 425;

		public static final int USERJOIN_TO_TABLE_NOT_POSSIBLE = 426;

		/** If a user invitation fails */
		public static final int INVITATION_FAILED = 427;
}

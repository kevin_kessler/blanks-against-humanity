package servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commandlayer.Broker;
import commandlayer.Command;

/**
 * Central Servlet to receive all requests. 
 * Converts the request into a command and executes it.<br />
 * The 'command' parameter is used to decide which command will be executed.
 */
// @WebServlet("/index") //Annotation Support erst ab Tomcat7!
public class CentralServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(CentralServlet.class.getName());
	static{
		LOG.setLevel(Level.ALL);
	}
	
	public CentralServlet() {
		super();
	}

	private static Broker mBroker;

	/**
	 * In the initialisation it is important to create the broker with all
	 * commands.
	 * 
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		super.init();
		mBroker = Broker.getBroker();
		mBroker.setContext(this.getServletContext());
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			doRequest(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			doRequest(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Receives all requests. 
	 * Transfers the request to a command and executes it.<br />
	 * The 'command' parameter is used to decide which command will be executed.
	 * @throws ServletException
	 * @throws IOException
	 */
	private void doRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		// Get command
		String cmdName = request.getParameter("command");
		Command cmd = mBroker.getCommandByName(cmdName);

		if (null == cmd) {
			response.getWriter().println("Command not found!");
			return;
		}
		LOG.log(Level.FINEST, "Executing requested command: "+cmdName);

		// Execute command. Performs logic and forwards to view.
		cmd.execute(request, response);
	}

}

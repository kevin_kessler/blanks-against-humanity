package commandlayer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * <br/>
 * This class serves as parent class for all commands. 
 * The servlet will forward a request by calling the 
 * execute() method. 
 */
public abstract class Command {

	protected static Gson gsonInstance = new GsonBuilder().create();
	private static String SUCCESSFUL_RESPONSE_TEMPLATE = "{\"state\": \"OK\", \"content\": %s}";
	private static String FAILED_RESPONSE_TEMPLATE = "{\"state\": \"FAILED\", \"errorcode\": %d, \"message\": \"%s\"}";
	
	protected String mName; //the command name
	protected String mTitle; //the title of the html document
	protected String mContent; //the path to the content jsp file
	
	public Command(String name, String title, String content){
		mName = name;
		mTitle = title;
		mContent = content;
	}
	
	public abstract void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	public void setRequestAttributes(HttpServletRequest request){
		request.setAttribute("title", mTitle);
		request.setAttribute("content", mContent);
	}
	
	public String getName(){
		return mName;
	}
	
	public String getTitle(){
		return mTitle;
	}

	public String getContent() {
		return mContent;
	}
	
	/**
	 * Sends the successful json response
	 * @param response
	 * @param contentAsJsonObject will be inserted in
	 * @throws IOException 
	 */
	protected void sendSuccess(HttpServletResponse response, String contentAsJsonObject) throws IOException {
		if (null == contentAsJsonObject || contentAsJsonObject.isEmpty())
			contentAsJsonObject = "{}";
		String responseMsg = String.format(SUCCESSFUL_RESPONSE_TEMPLATE, contentAsJsonObject);
		response.getWriter().print(responseMsg);		
	}
	
	protected void sendError(HttpServletResponse response, int code, String errMsg) throws IOException {
		String responseMsg = String.format(FAILED_RESPONSE_TEMPLATE, code, errMsg);
		response.getWriter().print(responseMsg);
	}
}

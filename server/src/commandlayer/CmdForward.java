package commandlayer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class extends Command and is only used for forwarding to the specified content jsp.
 * There is no logic besides setting the required attributes to the request object.
 */
public class CmdForward extends Command {
	private static final Logger LOG = Logger.getLogger(CmdForward.class.getName());
	static{
		LOG.setLevel(Level.ALL);
	}
	
	public CmdForward(String name, String title, String content){
		super(name, title, content);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//set request attributes
		setRequestAttributes(request);
		
		LOG.log(Level.FINEST, "Forwarding to "+getContent());
		RequestDispatcher reqdis = request.getRequestDispatcher("index.jsp");
		reqdis.forward(request, response);
	}
}

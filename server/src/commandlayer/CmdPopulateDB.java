package commandlayer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistencelayer.BAHPersistenceException;
import persistencelayer.DBFacade;

/**
 * This class extends Command and is used for test purposes (populating db with dummy content).
 */
public class CmdPopulateDB extends Command {
	private static final Logger LOG = Logger.getLogger(CmdPopulateDB.class.getName());
	static{
		LOG.setLevel(Level.ALL);
	}
	
	public CmdPopulateDB(String name, String title, String content){
		super(name, title, content);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//set request attributes
		setRequestAttributes(request);
		
		//populate DB
		try {
			DBFacade.getInstance().createDBContent();
			LOG.log(Level.FINER, "Sucessfully populated DB");
		} catch (BAHPersistenceException e) {
			LOG.log(Level.WARNING, "Could not populate DB: "+ e.toString());
			e.printStackTrace();
		}
		
		// Forward the request to view
		RequestDispatcher reqdis = Broker.getBroker().getContext().getRequestDispatcher("/index.jsp");
		reqdis.forward(request, response);
	}
}

package commandlayer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import persistencelayer.BAHPersistenceException;
import persistencelayer.DBFacade;
import persistencelayer.User;
import util.ErrorCode;

/**
 * This class extends Command and is used to login a user.
 * Example call: 
 * http://localhost:8080/bah_server/index?command=login&email=anna.bolika@hs-kl.de&pwd=training
 */
public class CmdLogin extends Command {

	private static final Logger LOG = Logger.getLogger(CmdLogin.class.getName());
	static{
		LOG.setLevel(Level.ALL);
	}
	
	public CmdLogin(String name, String title, String content){
		super(name, title, content);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//set request attributes
		setRequestAttributes(request);
		
		String email = request.getParameter("email");
		String passwd = request.getParameter("pwd");
		
		//try to login
		User user = null;
		int validationCode = ErrorCode.NO_ERROR;
		try {
			user = DBFacade.getInstance().findUserByEmail(email);
			boolean isAuthorized = DBFacade.getInstance().checkLogin(user, passwd);
			if(!isAuthorized)
				validationCode = ErrorCode.INVALID_USER_CREDENTIALS;
		} catch (BAHPersistenceException e) {
			validationCode = ErrorCode.INVALID_USER_CREDENTIALS;
		}
		
		//login failed?
		if(validationCode != ErrorCode.NO_ERROR){
			LOG.log(Level.FINE, "Login attempt failed: " + email);
			sendError(response, validationCode, "Email or password wrong");
			return;
		}
		
		//login ok -> create session, return user and session id as json
		LOG.log(Level.FINE, "Successfully logged in as " + email);
		HttpSession session = request.getSession(true);
		Gson doNotExposeGsonInstance = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		JsonObject json_user = (JsonObject) doNotExposeGsonInstance.toJsonTree(user);
		json_user.addProperty("JSESSIONID", session.getId());
		
		sendSuccess(response, json_user.toString());
	}
}

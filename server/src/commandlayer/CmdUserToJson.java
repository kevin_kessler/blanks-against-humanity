package commandlayer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistencelayer.BAHPersistenceException;
import persistencelayer.DBFacade;
import persistencelayer.User;
import util.ErrorCode;

/**
 * This class extends Command and is used to login a user.
 */
public class CmdUserToJson extends Command {
	private static final Logger LOG = Logger.getLogger(CmdUserToJson.class.getName());
	static{
		LOG.setLevel(Level.ALL);
	}
	
	
	public CmdUserToJson(String name, String title, String content){
		super(name, title, content);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//set request attributes
		setRequestAttributes(request);
		
		String email = request.getParameter("email");
		
		//try to find user
		//TODO: use simpleuser class
		User user = null;
		try {
			user = DBFacade.getInstance().findUserByEmail(email);
		} catch (BAHPersistenceException e) {
			LOG.log(Level.FINER, "Attempted to create user '"+email+"' but could not find him in db.");
			response.sendError(ErrorCode.USER_NOT_FOUND, "User '"+email+"' not found");
			return;
		}
		
		//user found. convert to json
		String gson_user = gsonInstance.toJson(user);
		
		//return user as json
		LOG.log(Level.FINEST, "Returning user " + user + " as Json: "+gson_user);
		response.getWriter().print(gson_user);
	}
}

package commandlayer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

/**
 * <br/>
 * Singleton-Pattern is used to avoid creating multiple Brokers. <br/>
 * <strong>Usage:</strong><br />
 * 1.Call Broker.getBroker() to get the instance of this class.<br/>
 * 2.Next, call the method getCommandByName(nameString) to get a Command. 
 * If nameString doesn't match any, the MainCmd will be returned.
 */
public class Broker {
	
	private static Broker mBroker;
	private Map<String, Command> mCommands; 
	private ServletContext mContext;
	
	private Broker(){
		mCommands = new HashMap<String, Command>();
		fillMap();
	}
	
	public static Broker getBroker(){
		if(null == mBroker)
			mBroker = new Broker();
		
		return mBroker;
	}
	
	public Command getCommandByName(String name){
		Command cmd = mCommands.get(name);
		
		// Command not found?
		// Then forward to main page
		if(null == cmd)
			cmd = mCommands.get("welcome");
		
		return cmd;
	}
	
	public void setContext(ServletContext context){
		mContext = context;
	}
	public ServletContext getContext(){
		return mContext;
	}
	
	
	private void fillMap(){
		Command cmd;
		
		cmd = new CmdForward("welcome", "BAH Welcome", "/content/welcome.jsp");
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdPopulateDB("populatedb", "Populated DB", "/content/welcome.jsp");
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdLogin("login", "BAH Login", null);
		mCommands.put(cmd.getName(), cmd);
		
		cmd = new CmdRegister("register", "BAH Registration", null);
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdUserToJson("usertojson", "BAH User to Json", null);
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdTestMongoDb("testmongo", "BAH Test MongoDB", null);
		mCommands.put(cmd.getName(), cmd);

		cmd = new CmdManageGameTable("table", "BAH Manage GameTable", null);
		mCommands.put(cmd.getName(), cmd);
	}

}

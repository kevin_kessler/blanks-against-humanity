package commandlayer;

import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistencelayer.BAHPersistenceException;
import persistencelayer.DBFacade;
import persistencelayer.User;
import util.ErrorCode;
import util.Security;

/**
 * This class extends Command and is used to register a new user.
 */
public class CmdRegister extends Command {
	private static final Logger LOG = Logger.getLogger(CmdRegister.class.getName());
	static{
		LOG.setLevel(Level.ALL);
	}
	
	private static final String REGEX_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final Pattern PATTERN_EMAIL = Pattern.compile(REGEX_EMAIL);
	
	private static final String REGEX_USERNAME = "^[_A-Za-z0-9-]{3,20}$";
	private static final Pattern PATTERN_USERNAME = Pattern.compile(REGEX_USERNAME);
	
	//at least 6 characters
	private static final String REGEX_PW = "^.{6,}$";
	private static final Pattern PATTERN_PW = Pattern.compile(REGEX_PW);
	
	public CmdRegister(String name, String title, String content){
		super(name, title, content);
	}
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		//set request attributes
		setRequestAttributes(request);
		
		//get input
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		String passwd_plain = request.getParameter("pwd");
		String passwd_repeat = request.getParameter("pwdrepeat");
		
		//validate input
		SimpleEntry<Integer, String> validationCode = validateInput(email, username, passwd_plain, passwd_repeat);
		if(validationCode.getKey() != ErrorCode.NO_ERROR){
			LOG.log(Level.FINER, "New user tried to register as " + email + " but problem occured: " + validationCode.getKey() + " " + validationCode.getValue());
			sendError(response, validationCode.getKey(), validationCode.getValue());
			return;
		}
		
		//create new user with encrypted pw
		User user = new User(email, username);
		byte[] salt = Security.generateSalt();
        byte[] passwd = Security.generatePassword(passwd_plain, salt);
        user.setSalt(salt);
        user.setPwdHash(passwd);
		
		//try to insert new user
		try {
			DBFacade.getInstance().insertUser(user);
		} catch (BAHPersistenceException e) {
			LOG.log(Level.FINER, "New user tried to register as " + email + " but that email already exists");
			sendError(response, ErrorCode.EMAIL_TAKEN, "Email does already exist");
			return;
		}
		
		//registration complete. login user
		LOG.log(Level.INFO, "Registered new user: " + email +". Logging in now ...");
		response.sendRedirect("index?command=login&email="+email+"&pwd="+passwd_plain);
	}
	
	private SimpleEntry<Integer, String> validateInput(String email, String username, String passwd, String passwd_repeat)
	{		
		boolean emailIsValid = isValidEmail(email);
		if(!emailIsValid){
			return new SimpleEntry<>(ErrorCode.INVALID_EMAIL, "E-Mail is invalid");
		}
		
		boolean usernameIsValid = isValidUsername(username);
		if(!usernameIsValid){
			return new SimpleEntry<>(ErrorCode.INVALID_USERNAME, "Username must be 3-20 characters long and must not contain special characters except '_' and '-'");
		}
		
		boolean passwdIsValid = isValidPassword(passwd);
		if(!passwdIsValid){
			return new SimpleEntry<>(ErrorCode.INVALID_PASSWORD, "Password must be at least 6 characters long");
		}
		
		boolean pwEquals = passwd.equals(passwd_repeat);
		if(!pwEquals){
			return new SimpleEntry<>(ErrorCode.PASSWORD_NOT_EQUAL, "Passwords do not match");
		}
		
		return new SimpleEntry<>(ErrorCode.NO_ERROR, "OK");
	}
	
	private boolean isValidEmail(final String email)
	{
		if(null == email) return false;
		Matcher matcher = PATTERN_EMAIL.matcher(email);
		return matcher.matches();
	}
	
	private boolean isValidUsername(final String username)
	{
		if(null == username) return false;
		Matcher matcher = PATTERN_USERNAME.matcher(username);
		return matcher.matches();
	}
	
	private boolean isValidPassword(final String pw)
	{
		if(null == pw) return false;
		Matcher matcher = PATTERN_PW.matcher(pw);
		return matcher.matches();
	}
}

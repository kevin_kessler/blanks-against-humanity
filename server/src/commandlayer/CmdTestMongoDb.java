package commandlayer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistencelayer.DBFacade;
import persistencelayer.User;
import persistencelayer.mongodb.GameTable;
import persistencelayer.mongodb.MongoDbService;

/**
 * For test purposes.
 * Example call:
 * http://localhost:8080/bah_server/index?command=testmongo
 */
public class CmdTestMongoDb extends Command {

	public CmdTestMongoDb(String name, String title, String content){
		super(name, title, content);
	}

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		MongoDbService mongoService = MongoDbService.getInstance();
		User user;
		try {
			user = DBFacade.getInstance().findUserByEmail("anna.bolika@hs-kl.de");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		GameTable newTable = GameTable.createNewGameTable("MyFirstTable", false, user);
		String _id = mongoService.createGameTableAndGetId(newTable);
		System.out.println("Wrote to mongoDB id: " + _id);
//		//set request attributes
//		setRequestAttributes(request);
//
//		String email = request.getParameter("email");
//		String passwd = request.getParameter("pwd");
//
//		//try to login
//		User user = null;
//		int validationCode = ErrorCode.NO_ERROR;
//		try {
//			user = DBFacade.getInstance().findUserByEmail(email);
//			boolean isAuthorized = DBFacade.getInstance().checkLogin(user, passwd);
//			if(!isAuthorized)
//				validationCode = ErrorCode.INVALID_USER_CREDENTIALS;
//		} catch (BAHPersistenceException e) {
//			validationCode = ErrorCode.INVALID_USER_CREDENTIALS;
//		}
//
//		//login failed?
//		if(validationCode != ErrorCode.NO_ERROR){
//			System.out.println("Login attempt failed: " + email);
//			response.sendError(validationCode, "Email or password wrong");
//			return;
//		}
//
//		//login ok -> create session, return user and session id as json
//		System.out.println("Successfully logged in as " + email);
//		HttpSession session = request.getSession(true);
//		JsonObject json_user = (JsonObject) gsonInstance.toJsonTree(user);
//		json_user.addProperty("JSESSIONID", session.getId());
//
//		response.getWriter().print(json_user);
	}
}

package commandlayer;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistencelayer.BAHPersistenceException;
import persistencelayer.DBFacade;
import persistencelayer.User;
import persistencelayer.mongodb.GameTable;
import persistencelayer.mongodb.MongoDbService;
import util.ErrorCode;

/**
 * Command which is responsible for managing GameTables.
 * 
 * Example calls for actions:
 * 
 * create
 * http://localhost:8080/bah_server/index?command=table&action=create&email=a@a.com&tablename=aNewTable&private=True
 * 
 * gettable
 * http://localhost:8080/bah_server/index?command=table&action=gettable&email=a@a.com&tableId=55c1071a5677541e0645b63c
 * 
 * getall
 * http://localhost:8080/bah_server/index?command=table&action=getall&email=a@a.com
 * 
 * joinrandomly
 * http://localhost:8080/bah_server/index?command=table&action=joinrandomly&email=a@a.com
 * 
 * removeuser
 * http://localhost:8080/bah_server/index?command=table&action=removeuser&email=harry.potter@hs-kl.de&tableId=55c1071a5677541e0645b63c
 * 
 * invite
 * http://localhost:8080/bah_server/index?command=table&action=invite&email_to_invite=harry.potter@hs-kl.de&tableId=55c1071a5677541e0645b63c
 * 
 * accept_invitation
 * http://localhost:8080/bah_server/index?command=table&action=accept_invitation&email_of_invitation=harry.potter@hs-kl.de&tableId=55c1071a5677541e0645b63c
 * 
 * decline_invitation
 * http://localhost:8080/bah_server/index?command=table&action=decline_invitation&email_of_invitation=harry.potter@hs-kl.de&tableId=55c1071a5677541e0645b63c
 * 
 * submit
 * http://localhost:8080/bah_server/index?command=table&action=submit&email=harry.potter@hs-kl.de&tableId=55c1071a5677541e0645b63c&whitecards=123;34
 * 
 * judge
 * http://localhost:8080/bah_server/index?command=table&action=judge&email=harry.potter@hs-kl.de&tableId=55c1071a5677541e0645b63c&whitecards=123;34
 */
public class CmdManageGameTable extends Command {
	
	private static final Logger LOG = Logger.getLogger(CmdManageGameTable.class.getName());
	//TODO: remove
	// Set root logger output to FINEST
	static {
		//get the top Logger:
	    Logger topLogger = java.util.logging.Logger.getLogger("");

	    // Handler for console (reuse it if it already exists)
	    Handler consoleHandler = null;
	    //see if there is already a console handler
	    for (Handler handler : topLogger.getHandlers()) {
	        if (handler instanceof ConsoleHandler) {
	            //found the console handler
	            consoleHandler = handler;
	            break;
	        }
	    }


	    if (consoleHandler == null) {
	        //there was no console handler found, create a new one
	        consoleHandler = new ConsoleHandler();
	        topLogger.addHandler(consoleHandler);
	    }
	    //set the console handler to fine:
	    consoleHandler.setLevel(java.util.logging.Level.FINEST);
	}
	
	private static final List<String> AVAILABLE_ACTIONS = Arrays.asList(new String[] {
			"create", "gettable", "getall", "joinrandomly", "removeuser", "invite", "accept_invitation", "decline_invitation", "submit", "judge"});
	
	

	public CmdManageGameTable(String name, String title, String content){
		super(name, title, content);
	}

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		
		//set request attributes
		setRequestAttributes(request);

		String action = request.getParameter("action");
		
		// action is necessary
		if(!AVAILABLE_ACTIONS.contains(action)){
			String errMsg = "No valid 'action' parameter given: " +
					(action != null ? action : "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		switch(action) {
		case "create":
			createTable(request, response);
			break;
		case "gettable":
			getGameTable(request, response);
			break;
		case "getall":
			getAllTables(request, response);
			break;
		case "joinrandomly":
			joinRandomly(request, response);
			break;
		case "removeuser":
			removeUser(request, response);
			break;
		case "invite":
			inviteUser(request, response);
			break;
		case "accept_invitation":
			acceptInvitation(request, response);
			break;
		case "decline_invitation":
			declineInvitation(request, response);
			break;
		case "submit":
			submitWhitecards(request, response);
			break;
		case "judge":
			judgeWhitecards(request, response);
			break;
		default:
			throw new AssertionError("Should never reach this spot.");
		}

	}

	private void createTable(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String email = request.getParameter("email");
		String gameTableName = request.getParameter("tablename");
		boolean isPrivate = Boolean.parseBoolean(request.getParameter("private"));
		
		// Check given parameters
		if (null == email || email.isEmpty()) {
			String errMsg = "No valid 'email' parameter given: " +
					(email != null ? email: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == gameTableName || gameTableName.isEmpty()) {
			String errMsg = "No valid 'gameTableName' parameter given: " +
					(gameTableName != null ? gameTableName: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Get simple user based on given email
		User user;
		try {
			user = DBFacade.getInstance().findUserByEmail(email);
		} catch (BAHPersistenceException e) {
			LOG.log(Level.WARNING, "User requested to create GameTable but sent email does not exist in database.", e);
			sendError(response, ErrorCode.USER_NOT_FOUND, e.getErrorMessage());
			return;
		}
		
		// Create table
		GameTable newTable = GameTable.createNewGameTable(gameTableName, isPrivate, user);
		String _id = mongoService.createGameTableAndGetId(newTable);
		newTable.setId(_id);
		
		// Success
		LOG.info(String.format("Successfully created new table(id=%s) from user with email %s.", _id, email)); 
		writeGameTableToResponse(newTable, response);
	}
	
	
	private void getGameTable(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String email = request.getParameter("email");
		String tableId = request.getParameter("tableId");
		
		// Check given parameters
		if (null == email || email.isEmpty()) {
			String errMsg = "No valid 'email' parameter given: " +
					(email != null ? email: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == tableId || tableId.isEmpty()) {
			String errMsg = "No valid 'tableId' parameter given: " +
					(tableId != null ? tableId: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		GameTable gameTable = mongoService.getGameTable(email, tableId);
		
		// Success
		LOG.info(String.format("Successfully retrieved table %s for user with email %s.", tableId, email)); 
		writeGameTableToResponse(gameTable, response);
		
	}
	

	private void getAllTables(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String email = request.getParameter("email");
		
		// Check given parameters
		if (null == email || email.isEmpty()) {
			String errMsg = "No valid 'email' parameter given: " +
					(email != null ? email: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Create table
		String jsonTables = mongoService.getAllTablesAsJsonStrFor(email);
		jsonTables = String.format("{\"tables\": %s}", jsonTables);
		
		// Success
		LOG.info(String.format("Successfully retrieved all tables for user with email %s.", email)); 
		LOG.finest("Sending content of all table: "+jsonTables);
		sendSuccess(response, jsonTables);
		
	}
	
	/**
	 * Joins the given user to a random GameTable.
	 * @param request has to contain the parameter "email"
	 * @param response
	 * @throws IOException 
	 */
	private void joinRandomly(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String email = request.getParameter("email");
		
		// Check given parameters
		if (null == email || email.isEmpty()) {
			String errMsg = "No valid 'email' parameter given: " +
					(email != null ? email: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Join a table
		GameTable gameTable;
		try {
			gameTable = mongoService.joinUserToARandomTable(email);
		} catch(BAHPersistenceException e) {
			String errMsg = "Could not add user to a random table.";
			LOG.log(Level.WARNING, errMsg, e);
			sendError(response, ErrorCode.USERJOIN_TO_TABLE_NOT_POSSIBLE, errMsg);
			return;
		}
		
		// Success
		LOG.info(String.format("Successfully joined user(%s) to table(%s).", email, gameTable.getId()));
		writeGameTableToResponse(gameTable, response);
	}

	/**
	 * Removes the given user from GamTable with give id.
	 * @param request has to contain the parameter "email" and "tableId"
	 * @throws IOException 
	 */
	private void removeUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String email = request.getParameter("email");
		String tableId = request.getParameter("tableId");
		
		// Check given parameters
		if (null == email || email.isEmpty()) {
			String errMsg = "No valid 'email' parameter given: " +
					(email != null ? email: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == tableId || tableId.isEmpty()) {
			String errMsg = "No valid 'tableId' parameter given: " +
					(tableId != null ? tableId: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Remove user from table
		mongoService.removeUserFromTable(email, tableId);
		
		// Success
		LOG.info(String.format("Successfully removed user(%s) from table(%s).", email, tableId));
		sendSuccess(response, "");
	}
	

	/**
	 *  Invites a user (given as email) to an GameTable.
	 * @param request has to contain the parameter "email_to_invite" and "tableId"
	 * @param response
	 * @throws IOException 
	 */
	private void inviteUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String emailOrUsernameToInvite = request.getParameter("email_to_invite");
		String tableId = request.getParameter("tableId");
		
		// Check given parameters
		if (null == emailOrUsernameToInvite || emailOrUsernameToInvite.isEmpty()) {
			String errMsg = "No valid 'emailOrUsernameToInvite' parameter given: " +
					(emailOrUsernameToInvite != null ? emailOrUsernameToInvite: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == tableId || tableId.isEmpty()) {
			String errMsg = "No valid 'tableId' parameter given: " +
					(tableId != null ? tableId: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Invite user to table
		GameTable gameTable;
		try {
			gameTable = mongoService.inviteUserToTable(emailOrUsernameToInvite, tableId);
		} catch (BAHPersistenceException e) {
			String errMsg = String.format("User with email %s not found in DB.", emailOrUsernameToInvite);
			LOG.log(Level.WARNING, errMsg, e);
			sendError(response, ErrorCode.USER_NOT_FOUND, errMsg);
			return;
		}
		
		if (null != gameTable) {
			LOG.info(String.format("Successfully invited user(%s) to table(%s).", emailOrUsernameToInvite, tableId));
			sendSuccess(response, "");
		} else {
			String errMsg = String.format("Invite for user (%s) to table(%s) requested but this user plays already or is already invited in this table.", emailOrUsernameToInvite, tableId);
			LOG.warning(errMsg);
			sendError(response, ErrorCode.INVITATION_FAILED, errMsg);
		}
	}
	
	/**
	 *  Accepts an invitation to a GameTable.
	 * @param request has to contain the parameter "email_of_invitation" and "tableId"
	 * @param response
	 * @throws IOException 
	 */
	private void acceptInvitation(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String emailOfInvitation = request.getParameter("email_of_invitation");
		String tableId = request.getParameter("tableId");
		
		// Check given parameters
		if (null == emailOfInvitation || emailOfInvitation.isEmpty()) {
			String errMsg = "No valid 'email_of_invitation' parameter given: " +
					(emailOfInvitation != null ? emailOfInvitation: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == tableId || tableId.isEmpty()) {
			String errMsg = "No valid 'tableId' parameter given: " +
					(tableId != null ? tableId: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Accept invitation in table
		GameTable gameTable;
		try {
			gameTable = mongoService.acceptInvitationInTable(emailOfInvitation, tableId);
		} catch (BAHPersistenceException e) {
			String errMsg = String.format("User with email %s not found in DB.", emailOfInvitation);
			LOG.log(Level.WARNING, errMsg, e);
			sendError(response, ErrorCode.USER_NOT_FOUND, errMsg);
			return;
		}
		
		if (null != gameTable) {
			LOG.info(String.format("Successfully accepted invitation for user(%s) in table(%s).", emailOfInvitation, tableId));
			writeGameTableToResponse(gameTable, response);
		} else {
			String errMsg = String.format("Could not accept invitation for user (%s) to table(%s) because this user plays already or is not invited in this table.", emailOfInvitation, tableId);
			LOG.warning(errMsg);
			sendError(response, ErrorCode.INVITATION_FAILED, errMsg);
		}
	}
	
	/**
	 *  Declines an invitation to a GameTable.
	 * @param request has to contain the parameter "email_of_invitation" and "tableId"
	 * @param response
	 * @throws IOException 
	 */
	private void declineInvitation(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String emailOfInvitation = request.getParameter("email_of_invitation");
		String tableId = request.getParameter("tableId");
		
		// Check given parameters
		if (null == emailOfInvitation || emailOfInvitation.isEmpty()) {
			String errMsg = "No valid 'email_of_invitation' parameter given: " +
					(emailOfInvitation != null ? emailOfInvitation: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == tableId || tableId.isEmpty()) {
			String errMsg = "No valid 'tableId' parameter given: " +
					(tableId != null ? tableId: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Decline invitation in table
		boolean declineSuccessful;
		try {
			declineSuccessful = mongoService.declineInvitationInTable(emailOfInvitation, tableId);
		} catch (BAHPersistenceException e) {
			String errMsg = String.format("User with email %s not found in DB.", emailOfInvitation);
			LOG.log(Level.WARNING, errMsg, e);
			sendError(response, ErrorCode.USER_NOT_FOUND, errMsg);
			return;
		}
		
		if (declineSuccessful) {
			LOG.info(String.format("Successfully declined invitation for user(%s) in table(%s).", emailOfInvitation, tableId));
			sendSuccess(response, "");
		} else {
			String errMsg = String.format("Could not decline invitation for user (%s) to table(%s) because this user plays already or is not invited in this table.", emailOfInvitation, tableId);
			LOG.warning(errMsg);
			sendError(response, ErrorCode.INVITATION_FAILED, errMsg);
		}
	}

	/**
	 *  Submits white cards to a GameTable.
	 * @param request has to contain the parameter "email", "tableId" and "whitecards" (see transformCards() )
	 * @param response
	 * @throws IOException 
	 */
	private void submitWhitecards(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String email = request.getParameter("email");
		String tableId = request.getParameter("tableId");
		String whiteCardIds = request.getParameter("whitecards");
		
		// Check given parameters
		if (null == email || email.isEmpty()) {
			String errMsg = "No valid 'email' parameter given: " +
					(email != null ? email: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == tableId || tableId.isEmpty()) {
			String errMsg = "No valid 'tableId' parameter given: " +
					(tableId != null ? tableId: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		// Transform whiteCardIds
		int[] validCardIds = transformCards(whiteCardIds);
		if (null == validCardIds || 0 == validCardIds.length) {
			String errMsg = "No valid 'whiteCardIds' parameters given: " +
					(whiteCardIds != null ? whiteCardIds: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		
		// Submit white cards in table
		GameTable gameTable = mongoService.submitWhiteCardsToTable(email, tableId, validCardIds);
		
		if (null != gameTable) {
			LOG.info(String.format("Successfully submitted cards %s for user(%s) in table(%s).", whiteCardIds, email, tableId));
			writeGameTableToResponse(gameTable, response);
		} else {
			String errMsg = String.format("Could not submit white cards %s for user (%s) to table(%s).", whiteCardIds, email, tableId);
			LOG.warning(errMsg);
			sendError(response, ErrorCode.INVITATION_FAILED, errMsg);
		}
		
	}
	
	/**
	 * Regular expression for given card ids.
	 * Examples:
	 * 			234;134;45
	 * 			0;2430325;234
	 * 			0
	 * 			130;2340;4525
	 * 
	 * 			Not possible
	 * 			012;324  # A leading 0 is not allowed
	 * 			23423;;234
	 */
	private static final String CARD_IDS_PATTERN = "^([0-9]|[1-9]\\d*)(;([0-9]|[1-9]\\d*))*$";
	/**
	 * This function assumes a string with card ids separated by semicolon.
	 * Order in cardIds represents the order. (This is used to put the white cards into the blanks 
	 * of the black card in the correct order).
	 * Example: "123;34"
	 * 			The first submitted card is the white card with id 123 and the second card has id 34.
	 * @return an array of int representing the ids of the given string
	 */
	private int[] transformCards(String cardIds) {
		if (null == cardIds || ! cardIds.matches(CARD_IDS_PATTERN))
			return null;
		
		String[] idsAsStr = cardIds.split(";");
		int[] ids = new int[idsAsStr.length];
		for(int i = 0; i < ids.length ;++i) {
			ids[i] = Integer.parseInt(idsAsStr[i]);
		}
		return ids;
	}
	
	/**
	 *  The request (from judge) selects the winning cards in table with 'tableId'.
	 * @param request has to contain the parameter "email", "tableId" and one or more "whitecard"'s
	 * @param response
	 * @throws IOException 
	 */
	private void judgeWhitecards(HttpServletRequest request, HttpServletResponse response) throws IOException {
		MongoDbService mongoService = MongoDbService.getInstance();
		
		String email = request.getParameter("email");
		String tableId = request.getParameter("tableId");
		String whiteCardIds = request.getParameter("whitecards");
		
		// Check given parameters
		if (null == email || email.isEmpty()) {
			String errMsg = "No valid 'email' parameter given: " +
					(email != null ? email: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		if (null == tableId || tableId.isEmpty()) {
			String errMsg = "No valid 'tableId' parameter given: " +
					(tableId != null ? tableId: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		// Transform whiteCardIds
		int[] validCardIds = transformCards(whiteCardIds);
		if (null == validCardIds || 0 == validCardIds.length) {
			String errMsg = "No valid 'whiteCardIds' parameters given: " +
					(whiteCardIds != null ? whiteCardIds: "null");
			LOG.warning(errMsg);
			sendError(response, ErrorCode.WRONG_REQUEST_PARAMETER, errMsg);
			return;
		}
		
		// Select white cards in table
		GameTable gameTable;
		try {
			gameTable = mongoService.judgeWhiteCardsInTable(email, tableId, validCardIds);
		} catch (BAHPersistenceException e) {
			String errMsg = "One of the winning users does not exist in DB.";
			LOG.log(Level.WARNING, errMsg, e);
			sendError(response, ErrorCode.USER_NOT_FOUND, errMsg);
			return;
		}
		
		if (null != gameTable) {
			LOG.info(String.format("Successfully judged cards %s in table(%s).", whiteCardIds, tableId));
			writeGameTableToResponse(gameTable, response);
		} else {
			String errMsg = String.format("Judge (%s) can not select winner cards %s for table(%s).", email, whiteCardIds, tableId);
			LOG.warning(errMsg);
			sendError(response, ErrorCode.INVITATION_FAILED, errMsg);
			return;
		}
	}

	private void writeGameTableToResponse(GameTable gameTable, HttpServletResponse response) throws IOException {
		String jsonTable = gsonInstance.toJson(gameTable);
		LOG.finest("Sending content of table: "+jsonTable);
		sendSuccess(response, jsonTable);
	}

}

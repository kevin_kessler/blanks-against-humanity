package persistencelayer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.google.gson.annotations.Expose;

import persistencelayer.mongodb.IngameWhiteCard;
import persistencelayer.mongodb.Round;
import persistencelayer.mongodb.SimpleUser;

@Entity 
public class User{
	private static final Logger LOG = Logger.getLogger(User.class.getName());
	//LOG.log(Level.INFO, "");
	
	public static final int MAX_NUM_OF_HAND_CARDS = 10;
	
	@Id @Expose
	private String email;
	
	@Expose
	private String username;
	
	@Expose
	private int credit;
	
	@Column(name="pwd")
	private byte[] pwdHash = new byte[0];
	private byte[] salt = new byte[0];
	
	@ManyToMany(cascade={CascadeType.REMOVE})
    @JoinTable(
    	name="USER_REPOSITORY_CARDS",
    	joinColumns={@JoinColumn(name="USER_ID")},
    	inverseJoinColumns={@JoinColumn(name="WHITECARD_ID")})
	private List<WhiteCard> cardRepository;
	
	public User(){}
	
	public User(String email, String username)
	{
		setEmail(email);
		setUsername(username);
		setCredit(0);
		
		//give the user a starter deck of white cards
		try {
			this.cardRepository = DBFacade.getInstance().getStarterWhiteCards();
		} catch (BAHPersistenceException e) {
			LOG.log(Level.SEVERE, "Tried to load starter white cards with no success: "+e.toString());
			e.printStackTrace();
		}
	}
	
	public User(String email, String username, int credit)
	{
		this(email, username);
		setCredit(credit);
	}
	
	public List<WhiteCard> getCardRepository(){
		return cardRepository;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public double getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}
	
	public byte[] getPwdHash()
	{
	   return pwdHash;
    }

    public void setPwdHash(byte[] pwdHash)
    {
    	this.pwdHash = pwdHash;
    }

    public byte[] getSalt()
    {
       return salt;
    }

    public void setSalt(byte[] salt)
    {
       this.salt = salt;
    }

	@Override
	public String toString() {
		return email + " | " + username + " | " + credit; 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
	
	/** 
	 * determine hand of cards randomly among users card repository
	 */
	public List<IngameWhiteCard> generateHand(){
		if(cardRepository.size() < MAX_NUM_OF_HAND_CARDS){
			String error = "User does not have a sufficient amount of white cards in his repository";
			LOG.log(Level.SEVERE, error);
			throw new RuntimeException(error);
		}
		List<WhiteCard> copiedCardReferences = new ArrayList<>(cardRepository);
		List<IngameWhiteCard> cards = new ArrayList<>();

		while(cards.size() < MAX_NUM_OF_HAND_CARDS){
			int randomIndex = (int)(Math.random()*(copiedCardReferences.size()));
			IngameWhiteCard card = copiedCardReferences.get(randomIndex).toIngameCard();
			copiedCardReferences.remove(randomIndex);
			cards.add(card);
		}
		return cards;
	}
	
	/**
	 * Draws if necessary new cards from cardRepository and resets submitted-flags.
	 * If a card with submitted-flag is encountered then a new card will be created.
	 * The reason is that we want to keep the submitted cards from previous round.
	 * @param cardsForNextRound
	 */
	public void fillAndResetCards(List<IngameWhiteCard> cardsForNextRound) {
		// Reset flags
		for(int i=0; i < cardsForNextRound.size() ;++i) {
			if (cardsForNextRound.get(i).isSubmitted()) {
				// "Reset" by creating a new card
				IngameWhiteCard card = cardsForNextRound.get(i);
				cardsForNextRound.set(i, new IngameWhiteCard(card.id, card.getText()));
			}
		}
		
		// Redraw
		while(cardsForNextRound.size() < MAX_NUM_OF_HAND_CARDS){
			int randomIndex = (int)(Math.random()*(cardRepository.size()));
			IngameWhiteCard possibleCard = cardRepository.get(randomIndex).toIngameCard();
			if (!cardsForNextRound.contains(possibleCard)) {
				cardsForNextRound.add(possibleCard);
			}
		}
	}

	public SimpleUser getSimpleUser() {
		return new SimpleUser(email, username);
	}

	/**
	 * Rewards all winner of one round. The judge will also be rewarded. This is used as an incentive for the user
	 */
	public static void rewardWinnersAndJudge(List<String> emailsOfSWinners, String judgeEmail) throws BAHPersistenceException {

		List<String> allEmails = new ArrayList<>(emailsOfSWinners);
		allEmails.add(judgeEmail);

		List<User> winners = DBFacade.getInstance().findUsersByEmails(allEmails);

		for (User u : winners) {
			u.reward();
			DBFacade.getInstance().updateUser(u);
		}

	}

	private void reward() {
		credit+=Round.WINNING_REWARD;
	}

}

package persistencelayer.mongodb;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Filters.not;
import static com.mongodb.client.model.Filters.lt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import persistencelayer.BAHPersistenceException;
import persistencelayer.DBFacade;

public class MongoDbService {
	
	private static final Logger LOG = Logger.getLogger(MongoDbService.class.getName());
	
	static{
		LOG.setLevel(Level.ALL);
	}
	
	private static class InstanceHolder {
		private static MongoDbService instance = new MongoDbService();
	}

	public static MongoDbService getInstance() {
		return InstanceHolder.instance;
	}
	
	private MongoClient mongo;
	private MongoDatabase db;
	private Gson gson;
	
	private MongoDbService() {
		mongo = new MongoClient( "localhost" , 27017 );
		db = mongo.getDatabase("bah");
		gson = new Gson();
	}
	
	private MongoCollection<Document> getGameTablesCollection() {
		return db.getCollection("game_tables");	
	}
	private MongoCollection<Document> getUserTablesCollection() {
		return db.getCollection("user_tables");	
	}
	
	/**
	 * Creates the given table.
	 * @param table
	 * @return the created ObjectId
	 */
	public String createGameTableAndGetId(GameTable table) {
		assert(null != table && table.isValid());
		
        //TODO: comment pretty formatter out
        // Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String jsonOutput = gson.toJson(table);
        
        Document tableDoc = Document.parse(jsonOutput);
        String _id = new ObjectId().toString();
        tableDoc.put("_id", _id);

        getGameTablesCollection().insertOne(tableDoc);
        table.setId(_id);
        addTableIdToUserTableCollection(table.getId(), table.getCurRound().getJudge().getEmail());
        
        return _id;
	}
	
	public void updateTable(GameTable table) {
		assert(table.isValidAndHasId());
		LOG.fine(String.format("Updating table with id %s", table.getId()));
		String jsonOutput = gson.toJson(table);
		Document tableDoc = Document.parse(jsonOutput);
		tableDoc.remove("_id");
//		Document idDoc = new Document("_id", table.getId());
		UpdateResult updateOne = getGameTablesCollection().updateOne(eq("_id", table.getId()), new Document("$set", tableDoc));
		if (updateOne.getModifiedCount() != 1) {
			LOG.warning(String.format("Did not update table %s. Probably the table does not exist.", table.getId()));
		}
	}
	
	public void deleteTable(GameTable table) {
		assert(table.getId() != null && !table.getId().isEmpty());
		LOG.fine(String.format("Deleting table %s", table.getId()));
		DeleteResult deleteResult = getGameTablesCollection().deleteOne(eq("_id", table.getId()));
		if (deleteResult.getDeletedCount() != 1) {
			LOG.warning(String.format("Did not delete table %s. Probably the table does not exist.", table.getId()));
		}
		removeTableIdForAllUsersFromUserTableCollection(table.getId());
	}


	/**
	 * Saves a list of participating user email's in table. This makes it easier to find all tables a user participates in.
	 */
	private void addTableIdToUserTableCollection(String tableId, String emailOfUser) {
		assert(tableId != null && !tableId.isEmpty());
		Document doc = createUserTableDocument(tableId, emailOfUser);
		getUserTablesCollection().insertOne(doc);
	}
	private void removeTableIdForOneUserFromUserTableCollection(String tableId, String emailOfUser) {
		assert(tableId != null && !tableId.isEmpty());
		Document doc = createUserTableDocument(tableId, emailOfUser);		
		getUserTablesCollection().deleteOne(doc);
	}
	private void removeTableIdForAllUsersFromUserTableCollection(String tableId) {
		assert(tableId != null && !tableId.isEmpty());
		DeleteResult deleteResult = getUserTablesCollection().deleteMany(eq("_id.tableId", tableId));
		LOG.fine(String.format("Deleted %d tableId-entries for id %s from db.user_tables.", deleteResult.getDeletedCount(), tableId));
	}
	/**
	 * Creates documents for the mongoDB collection from getUserTablesCollection()
	 */
	private Document createUserTableDocument(String tableId, String emailOfUser) {		
		Document _id = new Document();
		_id.put("email", emailOfUser);
		if(null != tableId)
			_id.put("tableId", tableId);
		Document doc = new Document("_id", _id);
		
		return doc;
	}

	public String getAllTablesAsJsonStrFor(String email) {
		return getAllTablesAsJsonStrFor(getAllTableIdsOfUser(email));
	}
	
	/**
	 * Retrieves all tableIds the user with 'email is participating in.
	 * Also tableIds in which the user is only invited will be returned.
	 */
	private List<String> getAllTableIdsOfUser(String email) {
		// Fetch all table ids for given email from mongoDB
		MongoCursor<Document> userTableIdCursor = getUserTablesCollection().find(eq("_id.email", email)).iterator();

		// Extract table ids
		final List<String> tableIds = new ArrayList<>();
		try {
		    while (userTableIdCursor.hasNext()) {
		    	Document doc = userTableIdCursor.next();
		    	if(doc.containsKey("_id")) {
					Document _id = (Document) doc.get("_id");
					if(_id.containsKey("tableId")) {
						tableIds.add(_id.getString("tableId"));
					} else {
						LOG.warning("Loaded document from db.user_tables has no '_id.tableId': " + doc.toJson());
					}
				} else {
					LOG.warning("Loaded document from db.user_tables has no '_id': " + doc.toJson());
				}
		    }
		} finally {
			userTableIdCursor.close();
		}
		
		return tableIds;
	}


	private String getAllTablesAsJsonStrFor(List<String> tableIds) {
		// Fetch all tables for given ids from mongoDB
		// Example: ...find({"_id": {"$in": ["a", "b"]}})
		MongoCursor<Document> allTableDocsCursor = getGameTablesCollection().find(in("_id", tableIds)).iterator();
		
		// Extract table ids
		final BasicDBList allTables = new BasicDBList();
		try {
		    while (allTableDocsCursor.hasNext()) {
		        allTables.add(allTableDocsCursor.next());
		    }
		} finally {
			allTableDocsCursor.close();
		}
		
		return allTables.toString();
	}

	public GameTable joinUserToARandomTable(String emailOfUserToAdd) throws BAHPersistenceException {
		List<String> tableIdsToExclude = getAllTableIdsOfUser(emailOfUserToAdd);
		
		// Find a possible table
		Bson query = and(not(in("_id", tableIdsToExclude)), eq("isPrivate", false));
		MongoCursor<Document> possibleTablesCursor = getGameTablesCollection().find(query)
				.projection(new Document("_id", true).append("curRound.playersWithHand", true)).iterator();
		
		// Extract table ids
		// Divide into lists, One list with GameTables in which the minimum amount of players is not reached.
		List<String> possibleTableIdsWithoutMinPlayers =new ArrayList<>();
		List<String> possibleTableIdsWithMinPlayers = new ArrayList<>();
		try {
		    while (possibleTablesCursor.hasNext()) {
		    	Document doc = possibleTablesCursor.next();
		    	if (roundHasNotMinPlayers( (Document) (doc.get("curRound")) ))
		    		possibleTableIdsWithoutMinPlayers.add(doc.getString("_id"));
		    	else
		    		possibleTableIdsWithMinPlayers.add(doc.getString("_id"));
		    }
		} finally {
			possibleTablesCursor.close();
		}
		
		// Prefer tables without the minimum amount of players
		List<String> possibleTableIds;
		if(0 != possibleTableIdsWithoutMinPlayers.size()) {
			possibleTableIds = possibleTableIdsWithoutMinPlayers;
		} else if (0 != possibleTableIdsWithMinPlayers.size()) {
			possibleTableIds = possibleTableIdsWithMinPlayers;
		} else {
			// No GameTables available. Create one.
			GameTable newTable = GameTable.createNewRandomGameTableFor(emailOfUserToAdd);
			createGameTableAndGetId(newTable);
			return newTable;
		}
		
		// We have all possible table ids. Pick randomly a table id
		int tableIdIndex = new Random().nextInt(possibleTableIds.size());
		
		return addPlayerToTable(emailOfUserToAdd, possibleTableIds.get(tableIdIndex));
	}

	/**
	 * Assumes the document
	 * {
        "playersWithHand" : {
            "aaa;;;a@a;POINT;com" : [...],
            "bbb;;;b@b;POINT;com" : [...],
            "ccc;;;c@c;POINT;com" : [...],
        }
       }
     * 
     * We want to count the amount of players with hand.
     * @return true if amount of players is smaller than GameTable.MIN_PLAYERS_TO_START 
	 */
	private boolean roundHasNotMinPlayers(Document doc) {
		assert(null != doc);

		Document playersWithCardsJson = (Document) doc.get("playersWithHand");

		assert(null != playersWithCardsJson);

		int amountOfPlayers = playersWithCardsJson.keySet().size();
		
		return amountOfPlayers < GameTable.MIN_PLAYERS_TO_START;
	}

	private GameTable addPlayerToTable(String emailOfUserToAdd, String tableId) {
		GameTable table = getGameTable(tableId);
		
		table.getCurRound().addPlayer(emailOfUserToAdd);
		
		updateTable(table);
		addTableIdToUserTableCollection(table.getId(), emailOfUserToAdd);
		
		return table;
	}

	public GameTable getGameTable(String email, String tableId) {
		List<String> allTableIdsOfUser = getAllTableIdsOfUser(email);
		
		if ( ! allTableIdsOfUser.contains(tableId)) {
			LOG.warning(String.format("Usere %s tried to get table %d but table is not registered for this user.", email, tableId));
			return null;
		}
			
		// Fetch table for given id from mongoDB
		return getGameTable(tableId);
	}
	
	private GameTable getGameTable(String tableId) {
		// Fetch table for given id from mongoDB
		Document tableDoc = getGameTablesCollection().find(eq("_id", tableId)).first();
		
		if(null == tableDoc)
			return null;
		
		return GameTable.createFrom(toGsonObject(tableDoc));
	}
	
	private JsonObject toGsonObject(Document mongoDoc) {
		JsonElement element = gson.fromJson(mongoDoc.toJson(), JsonElement.class);
		return element.getAsJsonObject();
	}

	/**
	 * Removes user with email from GameTable with tableId.
	 * If user has no table with tableId the removing is treated as success.
	 * If user is last player in table then the table will be deleted.
	 */
	public void removeUserFromTable(String email, String tableId) {
		// Check if the tableId is registered for the user
		List<String> tableIdsOfUser = getAllTableIdsOfUser(email);
		if (!tableIdsOfUser.contains(tableId)) {
			LOG.warning(String.format("User(%s) requested to delete table(%s) but this table is not in db.user_tables for requesting user.", email, tableId));
			// Nothing to do. Treat as success.
			return;
		}
		
		// tableId exists in db.user_tables for user with given email. Remove user from table
		GameTable table = getGameTable(tableId);
		
		if (null == table) {
			// tableId exists but no table. Delete tableId from user in db.user_tables
			removeTableIdForOneUserFromUserTableCollection(tableId, email);
			return;
		}
		
		removeUserFromTable(email, table);
	}
	private void removeUserFromTable(String email, GameTable table) {
		if (table.isLastPlayer(email)) {
			deleteTable(table);
			return;
		}
		
		table.removePlayer(email);

		updateTable(table);
		removeTableIdForOneUserFromUserTableCollection(table.getId(), email);
	}

	/**
	 * Invites the user with emailToInvite to GameTable with tableId.
	 * Saves the SimpleUser to pendingInvites of GameTable.
	 * @return null if operation fails otherwise the GameTable
	 * 
	 * @throws BAHPersistenceException user with given email does not exist.
	 */
	public GameTable inviteUserToTable(String emailOrUsernameToInvite, String tableId) throws BAHPersistenceException {

		SimpleUser su;
		if (emailOrUsernameToInvite.contains("@"))
			su = DBFacade.getInstance().findUserByEmail(emailOrUsernameToInvite).getSimpleUser();
		else
			su = DBFacade.getInstance().findUserByUsername(emailOrUsernameToInvite).getSimpleUser();
			

		GameTable gameTable = getGameTable(tableId);
		
		if (null == gameTable) {
			LOG.warning(String.format("Table with id %s does not exist", tableId));
			return null;
		}
		
		boolean successfulInvitation = gameTable.invite(su);
		if ( ! successfulInvitation) {
			return null;
		}
		
		updateTable(gameTable);
		addTableIdToUserTableCollection(tableId, su.getEmail());
		return gameTable;
	}

	public GameTable acceptInvitationInTable(String emailOfInvitation, String tableId) throws BAHPersistenceException {

		SimpleUser su = DBFacade.getInstance().findUserByEmail(emailOfInvitation).getSimpleUser();

		GameTable gameTable = getGameTable(tableId);
		
		if (null == gameTable) {
			LOG.warning(String.format("Table with id %s does not exist", tableId));
			return null;
		}
		
		boolean acceptSuccessful = gameTable.acceptInvitation(su);
		if ( ! acceptSuccessful) {
			return null;
		}
		
		updateTable(gameTable);
		// No update in db.user_tables necessary since there is already an entry
		
		return gameTable;
	}

	public boolean declineInvitationInTable(String emailOfInvitation, String tableId) throws BAHPersistenceException {
		
		SimpleUser su = DBFacade.getInstance().findUserByEmail(emailOfInvitation).getSimpleUser();
		
		GameTable gameTable = getGameTable(tableId);
		
		if (null == gameTable) {
			LOG.warning(String.format("Table with id %s does not exist", tableId));
			return false;
		}
		
		boolean declineSuccessful = gameTable.declineInvitation(su);
		if ( ! declineSuccessful) {
			return false;
		}
		
		updateTable(gameTable);
		removeTableIdForOneUserFromUserTableCollection(tableId, emailOfInvitation);
		
		return true;
	}

	public GameTable submitWhiteCardsToTable(String email, String tableId, int[] validCardIds) {
		GameTable gameTable = getGameTable(tableId);
		
		if (null == gameTable) {
			LOG.warning(String.format("Table with id %s does not exist", tableId));
			return null;
		}
		boolean submitSuccessful = gameTable.submitWhiteCards(email, validCardIds);
		if ( ! submitSuccessful) {
			return null;
		}
		
		updateTable(gameTable);
		return gameTable;
	}

	public GameTable judgeWhiteCardsInTable(String email, String tableId, int[] validCardIds) throws BAHPersistenceException {
		GameTable gameTable = getGameTable(tableId);
		
		if (null == gameTable) {
			LOG.warning(String.format("Table with id %s does not exist", tableId));
			return null;
		}
		
		boolean judgedSuccessful = gameTable.judgeWinningCards(email, validCardIds);
		if ( ! judgedSuccessful) {
			return null;
		}
		
		updateTable(gameTable);
		// If an exception would occur here we actually would have to reset the given rewards for the winners but we ignore it and bestow the users.

		return gameTable;
	}
	
	/**
	 * This function searches for idle players and removes them from the corresponding tables.
	 */
	public void deleteIdlePlayersFromGameTables() {
		
		// First fetch all tables where the last action time is lower than lowestAllowedTime
		long lowestAllowedTime = GameTable.getLowestAllowedIdle();
		
		MongoCursor<Document> allTablesWithIdlePlayersCursor = getGameTablesCollection().find(lt("timeToIndicateIdlePlayers", lowestAllowedTime)).iterator();
		
		try {
		    while (allTablesWithIdlePlayersCursor.hasNext()) {
		    	// Create GameTable object
		    	Document doc = allTablesWithIdlePlayersCursor.next();
		    	GameTable table = GameTable.createFrom(toGsonObject(doc));
		    	
		    	// Get and delete all idle players
		    	List<SimpleUser> idlePlayers = table.getIdlePlayers();
		    	for(SimpleUser idlePlayerToDelete : idlePlayers) {
		    		removeUserFromTable(idlePlayerToDelete.getEmail(), table);
		    	}
		    }
		} finally {
			allTablesWithIdlePlayersCursor.close();
		}
	}

}

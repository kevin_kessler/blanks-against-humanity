package persistencelayer.mongodb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.Entity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import persistencelayer.BAHPersistenceException;
import persistencelayer.BlackCard;
import persistencelayer.DBFacade;
import persistencelayer.User;

@Entity
public class Round {

	private static final Logger LOG = Logger.getLogger(GameTable.class.getName());
	public static final int WINNING_REWARD = 10;
	
	static{
		LOG.setLevel(Level.ALL);
	}
	
	private SimpleUser judge;
	
	private BlackCard blackCard;
	
	private Map<SimpleUser, List<IngameWhiteCard>> playersWithHand;
	
	/**
	 * The ids of the winning cards. Order in array represents the submit order.
	 * Example: [2,6]
	 * The first submitted card has the id 2. The second card has id 6.
	 * The winning cards are (in this order) card 2 and card 6.
	 */
	private int[] winnerCards;
	
	public Round(){}
	public Round(SimpleUser judge, BlackCard blackCard){
		this.judge = judge;
		this.blackCard = blackCard;
		this.winnerCards = null;
		this.playersWithHand = new HashMap<>();
	}
	public Round(Map<SimpleUser, List<IngameWhiteCard>> _playerWithCards, SimpleUser _judge, BlackCard _blackCard, int[] _winnerCards) {
		judge = _judge;
		blackCard = _blackCard;
		winnerCards = _winnerCards;
		playersWithHand = _playerWithCards;
	}
	public int[] getWinnerCards() {
		return winnerCards;
	}
	public List<SimpleUser> getPlayers() {
		return new ArrayList<SimpleUser>(playersWithHand.keySet());
	}
	public Map<SimpleUser, List<IngameWhiteCard>> getPlayersWithHand(){
		return playersWithHand;
	}
	public SimpleUser getJudge() {
		return judge;
	}
	public BlackCard getBlackCard() {
		return blackCard;
	}
	
	public void addPlayer(SimpleUser player){
		addPlayer(player.getEmail());
	}
	public void addPlayer(String email){
		User user;
		try {
			user = DBFacade.getInstance().findUserByEmail(email);
		} catch (BAHPersistenceException e) {
			return;
		}
		addPlayer(user);
	}
	public void addPlayer(User user) {
		SimpleUser sUser = user.getSimpleUser();
		if (playersWithHand.containsKey(sUser)) {
			LOG.warning(String.format("Tried to insert user (%s %s) but user is already in this round.", sUser.getUsername(), sUser.getEmail()) );
			return;
		}
		List<IngameWhiteCard> hand = user.generateHand();
		playersWithHand.put(sUser, hand);
	}

	/**
	 * Removes player with email. User shall not be judge in this round.
	 */
	protected void removePlayer(String email) {
		assert(!judge.getEmail().equals(email));
		
		List<SimpleUser> players = getPlayers();
		for(int i=0; i < players.size() ;++i) {
			if (players.get(i).getEmail().equals(email)) {
				playersWithHand.remove(players.get(i));
				break; // There should not be more than one player with this email
			}
		}
	}
	
	public static Round createFrom(JsonObject jsonObject) {
		SimpleUser _judge = SimpleUser.createFrom(jsonObject.get("judge").getAsJsonObject());
	
	    BlackCard _blackCard = BlackCard.createFrom(jsonObject.get("blackCard").getAsJsonObject());
	
	    // Map<SimpleUser, List<IngameWhiteCard>>
	    Map<SimpleUser, List<IngameWhiteCard>> _playerWithCards= new HashMap<>();
	    JsonObject playerWithCardsAsJson = jsonObject.get("playersWithHand").getAsJsonObject();
	    for(Map.Entry<String,JsonElement> playerWhiteCardsPair : playerWithCardsAsJson.entrySet()) {
	        SimpleUser user = SimpleUser.createFrom(playerWhiteCardsPair.getKey());
	        List<IngameWhiteCard> ingameCardsOfUser = new ArrayList<>();
	        JsonArray cardsArray = playerWhiteCardsPair.getValue().getAsJsonArray();
	        for(int i=0; i < cardsArray.size() ;i++) {
	            JsonElement cardElement = cardsArray.get(i);
	            IngameWhiteCard card = IngameWhiteCard.createFrom(cardElement.getAsJsonObject());
	            ingameCardsOfUser.add(card);
	        }
	        _playerWithCards.put(user, ingameCardsOfUser);
	    }
	    
	    
	    int[] _winnerCards = null;
	    if (jsonObject.has("winnerCards")) {
		    JsonArray cardsArray = jsonObject.get("winnerCards").getAsJsonArray();
		    _winnerCards = new int[cardsArray.size()];
		    for(int i=0; i < cardsArray.size() ;i++) {
		        _winnerCards[i] = cardsArray.get(i).getAsInt();
		    }
	    }
	
	    return new Round(_playerWithCards, _judge, _blackCard, _winnerCards);
	}
	
	public boolean isValid() {
		return judge != null && judge.isValid() && blackCard.isValid() && 0 < playersWithHand.size();
	}
	
	public boolean submitCards(SimpleUser su, int[] validCardIds) {
		if (null == su) {
			LOG.warning(String.format("Could not submit cards because user is not playing on this table."));
			return false;
		}
		
		if (validCardIds.length != blackCard.getNumOfBlanks()) {
			LOG.warning(String.format("Could not submit cards for user(%s) because the amount of given cards %d is not equals to the blanks %d of the black card with id %d).", su.getEmail(), validCardIds.length, blackCard.getNumOfBlanks(), blackCard.getId()));
			return false;
		}
		
		if (hasAtLeastOneSubmittedCard(su)) {
			LOG.warning(String.format("Could not submit cards for user(%s) because user has already submitted cards.", su.getEmail()));
			return false;
		}
		
		if (null != winnerCards) {
			LOG.warning(String.format("Could not submit cards because there are already winner cards: %s.", winnerCards.toString()));
			return false;
		}
		
		
		if ( ! markCardsAsSubmitted(su, validCardIds)) {
			LOG.warning(String.format("Could not submit cards for user(%s) because user does not have submitted cards %s on his hand.", su.getEmail(), Arrays.toString(validCardIds)));
			return false;
		}
		
		return true;
	}
	
	private boolean markCardsAsSubmitted(SimpleUser su, int[] validCardIds) {
		
		for(int i=0; i < validCardIds.length ;++i) {
			boolean cardExistsInUsersHand = false;
			for(IngameWhiteCard card : playersWithHand.get(su))
				if (card.getId() == validCardIds[i]) {
					card.setSubmitted(i+1);  // Order in validCardIds represents the order. (This is used to put the white cards into the blanks of the black card in the correct order)
					cardExistsInUsersHand = true;
					break;
				}
			if(!cardExistsInUsersHand) {
				resetSubmittedInCards(playersWithHand.get(su));
				return false;
			}
		}
		return true;
	}
	private void resetSubmittedInCards(List<IngameWhiteCard> cardsToReset) {
		for (IngameWhiteCard c : cardsToReset)
			c.resetSubmit();		
	}
	private boolean hasAtLeastOneSubmittedCard(SimpleUser su) {
		for(IngameWhiteCard card : playersWithHand.get(su))
			if (card.isSubmitted())
				return true;
		return false;
	}
	private int getNumOfSubmittedCards(SimpleUser su) {
		int counter = 0;
		for(IngameWhiteCard card : playersWithHand.get(su))
			if (card.isSubmitted())
				counter++;
		return counter;
	}
	protected List<IngameWhiteCard> getSubmittedCards(SimpleUser su) {
		List<IngameWhiteCard> submittedCards = new ArrayList<>();
		for(IngameWhiteCard card : playersWithHand.get(su))
			if (card.isSubmitted())
				submittedCards.add(card);
		return submittedCards;
	}
	public SimpleUser getPlayerByEmail(String email) {
		for(SimpleUser su : getPlayers())
			if (su.getEmail().equals(email))
				return su;
		return null;
	}

	public boolean judgeWinningCards(SimpleUser judge, int[] validCardIds) throws BAHPersistenceException {
		if (! getJudge().equals(judge)) {
			LOG.warning(String.format("Could not judge cards because user is not the current judge of this table."));
			return false;
		}

		if ( ! allPlayersExceptJudgeSubmittedCards()) {
			LOG.warning("Could not judge cards because not every user has already submitted cards.");
			return false;
		}

		if (null != winnerCards) {
			LOG.warning(String.format("Could not judge cards because there are already winner cards: %s.", winnerCards.toString()));
			return false;
		}

		List<String> winnerEmails = setWinningCardsAndGetEmailsOfWinners(validCardIds);
		if (0 == winnerEmails.size()) {
			LOG.warning(String.format("Judged winning cards(%s) do not correspond with submitted cards of any user.", Arrays.toString(validCardIds)));
			return false;
		}

		User.rewardWinnersAndJudge(winnerEmails, judge.getEmail());
		
		return true;
	}
	private List<String> setWinningCardsAndGetEmailsOfWinners(int[] winningCards) {
		List<String> winnerEmails = new ArrayList<>();
		
		// For every player ...
		for (SimpleUser player : getPlayers()) {
			// ... except the judge ...
			if (judge.equals(player))
				continue;
			
			boolean hasWinningHand = hasWinningHand(player, winningCards);
			
			// ... and if yes remember the winner.
			if (hasWinningHand) {
				winnerEmails.add(player.getEmail());
				winnerCards = winningCards;
			}	
		}
		
		return winnerEmails;
	}
	
	/**
	 * Determines if the user has a winning hand. Therefore the user needs to have all winning cards
	 * submitted in correct order.
	 * @param player
	 * @param winningCards the ids of the winning cards. Order in array represents the submit order
	 * 			(Index 0 is first submitted card and so on)
	 * @return true if the user has a winning hand
	 */
	private boolean hasWinningHand(SimpleUser player, int[] winningCards) {
		List<IngameWhiteCard> playersHand = getPlayersWithHand().get(player);
		for (int i=0; i < winningCards.length ;++i) {
			boolean winCardIsWithCorrectOrderInPlayerssHand = false;
			for(IngameWhiteCard c : playersHand) {
				if (winningCards[i] == c.getId()) {
					// c is a winning card but has it also the correct submit order?
					if (c.getSubmittedOrder() == i+1) {
						winCardIsWithCorrectOrderInPlayerssHand = true;
					}
				}
			}
			if ( ! winCardIsWithCorrectOrderInPlayerssHand)
				return false;
		}
		return true;
	}
	
	protected boolean allPlayersExceptJudgeSubmittedCards() {
		for(SimpleUser player : getPlayers()) {
			if (!hasSubmittedCards(player))
				return false;
		}
		return true;
	}
	public boolean hasSubmittedCards(SimpleUser player) {
		return judge.equals(player) || blackCard.getNumOfBlanks() == getNumOfSubmittedCards(player);
	}
}

package persistencelayer.mongodb;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import persistencelayer.BAHPersistenceException;
import persistencelayer.BlackCard;
import persistencelayer.DBFacade;
import persistencelayer.User;

	
public class GameTable {
	private static final Logger LOG = Logger.getLogger(GameTable.class.getName());
	
	/**
	 * This hashmap helps avoiding the recurring of the same black card.
	 * Since this is transient it will not be saved. The history is only as long active as long
	 * the server will not be restarted.
	 * 
	 * (tableId) -> [idsOfPreviousBlackcards]
	 */
	private Map<String, List<Integer>> historyOfBlackCards = new HashMap<>();
	
	static{
		LOG.setLevel(Level.ALL);
	}
	
	/**
	 * The minimum amount of players necessary to start a game.
	 */
	public static final int MIN_PLAYERS_TO_START = 3;
	
	/**
	 * Determines how long user can be idle until they will be removed from a GameTable.
	 */
	public static final long MAX_IDLE_DAYS_FOR_PLAYERS = 1;
	public static final long MILLISECONDS_PER_DAY = 1000*60*60*24;  // A day has 1000*60*60*24 milliceconds

	//TODO: MAX_IDLE_TIME_FOR_PLAYERS
	
	private String _id;
	private String name;
	private boolean isPrivate;
	private int roundCount;
	/**
	 * This time helps to determine if a player is idle for a long time.
	 * The timer will be set if a new round starts or if everybody submitted.
	 * If a round starts all players except the judge have to submit white cards.
	 * If all cards have been submitted the judge has to select the winner cards.
	 * If a player does not react for a long time then he will be removed from the table.
	 * The threshold will be determined by MAX_IDLE_DAYS_FOR_PLAYERS.
	 * 
	 * See also getIdlePlayers()
	 * 
	 * The time comes from java.util.Date.now()
	 */
	private long timeToIndicateIdlePlayers;
	
	private Round prevRound;
	
	private Round curRound;
	
	private List<SimpleUser> pendingUserInvites;
	
	public GameTable(String tableId, String name, boolean isPrivate, Round curRound)
	{
		this._id = tableId;
		this.name = name;
		this.isPrivate = isPrivate;
        this.curRound = curRound;
		this.roundCount = 0;
		this.pendingUserInvites = new ArrayList<>();
	}

	/**
	 * Creates a new GameTable in case only the email of the user is available.
	 * Chooses a name based on the username.
	 * @param email the email of the user
	 * @return a new table which is not saved yet in MongoDB.
	 * @throws BAHPersistenceException if user with email will not be found.
	 */
	public static GameTable createNewRandomGameTableFor(String email) throws BAHPersistenceException {
		User user = DBFacade.getInstance().findUserByEmail(email);
		String gameName = createRandomNameBasedOn(user);
		boolean isPrivate = false;
		return createNewGameTable(gameName, isPrivate, user);
	}
	/**
	 * In cases new tables has to be created but  not all information are available, this function
	 * is used to create a random name based on a random number and the username.
	 */
	private static String createRandomNameBasedOn(User user) {
		int randomNumber = new Random().nextInt(1000);
		return String.format("%s%d", user.getUsername(), randomNumber);
	}

	/**
	 * Use this function to create a new Game Table. The first round of the table will also be prepared.
	 */
	public static GameTable createNewGameTable(String name, boolean isPrivate, User creator) {
		return new GameTable(name, isPrivate, creator);
	}	
	private GameTable(String name, boolean isPrivate, User creator)
	{
		this._id = "";
		this.name = name;
		this.isPrivate = isPrivate;
		this.roundCount = 0;
		setTimeToIndicateIdlePlayers();
		this.pendingUserInvites = new ArrayList<>();
        this.curRound = createFirstRound(creator);
	}
	
	private void setTimeToIndicateIdlePlayers() {
		timeToIndicateIdlePlayers = new Date().getTime();
	}

	public boolean isPrivate() {
		return isPrivate;
	}
	public Round getPreviousRound() {
		return prevRound;
	}
	public void setPreviousRound(Round prevRound) {
		this.prevRound = prevRound;
	}
	public Round getCurRound() {
		return curRound;
	}
	public void setCurRound(Round curRound) {
		this.curRound = curRound;
	}
	public String getId() {
		return _id;
	}
	public void setId(String _id) {
		this._id = _id;
	}
	private void setRoundCount(int _roundCount) {
		roundCount = _roundCount;
	}
	public List<SimpleUser> getPendingUserInvites(){
		return pendingUserInvites;
	}
	private void setPendingUserInvites(List<SimpleUser> _pendingUserInvites) {
		assert (null != pendingUserInvites);
		pendingUserInvites = _pendingUserInvites;
	}
	public int getRoundCount(){
		return roundCount;
	}
	public String getName(){
		return name;
	}
	
	public void nextRound() {
		if(curRound == null)
			throw new RuntimeException("can not advance to next round without having played a first round");
		
		//TODO: delete prev round
		
		setPreviousRound(curRound);
		
		// Draw cards
		Map<SimpleUser, List<IngameWhiteCard>> playersWithHand = new HashMap<>(prevRound.getPlayersWithHand());
		List<SimpleUser> players = prevRound.getPlayers();
		for(SimpleUser su : players) {
			User user;
			try {
				user = DBFacade.getInstance().findUserByEmail(su.getEmail());
			} catch (BAHPersistenceException e) {
				LOG.log(Level.WARNING, String.format("User(%s %s) in mongoDb game table but not in MySQL.", su.getUsername(), su.getEmail()), e);
				// Remove the user from players
				playersWithHand.remove(su);
				continue;
			}
			
			List<IngameWhiteCard> cardsForNextRound = new ArrayList<>(playersWithHand.get(su));
			// Remove submitted cards from copied list
			// Iterate from the end since we operate on the list
			for (int i=cardsForNextRound.size()-1; 0 <= i ;--i) {
				if ( cardsForNextRound.get(i).isSubmitted())
					cardsForNextRound.remove(i);
			}
			user.fillAndResetCards(cardsForNextRound);			
			playersWithHand.put(su, cardsForNextRound);
		}
		
		//determine new judge, players and black card
		SimpleUser newJudge = null;
		SimpleUser prevJudge = prevRound.getJudge();
		if(players.isEmpty())
			newJudge = prevJudge;
		else if (1 == players.size()){
			newJudge = players.get(0);
		} else {
			// Choose next judge in list (assuming the order of the players does not change)
			int oldIdx = players.indexOf(prevJudge);
			newJudge = players.get( (oldIdx+1) % players.size());
		}
		
		//get a random black card
		BlackCard blackCard = getBlackCardAndAvoidThelastBlackCards(_id);
		
		curRound = new Round(playersWithHand, newJudge, blackCard, null);
		
		setTimeToIndicateIdlePlayers();
		roundCount++;
	}
	
	private BlackCard getBlackCardAndAvoidThelastBlackCards(String tableId) {
		assert(null != tableId && !tableId.isEmpty());
		
		List<Integer> historyOfCurrentGame = historyOfBlackCards.get(tableId);
		
		if (null == historyOfCurrentGame) {
			historyOfCurrentGame = new ArrayList<>();
			historyOfBlackCards.put(tableId, historyOfCurrentGame);
		}
		
		BlackCard blackCard = DBFacade.getInstance().getRandomBlackCard(historyOfCurrentGame);
		historyOfCurrentGame.add(blackCard.getId());
		
		return blackCard;
	}
	
	private Round createFirstRound(User tableCreator){
		SimpleUser judge = tableCreator.getSimpleUser();
		
		// get a random black card
		BlackCard blackCard = getBlackCardAndAvoidThelastBlackCards(_id);
		
		Round firstRound = new Round(judge, blackCard);
		firstRound.addPlayer(tableCreator);
		
		return firstRound;
	}

	public static GameTable createFrom(JsonObject jsonObject) {
        // Load all necessary data from json
        String _id = jsonObject.get("_id").getAsString();
        final String _name = jsonObject.get("name").getAsString();
        boolean _isPrivate = jsonObject.get("isPrivate").getAsBoolean();
        int _roundCount = jsonObject.get("roundCount").getAsInt();
        long _timeToIndicateIdlePlayers = getTimeToIndicateIdlePlayersFromJson(jsonObject.get("timeToIndicateIdlePlayers"));

        Round _curRound = Round.createFrom(jsonObject.get("curRound").getAsJsonObject());

        Round _prevRound = jsonObject.has("prevRound") ?
                                Round.createFrom(jsonObject.get("prevRound").getAsJsonObject()) :
                                null;

        List<SimpleUser> _pendingUserInvites = new ArrayList<>();
        JsonArray jsonArray = jsonObject.getAsJsonArray("pendingUserInvites");
        for(int i=0; i < jsonArray.size() ;i++) {
            JsonElement pendingInviteElement = jsonArray.get(i);
            SimpleUser pendingUser = SimpleUser.createFrom(pendingInviteElement.getAsJsonObject());
            _pendingUserInvites.add(pendingUser);
        }

        // Construct result table
        GameTable resultGameTable = new GameTable(_id, _name, _isPrivate, _curRound);
        resultGameTable.timeToIndicateIdlePlayers = _timeToIndicateIdlePlayers;
        resultGameTable.setRoundCount(_roundCount);
        resultGameTable.setPreviousRound(_prevRound);
        resultGameTable.setPendingUserInvites(_pendingUserInvites);

        return resultGameTable;
    }

	private static long getTimeToIndicateIdlePlayersFromJson(JsonElement jsonElement) {
		if (jsonElement.isJsonObject()) {
			return jsonElement.getAsJsonObject().get("$numberLong").getAsLong();
		} else {
			return jsonElement.getAsLong();
		}
	}

	public boolean isValid() {
		// Checking that invites are not players at the same time
		for(SimpleUser su : pendingUserInvites)
			if (curRound.getPlayers().contains(su)) {
				LOG.finest(String.format("GameTable is not valid. User %s is invited and at the same time a player in the current round", su));
				return false;
			}
		return name != null && !name.isEmpty() && curRound.isValid();
	}

	public boolean isValidAndHasId() {
		return isValid() && _id != null && !_id.isEmpty();
	}

	public void addPlayer(String email) {
		assert(curRound != null && email != null && !email.isEmpty());
		curRound.addPlayer(email);
	}
	
	public void removePlayer(String email) {
		assert(curRound != null && email != null && !email.isEmpty());
		// Is this user a judge?
		if(curRound.getJudge().getEmail().equals(email)) {
			assert (1 < curRound.getPlayers().size()) : "Cannot remove last Player. Just delete the table.";
			nextRound();
		}
		curRound.removePlayer(email);
	}

	public boolean isLastPlayer(String email) {
		List<SimpleUser> players = curRound.getPlayers();
		return players.size() == 1 && players.get(0).getEmail().equals(email);
	}

	/**
	 * Adds the give SimpleUser to pendingInvites.
	 * 
	 * @param su
	 * @return false if user is playing or invited already. Otherwise true.
	 */
	public boolean invite(SimpleUser su) {
		if (playsAlready(su) || isInvited(su))
			return false;
		
		pendingUserInvites.add(su);
		
		return true;
	}

	private boolean isInvited(SimpleUser su) {
		assert(null != pendingUserInvites);
		return pendingUserInvites.contains(su);
	}

	private boolean playsAlready(SimpleUser su) {
		return curRound.getPlayers().contains(su);
	}

	/**
	 * Accepts an invitation and moves user from pendingInvites to players of current round.
	 * 
	 * @param su
	 * @return false if user is already playing or not invited. Otherwise true.
	 */
	public boolean acceptInvitation(SimpleUser su) {
		if (playsAlready(su) || !isInvited(su))
			return false;
		
		pendingUserInvites.remove(su);
		
		curRound.addPlayer(su);
		
		return true;
	}

	/**
	 * Declines an invitation and removes user from pendingInvites.
	 * 
	 * @param su
	 * @return false if user is already playing or not invited. Otherwise true.
	 */
	public boolean declineInvitation(SimpleUser su) {
		if (playsAlready(su) || !isInvited(su))
			return false;
		
		pendingUserInvites.remove(su);
		
		return true;
	}

	/**
	 * Submits given white cards for user with 'email'
	 * @param email of a user currently playing
	 * @param validCardIds
	 * @return true if the whole process was successful. False if 
	 * 			- user has already submitted cards,
	 * 			- user with email does not play in this table
	 */
	public boolean submitWhiteCards(String email, int[] validCardIds) {
		
		assert (null != curRound && curRound.isValid());
		
		SimpleUser su = curRound.getPlayerByEmail(email);
		
		boolean submittedSuccessfully = curRound.submitCards(su, validCardIds);
		
		if (submittedSuccessfully){
			if (curRound.allPlayersExceptJudgeSubmittedCards())
				// Now it is the turn of the judge
				setTimeToIndicateIdlePlayers();
		}

		return submittedSuccessfully;
	}

	/**
	 * Selects the given white cards as winning cards for judge with 'email'
	 * @param email of judge of curRound
	 * @param validCardIds
	 * @return true if the whole process was successful. False if 
	 * 			- not all users have submitted cards (except the judge),
	 * 			- user with email is not the judge
	 * @throws BAHPersistenceException if winning users do not exist in DB and thus cannot be rewarded.
	 */
	public boolean judgeWinningCards(String email, int[] validCardIds) throws BAHPersistenceException {
		assert (null != curRound && curRound.isValid());
		
		SimpleUser su = curRound.getPlayerByEmail(email);
		
		boolean judgedSuccessfully = curRound.judgeWinningCards(su, validCardIds);
		
		if (judgedSuccessfully)
			nextRound();
		
		return judgedSuccessfully;
	}
	
	/**
	 * Gets all idle players. If not all players have submitted their cards
	 * then only the judge will be returned if MAX_IDLE_DAYS_FOR_PLAYERS exceeded.
	 * Otherwise only players without the judge will be regarded.
	 * 
	 * If table is private an empty list will be returned.
	 * @return
	 */
	public List<SimpleUser> getIdlePlayers() {
		List<SimpleUser> idlePlayers = new ArrayList<>();

		long deltaInMilliseconds = new Date().getTime() - timeToIndicateIdlePlayers;
		long deltaDays = deltaInMilliseconds / MILLISECONDS_PER_DAY;

		//TODO: remove again
		assert (deltaDays >= 0) : "Oooops you have to switch the order of the dates";
		
		if (deltaDays < MAX_IDLE_DAYS_FOR_PLAYERS)
			return idlePlayers;

		List<SimpleUser> allPlayers = curRound.getPlayers();
		if (1 == allPlayers.size()) {
			return allPlayers;
		}
		
		if (curRound.allPlayersExceptJudgeSubmittedCards()) {
			// This means that this is the turn of the judge
			idlePlayers.add(curRound.getJudge());
		}
		
		// Some player did not submit white cards within MAX_IDLE_DAYS_FOR_PLAYERS
		for(SimpleUser p : allPlayers) {
			if (! curRound.hasSubmittedCards(p))
				idlePlayers.add(p);
		}
		
		return idlePlayers;
	}

	/**
	 * This function can be used to determine lowest allowed idel time in long.
	 * This time is now() - MAX_IDLE_DAYS_FOR_PLAYERS in milliseconds
	 */
	public static long getLowestAllowedIdle() {
		long now = new Date().getTime();
		return now - (MAX_IDLE_DAYS_FOR_PLAYERS * MILLISECONDS_PER_DAY);
	}

}

package persistencelayer.mongodb;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

import persistencelayer.WhiteCard;

/**
 * This represents a WhiteCard in a GameTable.
 * It contains the attribute submitted. This makes it easier
 * to determine which user did submit a card.
 */
public class IngameWhiteCard extends WhiteCard {

	/**
	 * This integer indicates if this card was submitted.
	 * NOT_SUBMITTED means that this card is not submitted.
	 * A positive number indicates the order of submission.
	 */
	@Expose
	private int submittedOrder;
	public static final int NOT_SUBMITTED = 0;
	
	
	public IngameWhiteCard(int id, String text, int submittedOrder){
		super(id, text);
		setSubmitted(submittedOrder);
	}

	public IngameWhiteCard(int id, String text) {
		this(id, text, NOT_SUBMITTED);
	}

	public static IngameWhiteCard createFrom(JsonObject jsonObject) {
        int _id = jsonObject.get("id").getAsInt();
        int _submittedOrder = jsonObject.get("submittedOrder").getAsInt();
        String _text = jsonObject.get("text").getAsString();
        return new IngameWhiteCard(_id, _text, _submittedOrder);
	}
	
	public void setSubmitted(int submittedOrder) {
		assert (0 <= submittedOrder);
		this.submittedOrder = submittedOrder;
	}
	
	public boolean isSubmitted() {
		return submittedOrder != NOT_SUBMITTED;
	}
	
	public int getSubmittedOrder() {
		return submittedOrder;
	}

	public void resetSubmit() {
		setSubmitted(NOT_SUBMITTED);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + submittedOrder;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngameWhiteCard other = (IngameWhiteCard) obj;
		if (submittedOrder != other.submittedOrder)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "IngameWhiteCard [submittedOrder=" + submittedOrder + ", getText()=" + getText() + ", getId()=" + getId()
				+ "]";
	}
	
	
	
}

package persistencelayer.mongodb;

import java.util.logging.Logger;

import com.google.gson.JsonObject;

/**
 * A simple user to store in mongoDB and transfer to other users.
 */
public class SimpleUser {
	private static final Logger LOG = Logger.getLogger(SimpleUser.class.getName());
	
	private String email;
	private String username;
	
	public SimpleUser(String email, String username)
	{
		setEmail(email);
		setUsername(username);
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	@Override
    public String toString() {
		// we need to replace the point sinc it is not a valid mongoDb key for maps.
        return String.format("%s;;;%s",username, getMongoDbEmailFrom(email));
    }

    /**
     * This functions create a user object based on a string. The string has to have the syntax of
     * the string from user.toString().
     * So this is the opposite function of toString for User.
     *
     * This is necessary because we save the user as key in a map. If the map will be converted to
     * json the user has to be converted to a string representation. This will be done by the
     * toString() function. This function converts this representation back.
     */
    public static SimpleUser createFrom(String userToString) {

        assert (null != userToString && !userToString.isEmpty() && userToString.contains(";;;"));
        String[] unameAndEmail = userToString.split(";;;");

        assert (null != unameAndEmail && 2 == unameAndEmail.length);

        String username = unameAndEmail[0];
        String email = getEmailFrom(unameAndEmail[1]);

        return new SimpleUser(email, username);
    }

    public static SimpleUser createFrom(JsonObject jsonObject) {
        String email = jsonObject.get("email").getAsString();
        String username = jsonObject.get("username").getAsString();

        return new SimpleUser(email, username);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleUser user = (SimpleUser) o;

        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return !(username != null ? !username.equals(user.username) : user.username != null);

    }

    @Override
    public int hashCode() {
        int result = email != null ? email.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }
    
    /**
     * Converts given email to a MongoDb compliant version.
     * MongoDb does not allow points in keys.
     * @return a string with escaped '.' characters.
     */
    public static String getMongoDbEmailFrom(String email) {
    	return email.replace(".", ";POINT;");
    }
    public static String getEmailFrom(String mongoDbEmail) {
    	return mongoDbEmail.replace(";POINT;", ".");
    }

	public boolean isValid() {
		return username != null && !username.isEmpty() && email != null && !email.isEmpty();
	}
	
}

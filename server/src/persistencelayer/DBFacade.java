package persistencelayer;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import util.Security;

@SuppressWarnings("unchecked")
public class DBFacade {
	private static final Logger LOG = Logger.getLogger(DBFacade.class.getName());
	
	static{
		LOG.setLevel(Level.ALL);
	}
	
	/**
	 * Static class to ensure only one instance of DBFacade exists
	 */
	private static class InstanceHolder {
		private static DBFacade instance = new DBFacade();
	}

	/**
	 * Returns the one and only instance of DBFacade
	 * 
	 * @return
	 */
	public static DBFacade getInstance() {
		return InstanceHolder.instance;
	}

	/**
	 * JPA Factory to create JPA entity managers
	 */
	private EntityManagerFactory factory = null;

	private DBFacade() {
		super();
		this.factory = Persistence.createEntityManagerFactory("bah");
	}

	/**
	 * Returns a new JPA entity manager
	 */
	private EntityManager getNewEntityManager() {
		EntityManager manager;
		try {
			manager = factory.createEntityManager();
		}catch(PersistenceException e) {
			// Just retry it once. TODO: fix that server session bug thing
			manager = factory.createEntityManager();
		}
		return manager;
	}

	/**
	 * Populates the database with example content
	 * 
	 * @throws BAHPersistenceException
	 */
	public void createDBContent() throws BAHPersistenceException {
		
		List<User> users = getAllUsers();
		List<WhiteCard> cards = getAllWhiteCards();
		for(User u : users){
			for(WhiteCard c : cards)
				u.getCardRepository().add(c);
			updateUser(u);	
		}
		
		
		/*
		BlackCard blackCard = new BlackCard(1, "My name is "+BlackCard.BLANK_INDICATOR);
		this.insertBlackCard(blackCard);

		WhiteCard w1 = new WhiteCard("Kevin");
		WhiteCard w2 = new WhiteCard("Nicht der Viktor");
		WhiteCard w3 = new WhiteCard("Auch nicht der Viktor");
		WhiteCard w4 = new WhiteCard("Erst recht nicht der Viktor");
		this.insertWhiteCard(w1);
		this.insertWhiteCard(w2);
		this.insertWhiteCard(w3);
		this.insertWhiteCard(w4);
		
		User u1 = new User("harry.potter@hs-kl.de", "harry");
		byte[] salt1 = Security.generateSalt();
		byte[] pwd1 = Security.generatePassword("voldemort", salt1);
		u1.setSalt(salt1);
		u1.setPwdHash(pwd1);
		List<WhiteCard> u1Repository = u1.getCardRepository();
		u1Repository.add(w1);
		u1Repository.add(w2);
		u1Repository.add(w3);
		u1Repository.add(w4);

		User u2 = new User("anna.bolika@hs-kl.de", "anna");
		byte[] salt2 = Security.generateSalt();
		byte[] pwd2 = Security.generatePassword("training", salt2);
		u2.setSalt(salt2);
		u2.setPwdHash(pwd2);
		List<WhiteCard> u2Repository = u2.getCardRepository();
		u2Repository.add(w1);
		u2Repository.add(w2);
		u2Repository.add(w3);
		u2Repository.add(w4);
		
		User u3 = new User("a@a.com", "aaa");
		byte[] salt3 = Security.generateSalt();
		byte[] pwd3 = Security.generatePassword("aaaaaa", salt3);
		u3.setSalt(salt3);
		u3.setPwdHash(pwd3);
		List<WhiteCard> u3Repository = u3.getCardRepository();
		u3Repository.add(w1);
		u3Repository.add(w2);
		u3Repository.add(w3);
		u3Repository.add(w4);

		this.insertUser(u1);
		this.insertUser(u2);
		this.insertUser(u3);
		*/
	}

	/**
	 * Deletes entire database content
	 */
	public void deleteDBContent() {
		EntityManager em = this.getNewEntityManager();
		Query q1 = em.createNativeQuery("DELETE FROM "
				+ User.class.getSimpleName());

		Query q2 = em.createNativeQuery("DELETE FROM "
				+ BlackCard.class.getSimpleName());
		
		Query q3 = em.createNativeQuery("DELETE FROM "
				+ WhiteCard.class.getSimpleName());
		
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		q1.executeUpdate();
		q2.executeUpdate();
		q3.executeUpdate();
		tx.commit();
		em.close();
	}

	/**
	 * Inserts a user into database
	 * 
	 * @param benutzer
	 * @throws BAHPersistenceException
	 */
	public void insertUser(User user) throws BAHPersistenceException {
		if (null == user)
			return;

		EntityManager em = null;
		try {
			em = this.getNewEntityManager();
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
			LOG.log(Level.FINER, "Inserted User " + user.toString());
		} catch (RuntimeException e) {
			throw new BAHPersistenceException("Could not insert user: "
					+ user.toString() + " because " +e);
		} finally {
			if (null != em)
				em.close();
		}
	}
	
	/**
	 * Updates a user of the database
	 * 
	 * @param benutzer
	 * @throws BAHPersistenceException
	 */
	public void updateUser(User user) throws BAHPersistenceException {
		if (null == user)
			return;

		EntityManager em = null;
		try {
			em = this.getNewEntityManager();
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
			LOG.log(Level.FINER, "Updated User " + user.toString());
		} catch (RuntimeException e) {
			throw new BAHPersistenceException("Could not update user: "
					+ user.toString() + " because " +e);
		} finally {
			if (null != em)
				em.close();
		}
	}

	/**
	 * Inserts a black card into database
	 * 
	 * @param benutzer
	 * @throws BAHPersistenceException
	 */
	public void insertBlackCard(BlackCard card) throws BAHPersistenceException {
		if (null == card)
			return;

		EntityManager em = null;
		try {
			em = this.getNewEntityManager();
			em.getTransaction().begin();
			em.persist(card);
			em.getTransaction().commit();
			LOG.log(Level.FINER, "Inserted BlackCard " + card.toString());
		} catch (RuntimeException e) {
			throw new BAHPersistenceException("Could not persist black card: "
					+ card.toString() + " because " +e);
		} finally {
			if (null != em)
				em.close();
		}
	}

	/**
	 * Inserts a white card into database
	 * 
	 * @param benutzer
	 * @throws BAHPersistenceException
	 */
	public void insertWhiteCard(WhiteCard card) throws BAHPersistenceException {
		if (null == card)
			return;

		EntityManager em = null;
		try {
			em = this.getNewEntityManager();
			em.getTransaction().begin();
			em.persist(card);
			em.getTransaction().commit();
			LOG.log(Level.FINER, "Inserted WhiteCard " + card.toString());
		} catch (RuntimeException e) {
			throw new BAHPersistenceException("Could not persist white card: "
					+ card.toString() + " because " +e);
		} finally {
			if (null != em)
				em.close();
		}
	}

	/**
	 * Returns a list of all users contained within the database
	 * 
	 * @return
	 * @throws BAHPersistenceException
	 */
	public List<User> getAllUsers() throws BAHPersistenceException {
		EntityManager em = this.getNewEntityManager();
		Query q = em.createQuery("select u from " + User.class.getSimpleName()
				+ " u");
		List<User> userList = q.getResultList();
		em.close();
		return userList;
	}

	/**
	 * Returns a list of all black cards contained within the database
	 * 
	 * @return
	 * @throws BAHPersistenceException
	 */
	public List<BlackCard> getAllBlackCards() throws BAHPersistenceException {
		EntityManager em = this.getNewEntityManager();
		Query q = em.createQuery("select c from "
				+ BlackCard.class.getSimpleName() + " c");
		List<BlackCard> cardList = q.getResultList();
		em.close();
		return cardList;
	}
	
	/**
	 * Returns a list of all white cards contained within the database
	 * 
	 * @return
	 * @throws BAHPersistenceException
	 */
	public List<WhiteCard> getAllWhiteCards() throws BAHPersistenceException {
		EntityManager em = this.getNewEntityManager();
		Query q = em.createQuery("select c from "
				+ WhiteCard.class.getSimpleName() + " c");
		List<WhiteCard> cardList = q.getResultList();
		em.close();
		return cardList;
	}
	
	/**
	 * Returns a list of the starter white cards contained within the database.
	 * The starter white cards represent the starter kit of white cards that a user receives on registration.
	 * @return
	 * @throws BAHPersistenceException
	 */
	public List<WhiteCard> getStarterWhiteCards() throws BAHPersistenceException {
		EntityManager em = this.getNewEntityManager();
		Query q = em.createNativeQuery("select * from "+ WhiteCard.class.getSimpleName()+" as c LEFT JOIN "+ WhiteCard.TABLE_NAME_STARTER_CARDS +" as s ON c.id = s.WHITECARD_ID", WhiteCard.class);
		List<WhiteCard> cardList = q.getResultList();
		em.close();
		return cardList;
	}
	
	/**
	 * Returns a random white card from database
	 */
	public WhiteCard getRandomWhiteCard(){
		EntityManager em = this.getNewEntityManager();
		TypedQuery<Integer> intQuery = em.createQuery("select max(c.id) from " + WhiteCard.class.getSimpleName() + " c", Integer.class);
		int maxId = intQuery.getResultList().get(0);
		int randomId = (int)(Math.random() * maxId) + 1;
		TypedQuery<WhiteCard> randomCardQuery = em.createQuery("select c from "+ WhiteCard.class.getSimpleName() + " c where c.id="+randomId, WhiteCard.class);
		WhiteCard randomCard = randomCardQuery.getSingleResult();
		em.close();
		return randomCard;
	}
	
	/**
	 * Returns a random black card from database
	 * 
	 * @param historyOfBlackCards A list of black card ids belonging to a specific game.
	 * A black random card will not be one of these cards as long as the size is smaller 
	 * than the size of the black cards repository.
	 */
	public BlackCard getRandomBlackCard(List<Integer> historyOfBlackCards){
		EntityManager em = this.getNewEntityManager();
		TypedQuery<Integer> intQuery = em.createQuery("select max(c.id) from " + BlackCard.class.getSimpleName()+ " c", Integer.class);
		int maxId = intQuery.getResultList().get(0);
		
		if ( 0.8 * maxId <= historyOfBlackCards.size()) {  // We check 80% because we don't want to search long for a random number in case the list is almost as long as maxId
			// Remove the first ten
			int numOfCardsToRemove = 10;
			for(int i=0; i < numOfCardsToRemove && 0 <= historyOfBlackCards.size() ;++i)
				historyOfBlackCards.remove(0);  // Removing the first element since we assume this is the oldest black card
		}
		
		// Generate a random id which is not in the history
		int randomId = -1;
		while (-1 == randomId) {
			int candidate = (int)(Math.random() * maxId) + 1;
			if ( ! historyOfBlackCards.contains(candidate))
				randomId = candidate;
		}
		
		TypedQuery<BlackCard> randomCardQuery = em.createQuery("select c from "+ BlackCard.class.getSimpleName()+" c where c.id="+randomId, BlackCard.class);
		BlackCard randomCard = randomCardQuery.getSingleResult();
		em.close();
		return randomCard;
	}

	/**
	 * Looks for and returns the user with the given email from database
	 * 
	 * @param email
	 * @return
	 * @throws BAHPersistenceException
	 */
	public User findUserByEmail(String email) throws BAHPersistenceException {
		EntityManager em = this.getNewEntityManager();
		Query q = em.createQuery("select u from " + User.class.getSimpleName()
				+ " u WHERE u.email = :email");
		q.setParameter("email", email);
		List<User> userList = q.getResultList();
		em.close();

		if (userList.size() == 1) {
			return userList.get(0);
		} else if (userList.size() == 0) {
			throw new BAHPersistenceException("Found no user with email "
					+ email + " !");
		} else {
			throw new BAHPersistenceException("Email " + email
					+ " is not unique!");
		}
	}

	/**
	 * Looks for and returns the user with the given username from database
	 * 
	 * @param username
	 * @return
	 * @throws BAHPersistenceException
	 */
	public User findUserByUsername(String username) throws BAHPersistenceException {
		EntityManager em = this.getNewEntityManager();
		Query q = em.createQuery("select u from " + User.class.getSimpleName()
				+ " u WHERE u.username = :username");
		q.setParameter("username", username);
		List<User> userList = q.getResultList();
		em.close();
		
		if (userList.size() == 1) {
			return userList.get(0);
		} else if (userList.size() == 0) {
			throw new BAHPersistenceException("Found no user with username "
					+ username + " !");
		} else {
			throw new BAHPersistenceException("Username " + username
					+ " is not unique!");
		}
	}
	
	/**
	 * Looks for and returns all users with the given emails from database
	 * 
	 * @param email
	 * @return
	 * @throws BAHPersistenceException
	 */
	public List<User> findUsersByEmails(List<String> emails) throws BAHPersistenceException {
		EntityManager em = this.getNewEntityManager();
		Query q = em.createQuery("select u from " + User.class.getSimpleName()
				+ " u WHERE u.email IN :emails");
		q.setParameter("emails", emails);
		List<User> userList = q.getResultList();
		em.close();
		
		if (userList.size() != emails.size()) {
			throw new BAHPersistenceException(String.format("Num of found users %d does not match num of given emails %d. \nUsers: %s\nEmails: %s", userList.size(), emails.size(), userList, emails));
		}
		return userList;
	}

	/**
	 * Reads the user with the given email from db and checks whether the given
	 * password matches the user's one
	 * 
	 * @param usermail
	 * @param passwd
	 * @return
	 * @throws BAHPersistenceException
	 */
	public boolean checkLogin(String usermail, String passwd)
			throws BAHPersistenceException {
		User user = this.findUserByEmail(usermail);
		return checkLogin(user, passwd);
	}

	/**
	 * Checks whether the given password matches the user's one
	 * 
	 * @param user
	 * @param passwd
	 * @return
	 */
	public boolean checkLogin(User user, String passwd) {
		if (null == user)
			return false;

		byte[] pwd = user.getPwdHash();
		byte[] salt = user.getSalt();

		byte[] pwdToTest = Security.generatePassword(passwd, salt);

		if (pwd.length != pwdToTest.length)
			return false;

		boolean compare = true;
		for (int i = 0; i < pwd.length; i++) {
			compare &= (pwd[i] == pwdToTest[i]);
		}

		return compare;
	}

}

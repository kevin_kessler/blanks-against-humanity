package persistencelayer;

import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.JsonObject;

@Entity
public class BlackCard {
	public static final String BLANK_INDICATOR = "{BLANK}";
	private static final Logger LOG = Logger.getLogger(BlackCard.class.getName());
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	private int numOfBlanks;
	
	@Column(name="deDE")
	private String text;
	
	public BlackCard(){}
	public BlackCard(int numOfBlanks, String text){
		this.numOfBlanks = numOfBlanks;
		this.text = text;
	}
	
	private BlackCard(int _id, int _numOfBlanks, String _text) {
		id = _id;
		numOfBlanks = _numOfBlanks;
		text = _text;
	}

	public int getId() {
		return id;
	}
	public int getNumOfBlanks() {
		return numOfBlanks;
	}
	public String getText() {
		return text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlackCard other = (BlackCard) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BlackCard [id=" + id + ", numOfBlanks=" + numOfBlanks
				+ ", text=" + text + "]";
	}

	public static BlackCard createFrom(JsonObject jsonObject) {
		int _id = jsonObject.get("id").getAsInt();
        int _numOfBlanks = jsonObject.get("numOfBlanks").getAsInt();
        String _text = jsonObject.get("text").getAsString();
        return new BlackCard(_id, _numOfBlanks, _text);
	}

	public boolean isValid() {
		return 0 <= id && 0 < numOfBlanks && text != null && text.contains(BLANK_INDICATOR);
	}
	
	
}

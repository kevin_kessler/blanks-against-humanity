package persistencelayer;

public class BAHPersistenceException extends Exception
{
   private static final long serialVersionUID = -2410094803461586984L;

   public BAHPersistenceException(String errorMsg)
   {
      super(errorMsg);
   }
   
   public String getErrorMessage()
   {
      return super.getMessage();
   }
}

package persistencelayer;

import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.gson.annotations.Expose;

import persistencelayer.mongodb.IngameWhiteCard;

@Entity
public class WhiteCard {
	public static final String TABLE_NAME_STARTER_CARDS = WhiteCard.class.getSimpleName()+"_starter";
	private static final Logger LOG = Logger.getLogger(WhiteCard.class.getName());
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Expose
	protected Integer id;
	
	@Column(name="deDE")
	@Expose
	private String text;
	
	public WhiteCard(){}
	public WhiteCard(String text){
		this.text = text;
	}
	
	protected WhiteCard(int id, String text) {
		this.id = id;
		this.text = text;
	}
	public String getText() {
		return text;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WhiteCard other = (WhiteCard) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "WhiteCard [id=" + id + ", text=" + text + "]";
	}

	public int getId() {
		return id;
	}
	
	public IngameWhiteCard toIngameCard() {
		return new IngameWhiteCard(id, text);
	}
	/**
	 * idAsStr is valid if it is a string of any positive integer.
	 */
	public static boolean idIsCorrect(String idAsStr) {
		assert( null != idAsStr && !idAsStr.isEmpty());
		try {
			int possibleId = Integer.parseInt(idAsStr);
			return 0 <= possibleId;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
}

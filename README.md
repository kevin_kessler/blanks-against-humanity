An android application that represents a digital version of a famous party card game.

The repository includes the android application sources as well as the sources for running the central web-server application (Java EE) on which the games take place.

**The project was part of my studies in Computer Science (M.Sc.).**

Diese Datei beschreibt die Ordnerstruktur und Inhalte des vorliegenden git Repositories

assets
======
Beinhaltet während der Entwicklung erstellte Bilddateien sowie ihre zugehörigen Photoshop Projektdateien

client
======
Beinhaltet den Quellcode der Android Anwendung. Der Ordner Entspricht einem Android Studio Projekt und sollte ohne weiteres in die Entwicklungsumgebung importiert werden können.

db_installation
===============
Beinhaltet Skripte und Hilfsdateien zum Einrichten der Datenbanken, die zum Betrieb der Serveranwendung benötigt werden.

server
======
Entspricht dem Eclipse Java EE Projekt der Server Anwendung und beinhaltet deren Quellcode. Falls es Probleme beim Einbinden des Projekts in Eclipse geben sollte, helfen die Anweisungen aus Kapital 5 des Projektberichts.

bah.apk
=======
Ist die unsignierte APK der Anwendung zur einfachen Installation auf einem Android Gerät.

Projektbericht.pdf
==================
Entspricht dem Projektbericht

README.md
=========
Diese Datei
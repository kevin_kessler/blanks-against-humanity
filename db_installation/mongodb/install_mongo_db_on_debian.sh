#!/usr/bin/bash

# Execute this script as root, e.g. sudo bash <script>

# We need mongodb >=2.6. The official package manager do only have 2.4.
# See http://docs.mongodb.org/manual/tutorial/install-mongodb-on-debian/

apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.0 main" | tee /etc/apt/sources.list.d/mongodb.list
apt-get update
apt-get install -y mongodb-org


# Restore data. Assuming a 'mongodump' was executed in this directory.
mongo bah --eval "db.dropDatabase()"
mongorestore

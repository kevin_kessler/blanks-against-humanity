#!/usr/bin/bash

# Execute this script as root, e.g. sudo bash <script>

# We need mongodb >=2.6. The official package manager do only have 2.4.
# See https://www.digitalocean.com/community/questions/how-to-upgrade-mongodb-2-4-to-2-6-on-ubuntu-14-04

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | tee /etc/apt/sources.list.d/mongodb.list
apt-get update
apt-get install -y mongodb-org


# Restore data. Assuming a 'mongodump' was executed in this directory.
mongo bah --eval "db.dropDatabase()"
mongorestore

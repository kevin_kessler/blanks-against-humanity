Zum Einrichten der DataSource dem Tomcatserver folgende XML-Eintr�ge hinzuf�gen

context.xml: 
============
in Context:
<Context>
...
	<!-- BAH DataSource -->
	<ResourceLink 
		name="jdbc/bah" 
		global="jdbc/bah" 
		type="javax.sql.DataSource" />
...
</Context>



server.xml:
===========

in GlobalNamingResources:
<GlobalNamingResources>
...
	<!-- BAH DataSource -->
  	<Resource 
	  	auth="Container" 
	  	driverClassName="com.mysql.jdbc.Driver" 
	  	maxActive="10" 
	  	maxIdle="3" 
		maxWait="10000"
		name="jdbc/bah" 
		password="Qd2RqUEBzbn8q6nH" 
		type="javax.sql.DataSource" 
		url="jdbc:mysql://localhost/bah"
		username="bah_admin"/>	
...
</GlobalNamingResources>

#!/bin/bash


# Preinstallation of mysql with a root user is assumed. This script has to be executed in this directory

ls -1 *.sql | awk '{ print "source",$0 }' | mysql --batch -u root -p
